package de.hft_stuttgart.hss;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;

import com.airhacks.afterburner.injection.Injector;

import de.hft_stuttgart.hss.services.ArticleService;
import de.hft_stuttgart.hss.services.CustomerService;
import de.hft_stuttgart.hss.services.ExportService;
import de.hft_stuttgart.hss.services.InvoiceService;
import de.hft_stuttgart.hss.services.LoginService;
import de.hft_stuttgart.hss.services.OrderService;
import de.hft_stuttgart.hss.services.ShippingOrderService;
import de.hft_stuttgart.hss.services.ViewManager;
import de.hft_stuttgart.hss.services.misc.HssProgressNotification;
import de.hft_stuttgart.hss.services.misc.ServiceLocator;
import de.hft_stuttgart.hss.view.common.domain.context.ArticleContext;
import de.hft_stuttgart.hss.view.common.domain.context.CustomerContext;
import de.hft_stuttgart.hss.view.common.login.LoginView;
import de.hft_stuttgart.hss.view.common.validator.HssValidator;
import javafx.application.Application;
import javafx.application.Preloader;
import javafx.application.Preloader.StateChangeNotification;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class App extends Application {

	private ServiceLocator serviceLocator;

	private List<Class<?>> services = Arrays.asList(
			// Validator
			HssValidator.class,
			// Services
			ArticleService.class,
			CustomerService.class,
			LoginService.class,
			OrderService.class,
			InvoiceService.class,
			ExportService.class,
			ShippingOrderService.class,
			// Contexts
			ArticleContext.class,
			CustomerContext.class
			);

	// private List<Class<?>> contexts = Arrays.asList(
	//
	// );

	private void longStart(EventHandler<WorkerStateEvent> handler) {
		// simulate long init in background
		Task<Void> task = new Task<Void>() {
			@SuppressWarnings("unchecked")
			@Override
			protected Void call() throws Exception {
				int max = 100;

				// Send progress to preloader
				notifyPreloader(new HssProgressNotification((25.0 / max), "Loading injection context..."));
				serviceLocator = new ServiceLocator();

				notifyPreloader(new HssProgressNotification((40.0 / max), "Initialize injection context..."));
				serviceLocator.initialize();

				notifyPreloader(new HssProgressNotification((50.0 / max), "Loading services..."));
				Injector.setModelOrService(ServiceLocator.class, serviceLocator);
				for (Class<?> service : services) {
					Injector.setModelOrService((Class<Object>) service, serviceLocator.getInstance(service));
				}

				notifyPreloader(new HssProgressNotification((75.0 / max), "Get db connection..."));
				EntityManager entityManager = serviceLocator.getInstance(EntityManager.class);

				notifyPreloader(new HssProgressNotification((85.0 / max), "Open db connection... "));
				entityManager.createQuery("SELECT 1 FROM Employee").getResultList();

				notifyPreloader(new HssProgressNotification((99.0 / max), "Finished..."));
				notifyPreloader(new StateChangeNotification(StateChangeNotification.Type.BEFORE_START));

				return null;
			}

		};
		task.exceptionProperty().addListener((ov, oldV, newV) -> notifyPreloader(new Preloader.ErrorNotification("location", newV.getMessage(), newV)));
		task.setOnSucceeded(handler);
		new Thread(task).start();
	}

	@Override
	public void start(Stage stage) throws Exception {
		longStart(t -> {
			/*
			 * Eine neue Szene wird erstellt - Bühnengerüst bleibt - Szene auf
			 * der Bühne ändert sich. LoginView sagt aus das im Package ein
			 * loginView.fxml, loginView.css und der LoginViewPresenter gefunden
			 * wird
			 */
			stage.setTitle("High Speed Sales - Login");
			stage.setResizable(false);

			ViewManager viewManager = serviceLocator.getInstance(ViewManager.class);
			Injector.setModelOrService(ViewManager.class, viewManager);
			viewManager.setPrimaryStage(stage);

			LoginView loginView = new LoginView();
			Scene scene = new Scene(loginView.getView());
			stage.setScene(scene);
			stage.getIcons().add(new Image(getClass().getResourceAsStream("highSpeed_Icon.png")));

			stage.show();
		});
	}

	@Override
	public void stop() throws Exception {
		if (serviceLocator != null) {
			serviceLocator.close();
		}
		Injector.forgetAll();
		System.exit(0);
	}

}
