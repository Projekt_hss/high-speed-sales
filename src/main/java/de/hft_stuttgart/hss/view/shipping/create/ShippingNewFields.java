package de.hft_stuttgart.hss.view.shipping.create;

import static de.hft_stuttgart.hss.view.common.validator.HssValidatorType.DATE;

import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.validator.HssValidatorField;
import de.hft_stuttgart.hss.view.common.validator.HssValidatorType;

public enum ShippingNewFields implements HssValidatorField {
	SHIPPING_DATE(true, DATE) {
		@Override
		public Object getValue(Object object) {
			return ((OrderContext) object).getShipping().shippingDateProperty().get();
		}

		@Override
		public Object getSecondaryInfo() {
			return 365L;
		}
	};

	private boolean required;
	private HssValidatorType type;

	private ShippingNewFields(boolean required, HssValidatorType type) {
		this.required = required;
		this.type = type;
	}

	@Override
	public boolean isRequired(Object obj) {
		return required;
	}

	@Override
	public HssValidatorType getType() {
		return type;
	}
}
