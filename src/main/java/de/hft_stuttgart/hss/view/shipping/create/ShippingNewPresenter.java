package de.hft_stuttgart.hss.view.shipping.create;

import static de.hft_stuttgart.hss.view.shipping.create.ShippingNewFields.SHIPPING_DATE;

import java.util.HashMap;
import java.util.Optional;

import javax.inject.Inject;

import de.hft_stuttgart.hss.entities.OrderState;
import de.hft_stuttgart.hss.services.ExportService;
import de.hft_stuttgart.hss.services.ShippingOrderService;
import de.hft_stuttgart.hss.services.misc.ExportType;
import de.hft_stuttgart.hss.services.misc.HssUtils;
import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.domain.context.OrderDataContext;
import de.hft_stuttgart.hss.view.common.domain.data.OrderPosition;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.common.misc.DateBeforeCell;
import de.hft_stuttgart.hss.view.order.common.order.search.OrderSearchDialogView;
import de.hft_stuttgart.hss.view.shipping.overview.ShippingOverviewView;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.GridPane;
import javafx.util.converter.NumberStringConverter;

public class ShippingNewPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	TableView<OrderPosition> orderPositionTable;
	@FXML
	Button orderSelectButton;
	@FXML
	Button createButton;
	@FXML
	TextField customerId;
	@FXML
	TextField orderId;
	@FXML
	TextField company;
	@FXML
	TextField name;
	@FXML
	TextField street;
	@FXML
	TextField city;
	@FXML
	DatePicker shippingDate;
	@FXML
	GridPane shippingAddressWrapper;
	@FXML
	RadioButton radioYes;
	@FXML
	RadioButton radioNo;
	@FXML
	TextField street2;
	@FXML
	TextField city2;
	@FXML
	TextField number2;
	@FXML
	TextField zip2;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	OrderDataContext context;
	@Inject
	ExportService exportService;
	@Inject
	ShippingOrderService shippingOrderService;

	@SuppressWarnings("unchecked")
	@Override
	protected void init() {
		context.setStates(new OrderState[] { OrderState.OPEN });

		ObservableList<TableColumn<OrderPosition, ?>> columns = orderPositionTable.getColumns();

		((TableColumn<OrderPosition, Number>) columns.get(0)).setCellValueFactory((order) -> order.getValue().positionProperty());
		((TableColumn<OrderPosition, Number>) columns.get(1)).setCellValueFactory((order) -> order.getValue().articleIdProperty());
		((TableColumn<OrderPosition, String>) columns.get(2)).setCellValueFactory((order) -> order.getValue().articleNameProperty());
		((TableColumn<OrderPosition, Number>) columns.get(3)).setCellValueFactory((order) -> order.getValue().amountProperty());
		((TableColumn<OrderPosition, Number>) columns.get(3)).setCellFactory(TextFieldTableCell.forTableColumn(new NumberStringConverter()));
		((TableColumn<OrderPosition, String>) columns.get(4)).setCellValueFactory((order) -> order.getValue().unityProperty());

		shippingDate.setDayCellFactory(cell -> new DateBeforeCell((long) SHIPPING_DATE.getSecondaryInfo()));

		addValidatorField(SHIPPING_DATE, shippingDate);
	}

	/**
	 * Ein neuer Akt (ÜbersichtsSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void abortButtonClicked() {
		navigateToOverview();
	}

	/**
	 * Ein neuer Akt (LieferscheinÜbersichtsSeite) wird erstellt - Bühnengerüst
	 * bleibt - Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void backButtonClicked() {
		navigateTo(ShippingOverviewView.class);
	}

	/**
	 * Legt den Lieferschein in die Datenbank ab und kann als PDF exportiert
	 * werden
	 */
	public void createButtonClicked() {
		OrderContext order = context.getOrderContext();
		validate(order, ShippingNewFields.values(), t -> {
			shippingOrderService.save(order);

			Optional<ButtonType> result = showConfirmationDialog("print.dialog.title", "print.dialog.text");
			if (result.get() == ButtonType.YES) {
				exportService.export(ExportType.SHIPPING, order, resources::getString, this::showProgressDialog, e -> navigateToOverview(), viewManager::getPrimaryStage, new HashMap<>());
			} else {
				navigateToOverview();
			}

		});

	}

	/**
	 * Popup-Fenster um Aufträge, welche Status "offen" besitzen, auszuwählen.
	 */
	public void selectOrderButtonClicked() {
		showCustomDialog("view.shipping.create.search", OrderSearchDialogView.class);

		if (context.getOrderContext() != null) {
			setOrderDetails(context.getOrderContext());
			createButton.setDisable(false);
		}
	}

	/**
	 * @param orderContext
	 * Zuordnung der Variablen
	 */
	private void setOrderDetails(OrderContext orderContext) {
		customerId.textProperty().bindBidirectional(orderContext.getCustomer().customerIdProperty(), new NumberStringConverter());
		company.textProperty().bind(orderContext.getCustomer().companyProperty());
		name.textProperty().bind(HssUtils.concat(orderContext.getCustomer().prenameProperty(), orderContext.getCustomer().lastnameProperty()));
		street.textProperty().bind(HssUtils.concat(orderContext.getCustomer().streetProperty(), orderContext.getCustomer().numberProperty()));
		city.textProperty().bind(HssUtils.concat(orderContext.getCustomer().zipProperty(), orderContext.getCustomer().cityProperty()));
		street2.textProperty().bind(HssUtils.concat(orderContext.getShipping().streetProperty(), orderContext.getShipping().numberProperty()));
		city2.textProperty().bind(HssUtils.concat(orderContext.getShipping().zipProperty(), orderContext.getShipping().cityProperty()));
		shippingDate.valueProperty().bindBidirectional(orderContext.getShipping().shippingDateProperty());

		radioYes.selectedProperty().bind(orderContext.getShipping().shippingAddressAvailableProperty());
		radioNo.selectedProperty().bind(Bindings.not(orderContext.getShipping().shippingAddressAvailableProperty()));

		shippingAddressWrapper.visibleProperty().bind(orderContext.getShipping().shippingAddressAvailableProperty());

		orderPositionTable.setItems(orderContext.getPositions());
	}
}
