package de.hft_stuttgart.hss.view.shipping.details;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import javax.inject.Inject;

import de.hft_stuttgart.hss.services.ExportService;
import de.hft_stuttgart.hss.services.misc.ExportType;
import de.hft_stuttgart.hss.services.misc.HssUtils;
import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.domain.context.OrderDataContext;
import de.hft_stuttgart.hss.view.common.domain.data.OrderPosition;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.GridPane;
import javafx.util.converter.LocalDateStringConverter;
import javafx.util.converter.NumberStringConverter;

public class ShippingDetailsPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	TableView<OrderPosition> orderPositionTable;
	@FXML
	Button closeButton;
	@FXML
	TextField customerId;
	@FXML
	TextField shippingOrderId;
	@FXML
	TextField orderId;
	@FXML
	TextField company;
	@FXML
	TextField name;
	@FXML
	TextField street;
	@FXML
	TextField city;
	@FXML
	TextField shippingDate;
	@FXML
	GridPane shippingAddressWrapper;
	@FXML
	RadioButton radioYes;
	@FXML
	RadioButton radioNo;
	@FXML
	TextField street2;
	@FXML
	TextField city2;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	OrderDataContext context;
	@Inject
	ExportService exportService;

	@SuppressWarnings("unchecked")
	@Override
	protected void init() {
		ObservableList<TableColumn<OrderPosition, ?>> columns = orderPositionTable.getColumns();

		((TableColumn<OrderPosition, Number>) columns.get(0)).setCellValueFactory((position) -> position.getValue().positionProperty());
		((TableColumn<OrderPosition, Number>) columns.get(1)).setCellValueFactory((position) -> position.getValue().articleIdProperty());
		((TableColumn<OrderPosition, String>) columns.get(2)).setCellValueFactory((position) -> position.getValue().articleNameProperty());
		((TableColumn<OrderPosition, Number>) columns.get(3)).setCellValueFactory((position) -> position.getValue().amountProperty());
		((TableColumn<OrderPosition, Number>) columns.get(3)).setCellFactory(TextFieldTableCell.forTableColumn(new NumberStringConverter()));
		((TableColumn<OrderPosition, String>) columns.get(4)).setCellValueFactory((position) -> position.getValue().unityProperty());

		OrderContext orderContext = context.getOrderContext();

		orderId.textProperty().bindBidirectional(orderContext.getOrder().orderIdProperty(), new NumberStringConverter());
		shippingOrderId.textProperty().bindBidirectional(orderContext.getShipping().shippingIdProperty(), new NumberStringConverter());
		customerId.textProperty().bindBidirectional(orderContext.getCustomer().customerIdProperty(), new NumberStringConverter());
		company.textProperty().bind(orderContext.getCustomer().companyProperty());
		name.textProperty().bind(HssUtils.concat(orderContext.getCustomer().prenameProperty(), orderContext.getCustomer().lastnameProperty()));
		street.textProperty().bind(HssUtils.concat(orderContext.getCustomer().streetProperty(), orderContext.getCustomer().numberProperty()));
		city.textProperty().bind(HssUtils.concat(orderContext.getCustomer().zipProperty(), orderContext.getCustomer().cityProperty()));
		street2.textProperty().bind(HssUtils.concat(orderContext.getShipping().streetProperty(), orderContext.getShipping().numberProperty()));
		city2.textProperty().bind(HssUtils.concat(orderContext.getShipping().zipProperty(), orderContext.getShipping().cityProperty()));
		shippingDate.textProperty().bindBidirectional(orderContext.getShipping().shippingDateProperty(), new LocalDateStringConverter(DateTimeFormatter.ISO_LOCAL_DATE, DateTimeFormatter.ISO_LOCAL_DATE));

		radioYes.selectedProperty().bind(orderContext.getShipping().shippingAddressAvailableProperty());
		radioNo.selectedProperty().bind(Bindings.not(orderContext.getShipping().shippingAddressAvailableProperty()));

		shippingAddressWrapper.visibleProperty().bind(orderContext.getShipping().shippingAddressAvailableProperty());

		orderPositionTable.setItems(orderContext.getPositions());
	}


	/**
	 * Popup-Fenster wird geschlossen
	 */
	public void closeButtonClicked() {
		viewManager.getDialogStage().close();
	}

	/**
	 * Lieferschein Drucken als PDF
	 */
	public void printShipping() {
		exportService.export(ExportType.SHIPPING, context.getOrderContext(), resources::getString, this::showProgressDialog, null, viewManager::getPrimaryStage, new HashMap<>());
	}
}
