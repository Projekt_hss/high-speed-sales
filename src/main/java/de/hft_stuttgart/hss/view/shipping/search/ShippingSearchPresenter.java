package de.hft_stuttgart.hss.view.shipping.search;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.inject.Inject;

import de.hft_stuttgart.hss.services.ShippingOrderService;
import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.domain.context.OrderDataContext;
import de.hft_stuttgart.hss.view.common.domain.data.ShippingSearch;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.shipping.details.ShippingDetailsView;
import de.hft_stuttgart.hss.view.shipping.overview.ShippingOverviewView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.util.converter.LocalDateStringConverter;

public class ShippingSearchPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	TableView<ShippingSearch> shippingTable;
	@FXML
	TextField search;
	@FXML
	Button shippingSelectButton;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	ShippingOrderService shippingService;
	@Inject
	OrderDataContext context;

	@SuppressWarnings("unchecked")
	@Override
	protected void init() {
		List<ShippingSearch> allShippings = shippingService.getAll();
		shippingTable.setItems(FXCollections.observableArrayList(allShippings));

		ObservableList<TableColumn<ShippingSearch, ?>> columns = shippingTable.getColumns();
		((TableColumn<ShippingSearch, Number>) columns.get(0)).setCellValueFactory((bill) -> bill.getValue().shippingIdProperty());
		((TableColumn<ShippingSearch, LocalDate>) columns.get(1)).setCellValueFactory((bill) -> bill.getValue().shippingDateProperty());
		((TableColumn<ShippingSearch, LocalDate>) columns.get(1)).setCellFactory(TextFieldTableCell.forTableColumn(new LocalDateStringConverter(DateTimeFormatter.ISO_LOCAL_DATE, DateTimeFormatter.ISO_LOCAL_DATE)));
		((TableColumn<ShippingSearch, Number>) columns.get(2)).setCellValueFactory((bill) -> bill.getValue().orderIdProperty());
		((TableColumn<ShippingSearch, Number>) columns.get(3)).setCellValueFactory((bill) -> bill.getValue().customerIdProperty());
		((TableColumn<ShippingSearch, String>) columns.get(4)).setCellValueFactory((bill) -> bill.getValue().companyProperty());
		((TableColumn<ShippingSearch, String>) columns.get(5)).setCellValueFactory((bill) -> bill.getValue().lastnameProperty());
		((TableColumn<ShippingSearch, String>) columns.get(6)).setCellValueFactory((bill) -> bill.getValue().prenameProperty());

		onItemSelected(shippingTable, t -> shippingSelectButton.setDisable(false));
	}

	/**
	 * Ein neuer Akt (ÜbersichtsSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void abortButtonClicked() {
		navigateToOverview();
	}

	/**
	 * Ein neuer Akt (LieferscheinÜbersichtsSeite) wird erstellt - Bühnengerüst
	 * bleibt - Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void backButtonClicked() {
		navigateTo(ShippingOverviewView.class);
	}

	/**
	 * Details des ausgewählten Datensatz in einem Popup Fenster anzeigen
	 */
	public void detailButtonClicked() {
		ShippingSearch shippingSearch = shippingTable.getSelectionModel().getSelectedItem();
		if (shippingSearch != null) {
			OrderContext orderContext = shippingService.getShipping(shippingSearch.getShippingId());
			context.setOrderContext(orderContext);
			showCustomDialog("dialog.shipping.details", ShippingDetailsView.class);
		}
	}

	/**
	 * bereits angelegte Lieferscheine suchen
	 */
	public void search() {
		doSearch(search::getText, shippingService::getAll, shippingService::searchShippings, shippingTable::setItems);
	}

	/**
	 * Methode prüft auf Doppelklick und ruft danach eine weitere Methode auf
	 */
	public void mouseClicked(MouseEvent event) {
		if (event.getClickCount() == 2) {
			detailButtonClicked();
		}
	}
}
