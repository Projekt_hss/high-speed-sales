package de.hft_stuttgart.hss.view.shipping.overview;

import com.airhacks.afterburner.injection.Injector;

import de.hft_stuttgart.hss.view.common.domain.context.OrderDataContext;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.shipping.create.ShippingNewView;
import de.hft_stuttgart.hss.view.shipping.search.ShippingSearchView;

public class ShippingOverviewPresenter extends AbstractPresenter {

	/**
	 * Ein neuer Akt (ÜbersichtsSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void backButtonClicked() {
		navigateToOverview();
	}

	/**
	 * Ein neuer Akt (NeuerLieferscheinSeite) wird erstellt - Bühnengerüst
	 * bleibt - Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void shippingNewButtonClicked() {
		Injector.setModelOrService(OrderDataContext.class, new OrderDataContext());
		navigateTo(ShippingNewView.class);
	}

	/**
	 * Ein neuer Akt (LieferscheinSuchSeite) wird erstellt - Bühnengerüst bleibt
	 * - Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void shippingSearchButtonClicked() {
		Injector.setModelOrService(OrderDataContext.class, new OrderDataContext());
		navigateTo(ShippingSearchView.class);
	}

}
