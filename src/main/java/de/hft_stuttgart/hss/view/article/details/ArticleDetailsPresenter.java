package de.hft_stuttgart.hss.view.article.details;

import java.util.Locale;

import javax.inject.Inject;

import de.hft_stuttgart.hss.view.common.domain.context.ArticleContext;
import de.hft_stuttgart.hss.view.common.domain.data.ArticleSearch;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.util.converter.CurrencyStringConverter;
import javafx.util.converter.NumberStringConverter;

public class ArticleDetailsPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	TextField articleId;
	@FXML
	TextField articleName;
	@FXML
	TextArea description;
	@FXML
	TextField producer;
	@FXML
	TextField eanId;
	@FXML
	TextField purchasePrice;
	@FXML
	TextField salesPrice;
	@FXML
	TextField stock;
	@FXML
	TextField recorderPoint;
	@FXML
	TextField productGroup;
	@FXML
	TextField unity;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 */
	@Inject
	ArticleContext context;

	@Override
	protected void init() {
		ArticleSearch article = context.getArticle();

		articleId.textProperty().bindBidirectional(article.articleIdProperty(), new NumberStringConverter());
		articleName.textProperty().bind(article.articleNameProperty());
		description.textProperty().bind(article.descriptionProperty());
		producer.textProperty().bind(article.producerProperty());
		eanId.textProperty().bind(article.eanIdProperty());
		purchasePrice.textProperty().bindBidirectional(article.purchasePriceProperty(), new CurrencyStringConverter(Locale.GERMANY));
		salesPrice.textProperty().bindBidirectional(article.salesPriceProperty(), new CurrencyStringConverter(Locale.GERMANY));
		stock.textProperty().bindBidirectional(article.stockProperty(), new NumberStringConverter());
		recorderPoint.textProperty().bindBidirectional(article.recorderPointProperty(), new NumberStringConverter());
		productGroup.textProperty().bind(article.productGroupProperty());
		unity.textProperty().bind(article.unityProperty());
	}

	/**
	 * Popup-Fenster wird bei Methodenaufruf geschlossen
	 */
	public void closeButtonClicked() {
		viewManager.getDialogStage().close();
	}
}