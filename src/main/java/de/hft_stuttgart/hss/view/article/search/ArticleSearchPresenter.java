package de.hft_stuttgart.hss.view.article.search;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import de.hft_stuttgart.hss.services.ArticleService;
import de.hft_stuttgart.hss.view.article.details.ArticleDetailsView;
import de.hft_stuttgart.hss.view.article.overview.ArticleOverviewView;
import de.hft_stuttgart.hss.view.common.domain.context.ArticleContext;
import de.hft_stuttgart.hss.view.common.domain.data.ArticleSearch;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.util.converter.CurrencyStringConverter;

public class ArticleSearchPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	TableView<ArticleSearch> articleTable;
	@FXML
	TextField search;
	@FXML
	Button detailButton;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung Framework,
	 * analyisiert das komplette Projekt beim Starten der Anwendung und
	 * speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	ArticleService articleService;
	@Inject
	ArticleContext context;

	@SuppressWarnings("unchecked")
	@Override
	protected void init() {
		List<ArticleSearch> allArticles = articleService.getAll();
		articleTable.setItems(FXCollections.observableArrayList(allArticles));

		ObservableList<TableColumn<ArticleSearch, ?>> columns = articleTable.getColumns();
		((TableColumn<ArticleSearch, Number>) columns.get(0)).setCellValueFactory((article) -> article.getValue().articleIdProperty());
		((TableColumn<ArticleSearch, String>) columns.get(1)).setCellValueFactory((article) -> article.getValue().articleNameProperty());
		((TableColumn<ArticleSearch, String>) columns.get(2)).setCellValueFactory((article) -> article.getValue().producerProperty());
		((TableColumn<ArticleSearch, Number>) columns.get(3)).setCellValueFactory((article) -> article.getValue().salesPriceProperty());
		((TableColumn<ArticleSearch, Number>) columns.get(3)).setCellFactory(TextFieldTableCell.forTableColumn(new CurrencyStringConverter(Locale.GERMANY)));
		((TableColumn<ArticleSearch, String>) columns.get(4)).setCellValueFactory((article) -> article.getValue().productGroupProperty());
		((TableColumn<ArticleSearch, Number>) columns.get(5)).setCellValueFactory((article) -> article.getValue().stockProperty());
		((TableColumn<ArticleSearch, String>) columns.get(6)).setCellValueFactory((article) -> article.getValue().unityProperty());

		onItemSelected(articleTable, t -> detailButton.setDisable(false));
	}

	/**
	 * Ein neuer Akt (ÜbersichtsSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void abortButtonClicked() {
		navigateToOverview();
	}

	/**
	 * Ein neuer Akt (ArtikelÜbersichtsSeite) wird erstellt - Bühnengerüst
	 * bleibt - Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void backButtonClicked() {
		navigateTo(ArticleOverviewView.class);
	}

	/**
	 * Methode öffnet ein neues Popup Fenster mit den entsprechenden Details des
	 * ausgewählten Artikels
	 */
	public void detailButtonClicked() {
		ArticleSearch articleSearch = getSelectedItem(articleTable);
		if (articleSearch != null) {
			context.setArticle(articleSearch);
			showCustomDialog("view.article.details", ArticleDetailsView.class);
		}
	}

	/**
	 * Methode zum Suchen von Artikeln
	 */
	public void search() {
		doSearch(search::getText, articleService::getAll, articleService::searchArticles, articleTable::setItems);
	}

	/**
	 * Methode prüft auf Doppelklick und ruft danach eine weitere Methode auf
	 */
	public void mouseClicked(MouseEvent event) {
		if (event.getClickCount() == 2) {
			detailButtonClicked();
		}
	}

}
