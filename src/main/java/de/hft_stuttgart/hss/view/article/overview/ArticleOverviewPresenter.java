package de.hft_stuttgart.hss.view.article.overview;

import de.hft_stuttgart.hss.view.article.search.ArticleSearchView;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;

public class ArticleOverviewPresenter extends AbstractPresenter {

	/**
	 * Ein neuer Akt (ÜbersichtsSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void backButtonClicked() {
		navigateToOverview();
	}

	/**
	 * Ein neuer Akt (ArtikelSucheSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void articleSearchButtonClicked() {
		navigateTo(ArticleSearchView.class);
	}

}
