package de.hft_stuttgart.hss.view.common.domain.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class User {

	private StringProperty username = new SimpleStringProperty();
	private StringProperty password = new SimpleStringProperty();

	public String getUsername() {
		return this.username.get();
	}

	public void setUsername(String username) {
		this.username.set(username);
	}

	public StringProperty usernameProperty() {
		return username;
	}

	public String getPassword() {
		return this.password.get();
	}

	public void setPassword(String password) {
		this.password.set(password);
	}

	public StringProperty PasswordProperty() {
		return password;
	}
}
