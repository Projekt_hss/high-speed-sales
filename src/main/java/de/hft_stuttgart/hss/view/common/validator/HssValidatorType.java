package de.hft_stuttgart.hss.view.common.validator;

public enum HssValidatorType {
	OBJECT, STRING, DOUBLE, INT, DATE, EMAIL, TEL;
}
