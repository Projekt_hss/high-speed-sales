package de.hft_stuttgart.hss.view.common.domain.data;

import java.time.LocalDate;

import de.hft_stuttgart.hss.entities.CustomerEntity;
import de.hft_stuttgart.hss.entities.ShippingOrderEntity;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ShippingSearch {

	private IntegerProperty shippingId;
	private ObjectProperty<LocalDate> shippingDate;
	private IntegerProperty orderId;
	private IntegerProperty customerId;
	private StringProperty company;
	private StringProperty lastname;
	private StringProperty prename;

	public ShippingSearch(ShippingOrderEntity shipping) {
		this.shippingId = new SimpleIntegerProperty(shipping.getShippingOrderId());
		this.shippingDate = new SimpleObjectProperty<>(shipping.getShippingOrderDate());
		this.orderId = new SimpleIntegerProperty(shipping.getOrder().getOrderId());
		CustomerEntity customer = shipping.getOrder().getCustomer();
		this.customerId = new SimpleIntegerProperty(customer.getCustomerId());
		this.company = new SimpleStringProperty(customer.getCompany());
		this.lastname = new SimpleStringProperty(customer.getLastname());
		this.prename = new SimpleStringProperty(customer.getPrename());
	}

	public IntegerProperty shippingIdProperty() {
		return shippingId;
	}

	public Integer getShippingId() {
		return shippingId.get();
	}

	public ObjectProperty<LocalDate> shippingDateProperty() {
		return shippingDate;
	}

	public IntegerProperty orderIdProperty() {
		return orderId;
	}

	public IntegerProperty customerIdProperty() {
		return customerId;
	}

	public StringProperty companyProperty() {
		return company;
	}

	public StringProperty lastnameProperty() {
		return lastname;
	}

	public StringProperty prenameProperty() {
		return prename;
	}

}
