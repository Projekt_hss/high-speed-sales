package de.hft_stuttgart.hss.view.common.misc;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.util.StringConverter;

public class EnumStringConverter<T extends Enum<T>> extends StringConverter<T> {

	private Map<String, T> values = new HashMap<>();
	private ResourceBundle resources;
	private String prefix;

	public EnumStringConverter(ResourceBundle resources, String prefix) {
		this.resources = resources;
		this.prefix = prefix;
	}

	@Override
	public String toString(T object) {
		String value = resources.getString(prefix + object.toString());

		if (!values.containsKey(value)) {
			values.put(value, object);
		}

		return value;
	}

	@Override
	public T fromString(String string) {
		return values.get(string);
	}

}
