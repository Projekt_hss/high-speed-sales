package de.hft_stuttgart.hss.view.common.domain.data;

import de.hft_stuttgart.hss.entities.CustomerEntity;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CustomerSearch {
	private IntegerProperty customerId = new SimpleIntegerProperty();
	private StringProperty company = new SimpleStringProperty();
	private StringProperty lastname = new SimpleStringProperty();
	private StringProperty prename = new SimpleStringProperty();
	private StringProperty street = new SimpleStringProperty();
	private StringProperty zip = new SimpleStringProperty();
	private StringProperty city = new SimpleStringProperty();
	private StringProperty number = new SimpleStringProperty();

	public CustomerSearch() {
	}

	public CustomerSearch(CustomerEntity customer) {
		this.customerId.set(customer.getCustomerId());
		this.company.set(customer.getCompany());
		this.lastname.set(customer.getLastname());
		this.prename.set(customer.getPrename());
		this.street.set(customer.getAddress().getStreet());
		this.number.set(customer.getAddress().getNumber());
		this.zip.set(customer.getAddress().getLocation().getZip());
		this.city.set(customer.getAddress().getLocation().getCity());
	}

	public IntegerProperty customerIdProperty() {
		return customerId;
	}

	public Integer getCustomerId() {
		return customerId.get();
	}

	public StringProperty companyProperty() {
		return company;
	}

	public StringProperty lastnameProperty() {
		return lastname;
	}

	public String getLastname() {
		return lastname.get();
	}

	public StringProperty prenameProperty() {
		return prename;
	}

	public String getPrename() {
		return prename.get();
	}

	public StringProperty streetProperty() {
		return street;
	}

	public StringProperty zipProperty() {
		return zip;
	}

	public StringProperty cityProperty() {
		return city;
	}

	public StringProperty numberProperty() {
		return number;
	}

}
