package de.hft_stuttgart.hss.view.common.domain.context;

import de.hft_stuttgart.hss.view.common.domain.data.ArticleSearch;

public class ArticleContext {

	private ArticleSearch article;

	public void setArticle(ArticleSearch articleSearch) {
		article = articleSearch;
	}

	public ArticleSearch getArticle() {
		return article;
	}
}
