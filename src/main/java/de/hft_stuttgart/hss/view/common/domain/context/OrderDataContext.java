package de.hft_stuttgart.hss.view.common.domain.context;

import java.util.List;

import de.hft_stuttgart.hss.entities.OrderState;
import de.hft_stuttgart.hss.view.common.domain.data.OrderPosition;

public class OrderDataContext {

	private List<OrderPosition> addedPositions;
	private OrderState[] states;
	private OrderContext orderContext;

	public List<OrderPosition> getAddedPositions() {
		return addedPositions;
	}

	public void setAddedPositions(List<OrderPosition> addedPositions) {
		this.addedPositions = addedPositions;
	}

	public OrderState[] getStates() {
		return states;
	}

	public void setStates(OrderState[] states) {
		this.states = states;
	}

	public OrderContext getOrderContext() {
		return orderContext;
	}

	public void setOrderContext(OrderContext order) {
		this.orderContext = order;
	}


}
