package de.hft_stuttgart.hss.view.common.main.rootwindow;

import java.util.Locale;

import javax.inject.Inject;

import de.hft_stuttgart.hss.services.LoginService;
import de.hft_stuttgart.hss.view.common.login.LoginView;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.common.main.help.HelpView;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class RootWindowPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	MenuBar menubar;
	@FXML
	Menu menubarLanguage;
	@FXML
	TitledPane buttomPane;
	@FXML
	BorderPane rootGrid;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	LoginService loginService;

	@Override
	protected void init() {
		viewManager.setRootGrid(rootGrid);
		displayLanguage();
		navigateToOverview();
	}

	/**
	 * Die Sprache in der Menüleiste wird angezeigt
	 */
	public void displayLanguage() {
		menubarLanguage.setVisible(true);
	}

	/**
	 * Die Sprache wird auf Englisch umgestellt
	 */
	public void changeLangToEnglisch() {
		/*
		 * Es wird überprüft, ob die bisherige Sprache der Englischen
		 * entspricht, wenn nicht wird die Sprache auf Englisch gesetzt und das
		 * Bühnengerüst neu erzeugt
		 */
		if (!Locale.UK.equals(Locale.getDefault())) {
			Locale.setDefault(Locale.UK);
			RootWindowView rootWindowView = new RootWindowView();
			viewManager.getPrimaryStage().getScene().setRoot(rootWindowView.getView());
		}
	}

	/**
	 * Die Sprache wird auf Deutsch umgestellt
	 */
	public void changeLangToGerman() {
		/*
		 * Es wird überprüft, ob die bisherige Sprache der Deutschen entspricht,
		 * wenn nicht wird die Sprache auf Deutsch gesetzt und das Bühnengerüst
		 * neu erzeugt
		 */
		if (!Locale.GERMANY.equals(Locale.getDefault())) {
			Locale.setDefault(Locale.GERMANY);
			RootWindowView rootWindowView = new RootWindowView();
			viewManager.getPrimaryStage().getScene().setRoot(rootWindowView.getView());
		}
	}

	/**
	 * Ausloggen und zum Anmeldefenster zurückkehren
	 * 
	 */
	public void logout() {
		// siehe LoginPresenter bzgl. Beschreibung der einzelnen
		// Befehle
		Stage stage = viewManager.getPrimaryStage();
		stage.close();
		Stage stageNew = new Stage();
		viewManager.setPrimaryStage(stageNew);
		LoginView loginPage = new LoginView();
		Scene scene = new Scene(loginPage.getView());
		stageNew.setScene(scene);
		stageNew.setTitle("High Speed Sales - Login");
		stageNew.setResizable(false);
		stageNew.setMaximized(false);
		stageNew.setHeight(500);
		stageNew.setWidth(300);
		stageNew.centerOnScreen();
		stageNew.getIcons().add(new Image(getClass().getResourceAsStream("highSpeed_Icon.png")));
		stageNew.show();
	}


	/**
	 * Beendet das Programm
	 */
	public void exit() {
		Platform.exit();
	}

	/**
	 * Erzeugt ein Popup Fenster und zeigt die Info Seite an
	 */
	public void helpAbout() {
		showCustomDialog("menu.help.text", HelpView.class);
	}


}
