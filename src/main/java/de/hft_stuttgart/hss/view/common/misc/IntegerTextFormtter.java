package de.hft_stuttgart.hss.view.common.misc;

import java.text.NumberFormat;
import java.text.ParsePosition;

import javafx.scene.control.TextFormatter;

public class IntegerTextFormtter extends TextFormatter<Object> {

	public IntegerTextFormtter() {
		super(c -> {
			if (c.getControlNewText().isEmpty()) {
				return c;
			}

			
			ParsePosition parsePosition = new ParsePosition(0);
			Object object = NumberFormat.getIntegerInstance().parse(c.getControlNewText(), parsePosition);

			if (object == null || parsePosition.getIndex() < c.getControlNewText().length()) {
				return null;
			} else {
				return c;
			}
		});
	}
}
