package de.hft_stuttgart.hss.view.common.domain.data;

import de.hft_stuttgart.hss.entities.CustomerEntity;
import de.hft_stuttgart.hss.entities.TitleType;
import de.hft_stuttgart.hss.services.AddressAware;
import de.hft_stuttgart.hss.services.misc.HssUtils;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CustomerDetails implements AddressAware {
	private IntegerProperty customerId = new SimpleIntegerProperty();
	private StringProperty company = new SimpleStringProperty();
	private StringProperty lastname = new SimpleStringProperty();
	private StringProperty prename = new SimpleStringProperty();
	private StringProperty street = new SimpleStringProperty();
	private StringProperty number = new SimpleStringProperty();
	private StringProperty zip = new SimpleStringProperty();
	private StringProperty city = new SimpleStringProperty();
	private StringProperty fax = new SimpleStringProperty();
	private StringProperty mobile = new SimpleStringProperty();
	private StringProperty phone = new SimpleStringProperty();
	private StringProperty mail = new SimpleStringProperty();
	private ObjectProperty<TitleType> title = new SimpleObjectProperty<>();
	private DoubleProperty discount = new SimpleDoubleProperty();

	public CustomerDetails() {
	}

	public CustomerDetails(CustomerEntity customer) {
		this.customerId.set(customer.getCustomerId());
		this.company.set(customer.getCompany());
		this.lastname.set(customer.getLastname());
		this.prename.set(customer.getPrename());
		this.street.set(customer.getAddress().getStreet());
		this.number.set(customer.getAddress().getNumber());
		this.zip.set(customer.getAddress().getLocation().getZip());
		this.city.set(customer.getAddress().getLocation().getCity());
		this.fax.set(customer.getFax());
		this.mobile.set(customer.getMobile());
		this.phone.set(customer.getPhone());
		this.mail.set(customer.getMail());
		this.title.set(customer.getTitle());
		this.discount.set(customer.getDiscount());
	}

	public Integer getCustomerId() {
		return this.customerId.get();
	}

	public void setCustomerId(Integer customerId) {
		this.customerId.set(customerId);
	}

	public IntegerProperty customerIdProperty() {
		return customerId;
	}

	public String getCompany() {
		return this.company.get();
	}

	public void setCompany(String company) {
		this.company.set(company);
	}

	public StringProperty companyProperty() {
		return company;
	}

	public String getLastname() {
		return this.lastname.get();
	}

	public void setLastname(String lastname) {
		this.lastname.set(lastname);
	}

	public StringProperty lastnameProperty() {
		return lastname;
	}

	public String getPrename() {
		return this.prename.get();
	}

	public void setPrename(String prename) {
		this.prename.set(prename);
	}

	public StringProperty prenameProperty() {
		return prename;
	}

	public String getName() {
		return HssUtils.concat(prename, lastname).get();
	}

	public String getStreet() {
		return this.street.get();
	}

	public void setStreet(String street) {
		this.street.set(street);
	}

	public StringProperty streetProperty() {
		return street;
	}

	public String getNumber() {
		return this.number.get();
	}

	public void setNumber(String number) {
		this.number.set(number);
	}

	public StringProperty numberProperty() {
		return number;
	}

	public String getStreetAndNumber() {
		return HssUtils.concat(street, number).get();
	}

	public String getZip() {
		return this.zip.get();
	}

	public void setZip(String zip) {
		this.zip.set(zip);
	}

	public StringProperty zipProperty() {
		return zip;
	}

	public String getCity() {
		return this.city.get();
	}

	public void setCity(String city) {
		this.city.set(city);
	}

	public StringProperty cityProperty() {
		return city;
	}

	public String getZipAndCity() {
		return HssUtils.concat(zip, city).get();
	}

	public String getPhone() {
		return this.phone.get();
	}

	public void setPhone(String phone) {
		this.phone.set(phone);
	}

	public StringProperty phoneProperty() {
		return phone;
	}

	public String getMobile() {
		return this.mobile.get();
	}

	public void setMobile(String mobile) {
		this.mobile.set(mobile);
	}

	public StringProperty mobileProperty() {
		return mobile;
	}

	public String getFax() {
		return this.fax.get();
	}

	public void setFax(String fax) {
		this.fax.set(fax);
	}

	public StringProperty faxProperty() {
		return fax;
	}

	public String getMail() {
		return this.mail.get();
	}

	public void setMail(String mail) {
		this.mail.set(mail);
	}

	public StringProperty mailProperty() {
		return mail;
	}

	public TitleType getTitle() {
		return title.get();
	}

	public void setTitle(TitleType title) {
		this.title.set(title);
	}

	public ObjectProperty<TitleType> titleProperty() {
		return title;
	}

	public void setDiscount(Double discount) {
		this.discount.set(discount);
	}

	public Double getDiscount() {
		return discount.get();
	}

	public DoubleProperty discountProperty() {
		return discount;
	}

}
