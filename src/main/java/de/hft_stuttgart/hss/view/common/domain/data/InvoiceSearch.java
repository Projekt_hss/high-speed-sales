package de.hft_stuttgart.hss.view.common.domain.data;

import java.time.LocalDate;

import de.hft_stuttgart.hss.entities.CustomerEntity;
import de.hft_stuttgart.hss.entities.InvoiceEntity;
import de.hft_stuttgart.hss.entities.PaymentType;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class InvoiceSearch {
	private IntegerProperty invoiceId = new SimpleIntegerProperty();
	private ObjectProperty<LocalDate> invoiceDate = new SimpleObjectProperty<>();
	private IntegerProperty orderId = new SimpleIntegerProperty();
	private DoubleProperty orderTotal = new SimpleDoubleProperty();
	private ObjectProperty<PaymentType> paymentType = new SimpleObjectProperty<>();
	private IntegerProperty customerId = new SimpleIntegerProperty();
	private StringProperty company = new SimpleStringProperty();
	private StringProperty lastname = new SimpleStringProperty();
	private StringProperty prename = new SimpleStringProperty();
	
	public InvoiceSearch() {
	}

	public InvoiceSearch(InvoiceEntity invoice) {
		this.invoiceId.set(invoice.getInvoiceId());
		this.invoiceDate.set(invoice.getInvoiceDate());
		this.orderId.set(invoice.getOrder().getOrderId());
		this.orderTotal.set(invoice.getOrder().getOrderTotal());
		this.paymentType.set(invoice.getOrder().getPaymentType());
		CustomerEntity customer = invoice.getOrder().getCustomer();
		this.customerId.set(customer.getCustomerId());
		this.company.set(customer.getCompany());
		this.lastname.set(customer.getLastname());
		this.prename.set(customer.getPrename());
	}

	public IntegerProperty invoiceIdProperty() {
		return invoiceId;
	}

	public Integer getInvoiceId() {
		return invoiceId.get();
	}

	public ObjectProperty<LocalDate> invoiceDateProperty() {
		return invoiceDate;
	}

	public LocalDate getInvoiceDate() {
		return invoiceDate.get();
	}

	public IntegerProperty orderIdProperty() {
		return orderId;
	}

	public Integer getOrderId() {
		return orderId.get();
	}

	public DoubleProperty orderTotalProperty() {
		return orderTotal;
	}

	public ObjectProperty<PaymentType> paymentTypeProperty() {
		return paymentType;
	}

	public IntegerProperty customerIdProperty() {
		return customerId;
	}

	public StringProperty companyProperty() {
		return company;
	}

	public StringProperty lastnameProperty() {
		return lastname;
	}

	public StringProperty prenameProperty() {
		return prename;
	}

}
