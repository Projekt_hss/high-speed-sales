package de.hft_stuttgart.hss.view.common.domain.context;

import java.util.stream.Collectors;

import de.hft_stuttgart.hss.entities.InvoiceEntity;
import de.hft_stuttgart.hss.entities.OrderEntity;
import de.hft_stuttgart.hss.entities.ShippingOrderEntity;
import de.hft_stuttgart.hss.view.common.domain.data.CustomerDetails;
import de.hft_stuttgart.hss.view.common.domain.data.InvoiceDetails;
import de.hft_stuttgart.hss.view.common.domain.data.OrderDetails;
import de.hft_stuttgart.hss.view.common.domain.data.OrderPosition;
import de.hft_stuttgart.hss.view.common.domain.data.ShippingDetails;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class OrderContext {

	private CustomerDetails customer;
	private ShippingDetails shipping;
	private ObservableList<OrderPosition> positions;
	private OrderDetails order;
	private InvoiceDetails invoice;

	public OrderContext() {
		this.positions = FXCollections.observableArrayList();
	}

	public OrderContext(ShippingOrderEntity shipping) {
		this(shipping.getOrder());
		this.shipping.extendShipping(shipping);
	}

	public OrderContext(InvoiceEntity invoice) {
		this(invoice.getOrder());
		this.invoice = new InvoiceDetails(invoice);
	}

	public OrderContext(OrderEntity order) {
		this.customer = new CustomerDetails(order.getCustomer());
		this.shipping = new ShippingDetails(order.getShippingAddress(), order.getShippingAddress() != order.getCustomer().getAddress());
		this.positions = FXCollections.observableArrayList(order.getOrderpositions().stream().map(pos -> new OrderPosition(pos)).collect(Collectors.toList()));
		this.order = new OrderDetails(order);
	}

	public boolean isEmpty() {
		return (customer == null) && (shipping == null) && (positions == null || positions.isEmpty()) && (order == null);
	}

	public CustomerDetails getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDetails customer) {
		this.customer = customer;
	}

	public ShippingDetails getShipping() {
		return shipping;
	}

	public void setShipping(ShippingDetails shipping) {
		this.shipping = shipping;
	}

	public ObservableList<OrderPosition> getPositions() {
		return positions;
	}

	public void setPositions(ObservableList<OrderPosition> positions) {
		this.positions = positions;
	}

	public OrderDetails getOrder() {
		return order;
	}

	public void setOrder(OrderDetails order) {
		this.order = order;
	}

	public InvoiceDetails getInvoice() {
		return invoice;
	}

	public void setInvoice(InvoiceDetails invoice) {
		this.invoice = invoice;
	}

}
