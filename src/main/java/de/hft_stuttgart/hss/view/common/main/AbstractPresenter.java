package de.hft_stuttgart.hss.view.common.main;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.inject.Inject;

import com.airhacks.afterburner.views.FXMLView;

import de.hft_stuttgart.hss.services.ViewManager;
import de.hft_stuttgart.hss.services.misc.ServiceLocator;
import de.hft_stuttgart.hss.view.common.main.overview.OverviewView;
import de.hft_stuttgart.hss.view.common.validator.HssValidator;
import de.hft_stuttgart.hss.view.common.validator.HssValidatorException;
import de.hft_stuttgart.hss.view.common.validator.HssValidatorField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public abstract class AbstractPresenter implements Initializable {


	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	protected ServiceLocator serviceLocator;
	@Inject
	protected ViewManager viewManager;
	@Inject
	protected HssValidator validator;

	// Deklaration lokaler Variablen
	protected ResourceBundle resources;
	private Map<HssValidatorField, Node> fields;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.resources = resources;
		this.fields = new HashMap<>();
		init();
	}

	/**
	 * Default Implementierung: macht nichts, außer die Kindklasse (die von
	 * Abstract extended) möchte etwas initialisieren
	 */
	protected void init() {

	}

	/**
	 * 
	 */
	protected void navigateToOverview() {
		navigateTo(OverviewView.class);
	}

	protected void navigateTo(Class<? extends FXMLView> view) {
		try {
			FXMLView fxmlView = view.newInstance();
			fxmlView.getView(viewManager.getRootGrid()::setCenter);
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			// Kann nicht passieren
		}
	}

	/**
	 * Markiert das entsprechende falsch eingegebene Feld
	 * 
	 */
	protected void markFieldAsInvalid(List<HssValidatorField> invalidFields) {
		for (HssValidatorField field : invalidFields) {
			fields.get(field).getStyleClass().add("invalid");
		}
	}

	/**
	 * 
	 */
	protected void addValidatorField(HssValidatorField field, Node node) {
		fields.put(field, node);
	}

	/**
	 * 
	 */
	protected void removeInvalidMarkerFromAllFields() {
		fields.forEach((validatorField, node) -> node.getStyleClass().remove("invalid"));
	}

	/**
	 * 
	 */
	protected Optional<ButtonType> showDialog(AlertType type, Supplier<ButtonType[]> buttonSupplier, String title, String contentText, Object... parameters) {
		Alert alert = new Alert(type);
		if (buttonSupplier != null) {
			alert.getButtonTypes().clear();
			alert.getButtonTypes().addAll(buttonSupplier.get());
		}
		alert.setTitle(resources.getString(title));
		alert.setHeaderText(null);
		alert.setContentText(String.format(resources.getString(contentText), parameters));
		alert.initOwner(viewManager.getPrimaryStage());
		return alert.showAndWait();
	}

	/**
	 * 
	 */
	protected Optional<ButtonType> showConfirmationDialog(String title, String contentText, Object... parameters) {
		return showDialog(AlertType.CONFIRMATION, () -> new ButtonType[] { ButtonType.YES, ButtonType.NO }, title, contentText, parameters);
	}

	/**
	 * 
	 */
	protected void showInfoDialog(String title, String contentText, Object... parameters) {
		showDialog(AlertType.INFORMATION, null, title, contentText, parameters);
	}

	protected void showErrorDialog(String title, String contentText, Object... parameters) {
		showDialog(AlertType.ERROR, null, title, contentText, parameters);
	}

	/**
	 * 
	 */
	protected void showCustomDialog(String title, Class<? extends FXMLView> view) {
		try {
			Stage dialogWindow = new Stage();
			dialogWindow.initOwner(viewManager.getPrimaryStage());
			dialogWindow.initModality(Modality.APPLICATION_MODAL);
			dialogWindow.setTitle(resources.getString(title));
			dialogWindow.setResizable(false);

			FXMLView fxmlView = view.newInstance();
			Scene scene = new Scene(fxmlView.getView());
			dialogWindow.setScene(scene);

			viewManager.setDialogStage(dialogWindow);
			dialogWindow.showAndWait();
			viewManager.setDialogStage(null);
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			// Kann nicht passieren
		}
	}

	/**
	 * 
	 */
	protected void validate(Object object, HssValidatorField[] fields, Consumer<Void> successHandler) {
		try {
			removeInvalidMarkerFromAllFields();
			validator.validate(object, fields);
			successHandler.accept(null);
		} catch (HssValidatorException e) {
			markFieldAsInvalid(e.getInvalidFields());
		}
	}

	/**
	 * 
	 */
	protected void showProgressDialog(Task<?> task, Consumer<WorkerStateEvent> handler) {
		if (task == null) {
			if (handler != null) {
				handler.accept(null);
			}
			return;
		}

		Stage progressWindow = new Stage();
		progressWindow.initStyle(StageStyle.UNDECORATED);
		progressWindow.initOwner(viewManager.getPrimaryStage());
		progressWindow.initModality(Modality.APPLICATION_MODAL);
		progressWindow.setTitle("Progress");
	
		ProgressBar bar = new ProgressBar();
		bar.progressProperty().bind(task.progressProperty());
		Label text = new Label();
		text.textProperty().bind(task.messageProperty());
		BorderPane pane = new BorderPane();
		pane.setCenter(bar);
		pane.setBottom(text);
	
		Scene scene = new Scene(pane);
		progressWindow.setScene(scene);

		task.exceptionProperty().addListener((ov, o, n) -> {
			progressWindow.close();
			n.printStackTrace();
			showDialog(AlertType.ERROR, null, "error.title", "error.text");
		});
	
		viewManager.setProgressStage(progressWindow);
		progressWindow.show();

		task.setOnSucceeded(t -> {
			viewManager.getProgressStage().close();
			viewManager.setProgressStage(null);
			if (handler != null) {
				handler.accept(t);
			}
		});
	}

	/**
	 * 
	 */
	protected <T> void doSearch(Supplier<String> searchValueSupplier, Supplier<List<T>> getAllSupplier, Function<String, List<T>> searchSupplier, Consumer<ObservableList<T>> tableConsumer) {
		String searchValue = searchValueSupplier.get();

		List<T> searchResult;
		if (searchValue == null || searchValue.trim().isEmpty()) {
			searchResult = getAllSupplier.get();
		} else {
			searchResult = searchSupplier.apply(searchValue);
		}

		tableConsumer.accept(FXCollections.observableArrayList(searchResult));
	}

	/**
	 * 
	 */
	protected void onItemSelected(TableView<?> table, Consumer<?> s) {
		table.getSelectionModel().selectedItemProperty().addListener((ov, o, n) -> s.accept(null));
	}

	/**
	 * 
	 */
	protected <T> T getSelectedItem(TableView<T> table) {
		return table.getSelectionModel().getSelectedItem();
	}

}
