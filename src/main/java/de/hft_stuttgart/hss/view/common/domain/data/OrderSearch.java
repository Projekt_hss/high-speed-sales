package de.hft_stuttgart.hss.view.common.domain.data;

import java.time.LocalDateTime;

import de.hft_stuttgart.hss.entities.OrderEntity;
import de.hft_stuttgart.hss.entities.OrderState;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class OrderSearch {
	private IntegerProperty orderId = new SimpleIntegerProperty();
	private ObjectProperty<LocalDateTime> orderDate = new SimpleObjectProperty<>();
	private IntegerProperty customerId = new SimpleIntegerProperty();
	private StringProperty company = new SimpleStringProperty();
	private StringProperty lastname = new SimpleStringProperty();
	private StringProperty prename = new SimpleStringProperty();
	private ObjectProperty<OrderState> state = new SimpleObjectProperty<>();
	private StringProperty employee = new SimpleStringProperty();

	public OrderSearch() {
	}

	public OrderSearch(OrderEntity order) {
		this.orderId.set(order.getOrderId());
		this.orderDate.set(order.getDate());
		this.customerId.set(order.getCustomer().getCustomerId());
		this.company.set(order.getCustomer().getCompany());
		this.lastname.set(order.getCustomer().getLastname());
		this.prename.set(order.getCustomer().getPrename());
		this.state.set(order.getState());
		this.employee.set(order.getCreator().getUsername());
	}

	public IntegerProperty orderIdProperty() {
		return orderId;
	}

	public Integer getOrderId() {
		return orderId.get();
	}

	public ObjectProperty<LocalDateTime> orderDateProperty() {
		return orderDate;
	}

	public IntegerProperty customerIdProperty() {
		return customerId;
	}

	public StringProperty companyProperty() {
		return company;
	}

	public StringProperty lastnameProperty() {
		return lastname;
	}

	public StringProperty prenameProperty() {
		return prename;
	}

	public ObjectProperty<OrderState> stateProperty() {
		return state;
	}

	public StringProperty employeeProperty() {
		return employee;
	}

}
