package de.hft_stuttgart.hss.view.common.domain.data;

import java.time.LocalDate;

import de.hft_stuttgart.hss.entities.AddressEntity;
import de.hft_stuttgart.hss.entities.ShippingOrderEntity;
import de.hft_stuttgart.hss.services.AddressAware;
import de.hft_stuttgart.hss.services.misc.HssUtils;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ShippingDetails implements AddressAware {
	private boolean extended;
	private BooleanProperty shippingAddressAvailable = new SimpleBooleanProperty();
	private StringProperty street = new SimpleStringProperty();
	private StringProperty number = new SimpleStringProperty();
	private StringProperty zip = new SimpleStringProperty();
	private StringProperty city = new SimpleStringProperty();
	private IntegerProperty shippingId = new SimpleIntegerProperty();
	private ObjectProperty<LocalDate> shippingDate = new SimpleObjectProperty<>();
	private StringProperty shippingCreator = new SimpleStringProperty();

	public ShippingDetails() {
	}

	public ShippingDetails(AddressEntity shippingAddress, boolean shippingAddressAvailable) {
		this.shippingAddressAvailable.set(shippingAddressAvailable);
		this.street.set(shippingAddress.getStreet());
		this.number.set(shippingAddress.getNumber());
		this.zip.set(shippingAddress.getLocation().getZip());
		this.city.set(shippingAddress.getLocation().getCity());
	}

	public void extendShipping(ShippingOrderEntity shipping) {
		setToExtended();
		this.shippingId.set(shipping.getShippingOrderId());
		this.shippingDate.set(shipping.getShippingOrderDate());
		this.shippingCreator.set(HssUtils.getCreatorName(shipping.getCreator()));
	}

	public boolean isExtended() {
		return extended;
	}

	public StringProperty streetProperty() {
		return street;
	}

	@Override
	public String getStreet() {
		return street.get();
	}

	public StringProperty numberProperty() {
		return number;
	}

	@Override
	public String getNumber() {
		return number.get();
	}

	public String getStreetAndNumber() {
		return HssUtils.concat(street, number).get();
	}

	public StringProperty zipProperty() {
		return zip;
	}

	@Override
	public String getZip() {
		return zip.get();
	}

	public StringProperty cityProperty() {
		return city;
	}

	@Override
	public String getCity() {
		return city.get();
	}

	public String getZipAndCity() {
		return HssUtils.concat(zip, city).get();
	}

	public BooleanProperty shippingAddressAvailableProperty() {
		return shippingAddressAvailable;
	}

	public Boolean isShippingAddressAvailable() {
		return shippingAddressAvailable.get();
	}

	public ObjectProperty<LocalDate> shippingDateProperty() {
		return shippingDate;
	}

	public LocalDate getShippingDate() {
		return shippingDate.get();
	}

	public void setShippingDate(ObjectProperty<LocalDate> shippingDate) {
		this.shippingDate = shippingDate;
	}

	public IntegerProperty shippingIdProperty() {
		return shippingId;
	}

	public void setShippingId(Integer shippingId) {
		this.shippingId.set(shippingId);
	}

	public Integer getShippingOrderId() {
		return shippingId.get();
	}

	public StringProperty shippingCreatorProperty() {
		return shippingCreator;
	}

	public void setShippingCreator(String shippingCreator) {
		this.shippingCreator.set(shippingCreator);
	}

	public String getShippingCreator() {
		return shippingCreator.get();
	}

	public void setToExtended() {
		this.extended = true;
	}
}
