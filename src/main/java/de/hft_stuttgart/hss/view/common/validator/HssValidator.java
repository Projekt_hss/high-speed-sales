package de.hft_stuttgart.hss.view.common.validator;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class HssValidator {

	private static final Pattern PHONE_PATTERN = Pattern.compile("^(?:([+][0-9]{1,2})+[ .-]*)?([(]{1}[0-9]{1,6}[)])?([0-9 .-/]{3,20})((x|ext|extension)[ ]?[0-9]{1,4})?$");
	private static final Pattern EMAIL_PATTERN = Pattern.compile(".+@.+\\..+");

	/**
	 * Validiert den Wert eines Feldes
	 * 
	 * @param obj
	 * @param fields
	 * @throws HssValidatorException
	 */
	public void validate(Object obj, HssValidatorField[] fields) throws HssValidatorException {
		List<HssValidatorField> invalidFields = new ArrayList<>();
		for (HssValidatorField field : fields) {
			Object value = field.getValue(obj);
			boolean required = field.isRequired(obj);
			// Required Felder werden immer auf leer geprüft
			HssValidatorField validationField = validateObjectNotNull(field, required, value);
			if (validationField == null) {
				// Wenn Feld nicht leer, dann je nach Typ noch weitere
				// Validierungschritte.
				switch (field.getType()) {
				case STRING:
					validationField = validateStringProperty(field, required, value);
					break;
				case DOUBLE:
					validationField = validateDoubleProperty(field, value);
					break;
				case INT:
					validationField = validateIntegerProperty(field, value);
					break;
				case DATE:
					validationField = validateDateProperty(field, value);
					break;
				case EMAIL:
					validationField = validateStringProperty(field, required, value);
					if (validationField == null) {
						validationField = validateEmailProperty(field, value);
					}
					break;
				case TEL:
					validationField = validateStringProperty(field, required, value);
					if (validationField == null) {
						validationField = validatePhoneProperty(field, value);
					}
					break;
				default:
					break;
				}
			}
			if (validationField != null) {
				invalidFields.add(validationField);
			}
		}

		if (!invalidFields.isEmpty()) {
			throw new HssValidatorException(invalidFields);
		}
	}

	private HssValidatorField validatePhoneProperty(HssValidatorField field, Object value) {
		if (value != null && !((String) value).trim().isEmpty()) {
			Matcher matcher = PHONE_PATTERN.matcher((String) value);
			if (!matcher.find()) {
				return field;
			}
		}
		return null;
	}

	private HssValidatorField validateEmailProperty(HssValidatorField field, Object value) {
		if (value != null && !((String) value).trim().isEmpty()) {
			Matcher matcher = EMAIL_PATTERN.matcher((String) value);
			if (!matcher.find()) {
				return field;
			}
		}
		return null;
	}

	private HssValidatorField validateDateProperty(HssValidatorField field, Object value) {
		Object secondaryInfo = field.getSecondaryInfo();
		if (secondaryInfo != null && secondaryInfo instanceof Long) {
			if (((LocalDate) value).isBefore(LocalDate.now().minusDays((long) secondaryInfo))) {
				return field;
			}
		}
		return null;
	}

	private HssValidatorField validateStringProperty(HssValidatorField field, boolean required, Object value) {
		if (required && ((String) value).trim().isEmpty()) {
			return field;
		}
		return null;
	}

	private HssValidatorField validateObjectNotNull(HssValidatorField field, boolean required, Object value) {
		if (required && value == null) {
			return field;
		}

		return null;
	}

	private HssValidatorField validateDoubleProperty(HssValidatorField field, Object value) {
		Double number = value instanceof Double ? (Double) value : Double.parseDouble(value.toString());
		if (number < 0) {
			return field;
		}

		return null;
	}

	private HssValidatorField validateIntegerProperty(HssValidatorField field, Object value) {
		Integer number = value instanceof Integer ? (Integer) value : Integer.parseInt(value.toString());
		if (number < 0) {
			return field;
		}

		return null;
	}

}

