package de.hft_stuttgart.hss.view.common.domain.data;

import java.time.LocalDate;

import de.hft_stuttgart.hss.entities.InvoiceEntity;
import de.hft_stuttgart.hss.services.misc.HssUtils;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class InvoiceDetails {
	private IntegerProperty invoiceId = new SimpleIntegerProperty();
	private ObjectProperty<LocalDate> invoiceDate = new SimpleObjectProperty<>();
	private StringProperty invoiceCreator = new SimpleStringProperty();

	public InvoiceDetails() {
	}

	public InvoiceDetails(InvoiceEntity invoice) {
		this.invoiceId.set(invoice.getInvoiceId());
		this.invoiceDate.set(invoice.getInvoiceDate());
		this.invoiceCreator.set(HssUtils.getCreatorName(invoice.getCreator()));
	}

	public IntegerProperty invoiceIdProperty() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId.set(invoiceId);
	}

	public Integer getInvoiceId() {
		return invoiceId.get();
	}

	public ObjectProperty<LocalDate> invoiceDateProperty() {
		return invoiceDate;
	}

	public LocalDate getInvoiceDate() {
		return invoiceDate.get();
	}

	public StringProperty invoiceCreatorProperty() {
		return invoiceCreator;
	}

	public void setInvoiceCreator(String invoiceCreator) {
		this.invoiceCreator.set(invoiceCreator);
	}

	public String getInvoiceCreator() {
		return invoiceCreator.get();
	}
}
