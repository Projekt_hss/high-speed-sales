package de.hft_stuttgart.hss.view.common.domain.context;

import de.hft_stuttgart.hss.view.common.domain.data.CustomerDetails;

public class CustomerContext {

	private CustomerDetails customer;

	public CustomerDetails getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDetails customer) {
		this.customer = customer;
	}

}
