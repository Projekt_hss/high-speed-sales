package de.hft_stuttgart.hss.view.common.misc;

import java.time.LocalDate;

import javafx.scene.control.DateCell;

public class DateBeforeCell extends DateCell {

	private long days;

	public DateBeforeCell(long days) {
		this.days = days;
	}

	@Override
	public void updateItem(LocalDate item, boolean empty) {
		super.updateItem(item, empty);

		if (item.isBefore(LocalDate.now().minusDays(days))) {
			setDisable(true);
			setStyle("-fx-background-color: #d0d0d0;");
		}
	}
}
