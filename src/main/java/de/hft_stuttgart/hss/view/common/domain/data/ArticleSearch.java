package de.hft_stuttgart.hss.view.common.domain.data;

import de.hft_stuttgart.hss.entities.ArticleEntity;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ArticleSearch {
	private IntegerProperty articleId = new SimpleIntegerProperty();
	private StringProperty articleName = new SimpleStringProperty();
	private StringProperty producer = new SimpleStringProperty();
	private StringProperty description = new SimpleStringProperty();
	private DoubleProperty salesPrice = new SimpleDoubleProperty();
	private DoubleProperty purchasePrice = new SimpleDoubleProperty();
	private StringProperty productGroup = new SimpleStringProperty();
	private DoubleProperty stock = new SimpleDoubleProperty();
	private DoubleProperty recorderPoint = new SimpleDoubleProperty();
	private StringProperty unity = new SimpleStringProperty();
	private StringProperty eanId = new SimpleStringProperty();

	public ArticleSearch() {
	}

	public ArticleSearch(ArticleEntity article) {
		this.articleId.set(article.getArticleId());
		this.articleName.set(article.getArticleName());
		this.producer.set(article.getProducer());
		this.description.set(article.getDescription());
		this.salesPrice.set(article.getSalesPrice());
		this.purchasePrice.set(article.getPurchasePrice());
		this.productGroup.set(article.getProductGroup());
		this.stock.set(article.getStock());
		this.recorderPoint.set(article.getRecorderPoint());
		this.unity.set(article.getUnity());
		this.eanId.set(article.getEanNummer());
	}

	public IntegerProperty articleIdProperty() {
		return articleId;
	}

	public Integer getArticleId() {
		return articleId.get();
	}

	public StringProperty articleNameProperty() {
		return articleName;
	}

	public String getArticleName() {
		return articleName.get();
	}

	public StringProperty producerProperty() {
		return producer;
	}

	public StringProperty descriptionProperty() {
		return description;
	}

	public DoubleProperty salesPriceProperty() {
		return salesPrice;
	}

	public Double getSalesPrice() {
		return salesPrice.get();
	}

	public DoubleProperty purchasePriceProperty() {
		return purchasePrice;
	}

	public StringProperty productGroupProperty() {
		return productGroup;
	}

	public DoubleProperty stockProperty() {
		return stock;
	}

	public DoubleProperty recorderPointProperty() {
		return recorderPoint;
	}

	public StringProperty unityProperty() {
		return unity;
	}

	public String getUnity() {
		return unity.get();
	}

	public StringProperty eanIdProperty() {
		return eanId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((articleId == null) ? 0 : articleId.getValue().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass()) {
			if (obj instanceof OrderPosition) {
				OrderPosition position = (OrderPosition) obj;
				if (position.articleIdProperty().isEqualTo(articleId).get()) {
					return true;
				}
			}
			return false;
		} else {
			ArticleSearch other = (ArticleSearch) obj;
			if (articleId == null) {
				if (other.articleId != null)
					return false;
			} else if (!articleId.isEqualTo(other.articleId).get())
				return false;
			return true;
		}
	}

}
