package de.hft_stuttgart.hss.view.common.domain.data;

import java.time.LocalDateTime;

import de.hft_stuttgart.hss.entities.OrderEntity;
import de.hft_stuttgart.hss.entities.PaymentType;
import de.hft_stuttgart.hss.entities.ShippingType;
import de.hft_stuttgart.hss.services.misc.HssUtils;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class OrderDetails {

	private IntegerProperty orderId = new SimpleIntegerProperty();
	private ObjectProperty<LocalDateTime> orderDate = new SimpleObjectProperty<>();
	private ObjectProperty<PaymentType> paymentType = new SimpleObjectProperty<>();
	private ObjectProperty<ShippingType> shippingType = new SimpleObjectProperty<>();
	private BooleanProperty installation = new SimpleBooleanProperty();
	private DoubleProperty vat = new SimpleDoubleProperty();
	private DoubleProperty vatTotal = new SimpleDoubleProperty();
	private DoubleProperty discount = new SimpleDoubleProperty();
	private DoubleProperty discountTotal = new SimpleDoubleProperty();
	private DoubleProperty positionTotal = new SimpleDoubleProperty();
	private DoubleProperty orderSum = new SimpleDoubleProperty();
	private DoubleProperty orderTotal = new SimpleDoubleProperty();
	private StringProperty creator = new SimpleStringProperty();

	public OrderDetails() {
		vat.set(0.19);
	}

	public OrderDetails(OrderEntity order) {
		this.orderId.set(order.getOrderId());
		this.orderDate.set(order.getDate());
		this.paymentType.set(order.getPaymentType());
		this.shippingType.set(order.getShippingType());
		this.installation.set(order.getInstallation());
		this.vat.set(order.getVat());
		this.vatTotal.set(order.getVatTotal());
		this.orderTotal.set(order.getOrderTotal());
		this.discount.set(order.getDiscount());
		this.discountTotal.set(order.getDiscountTotal());
		this.positionTotal.set(order.getPositionTotal());
		this.orderSum.bind(Bindings.subtract(orderTotal, vatTotal));
		this.creator.set(HssUtils.getCreatorName(order.getCreator()));
	}

	public IntegerProperty orderIdProperty() {
		return orderId;
	}

	public Integer getOrderId() {
		return orderId.get();
	}

	public ObjectProperty<LocalDateTime> orderDateProperty() {
		return orderDate;
	}

	public LocalDateTime getOrderDate() {
		return orderDate.get();
	}

	public ObjectProperty<PaymentType> paymentTypeProperty() {
		return paymentType;
	}

	public PaymentType getPaymentType() {
		return paymentType.get();
	}

	public ObjectProperty<ShippingType> shippingTypeProperty() {
		return shippingType;
	}

	public ShippingType getShippingType() {
		return shippingType.get();
	}

	public BooleanProperty installationProperty() {
		return installation;
	}

	public Boolean getInstallation() {
		return installation.get();
	}

	public void setInstallation(Boolean installation) {
		this.installation.set(installation);
	}

	public DoubleProperty positionTotalProperty() {
		return positionTotal;
	}

	public Double getPositionTotal() {
		return positionTotal.get();
	}

	public DoubleProperty discountProperty() {
		return discount;
	}

	public Double getDiscount() {
		return discount.get();
	}

	public void setDiscount(Double discount) {
		this.discount.set(discount);
	}

	public DoubleProperty discountTotalProperty() {
		return discountTotal;
	}

	public Double getDiscountTotal() {
		return discountTotal.get();
	}

	public DoubleProperty vatProperty() {
		return vat;
	}

	public Double getVat() {
		return vat.get();
	}

	public DoubleProperty vatTotalProperty() {
		return vatTotal;
	}

	public Double getVatTotal() {
		return vatTotal.get();
	}

	public DoubleProperty orderSumProperty() {
		return orderSum;
	}

	public Double getOrderSum() {
		return orderSum.get();
	}

	public DoubleProperty orderTotalProperty() {
		return orderTotal;
	}

	public Double getOrderTotal() {
		return orderTotal.get();
	}

	public StringProperty creatorProperty() {
		return creator;
	}

	public String getCreator() {
		return creator.get();
	}

}
