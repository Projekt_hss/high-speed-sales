package de.hft_stuttgart.hss.view.common.validator;

import java.util.List;

public class HssValidatorException extends Exception {

	private static final long serialVersionUID = 1L;
	private List<HssValidatorField> invalidFields;

	public HssValidatorException(List<HssValidatorField> invalidFields) {
		super();
		this.invalidFields = invalidFields;
	}

	public List<HssValidatorField> getInvalidFields() {
		return invalidFields;
	}
}
