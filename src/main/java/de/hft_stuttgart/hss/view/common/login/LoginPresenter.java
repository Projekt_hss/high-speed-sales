package de.hft_stuttgart.hss.view.common.login;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.inject.Inject;

import de.hft_stuttgart.hss.services.LoginService;
import de.hft_stuttgart.hss.services.ViewManager;
import de.hft_stuttgart.hss.services.misc.ServiceLocator;
import de.hft_stuttgart.hss.view.common.domain.data.User;
import de.hft_stuttgart.hss.view.common.main.rootwindow.RootWindowView;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class LoginPresenter implements Initializable {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	Label message;
	@FXML
	TextField username;
	@FXML
	PasswordField password;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	ServiceLocator serviceLocator;
	@Inject
	ViewManager viewManager;
	@Inject
	LoginService loginService;

	// lokale Variable deklarieren
	private User user;

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// Erstellt neues User Objekt
		user = new User();
		// Erstellt eine Bidirectionale Verbindung zwischen Eingabefeld und
		// Objekt, dadurch werden alle Eingaben in der UI automatisch mit dem
		// Objekt synchronisiert
		username.textProperty().bindBidirectional(user.usernameProperty());
		password.textProperty().bindBidirectional(user.PasswordProperty());
	}

	/**
	 * Methode wird aufgerufen, wenn der Login Button geklickt wird Methode
	 * erzeugt eine neue Bühne (Stage) und schließt die alte.
	 */
	public void loginButtonClicked() {
		boolean isAuthenticated = loginService.doLogin(user);
		if (isAuthenticated) {
			// Neue Stage wird erzeugt
			Stage stage = createApplicationWindow();
			// Bisherige Stage wird geschlossen
			viewManager.getPrimaryStage().close();
			// Neue Stage wird angezeigt
			stage.show();
			// Neue Stage wird auf PrimaryStage gesetzt
			viewManager.setPrimaryStage(stage);
		} else {
			/*
			 * Fehlermessage anzeigen (FXML Label Message ist im FXML auf
			 * Visible(false)/hide gesetzt und wird angezeigt, wenn der Login
			 * fehlgeschlagen ist.)
			 */
			message.setVisible(true);
			/*
			 * Es wird überprüft, ob im UserEingabeTextfeld der Username leer
			 * ist, wenn ja wird der Fokus auf das UserEingabeTextfeld gelegt
			 * (Cursor ist im Feld drin). Wenn ein Username eingegeben ist, geht
			 * der Fokus auf das PasswortEingabeFeld
			 */
			if (user.getUsername() == null || user.getUsername().isEmpty()) {
				username.requestFocus();
			} else {
				password.requestFocus();
			}
		}
	}

	/**
	 * Eine neue Szene wird erstellt - Bühnengerüst bleibt - Szene auf der Bühne
	 * ändert sich. Overwiew sagt aus das im Package ein overview.fxml,
	 * overview.css und der OverviewPresenter gefunden wird
	 */
	private Stage createApplicationWindow() {
		// Neues Bühnengerüst wird erzeugt
		RootWindowView rootPage = new RootWindowView();
		// Neue Szene auf der Bühne wird erzeugt
		Scene scene = new Scene(rootPage.getView());
		// Neue Stage wird erzeugt
		Stage stage = new Stage();
		// Der Stage wird der Akt/Szene zugeordnet
		stage.setScene(scene);
		// Der Titel für das Fenster wird gesetzt
		stage.setTitle("High Speed Sales ");
		// Fenster Größe kann verändert werden
		stage.setResizable(true);
		// Fenster Minimal-Höhe und -Breite werden gesetzt
		stage.setMinHeight(850);
		stage.setMinWidth(1300);
		// Fenster wird auf Maximal Größe gesetzt
		stage.setMaximized(true);
		// Fenster wird in die Mitte gesetzt.
		stage.centerOnScreen();
		// Der Stage wird das Icon (Links oben in der Menüleiste) zugewiesen
		stage.getIcons().add(new Image(getClass().getResourceAsStream("highSpeed_Icon.png")));
		// Stage wird zurückgegeben
		return stage;
	}

	/**
	 * Die Sprache wird auf Englisch umgestellt
	 */
	public void changeLangToEnglisch() {
		// Es wird überprüft, ob die bisherige Sprache der Englischen
		// entspricht, wenn nicht wird die Sprache auf Englisch gesetzt
		if (!Locale.UK.equals(Locale.getDefault())) {
			Locale.setDefault(Locale.UK);
			// Methode showNewLoginWindow wird aufgerufen, um neue Stage zu
			// erzeugen.
			showNewLoginWindow();
		}
	}

	/**
	 * Die Sprache wird auf Deutsch umgestellt
	 */
	public void changeLangToGerman() {
		// Es wird überprüft, ob die bisherige Sprache der Deutschen
		// entspricht, wenn nicht wird die Sprache auf Deutsch gesetzt
		if (!Locale.GERMANY.equals(Locale.getDefault())) {
			Locale.setDefault(Locale.GERMANY);
			// Methode showNewLoginWindow wird aufgerufen, um neue Stage zu
			// erzeugen.
			showNewLoginWindow();
		}

	}

	/**
	 * Methode wird aufgerufen, wenn bei der Anmeldemaske die Sprache umgestellt
	 * wird. Es ist zu beachten das die Sprache nicht währen des Betriebs
	 * geändert werden kann. Das bedeutet des die Stage neu gestartet werden
	 * muss damit eine Sprachänderung wirksam wird.
	 */
	private void showNewLoginWindow() {
		// siehe createApplicationWindow bzgl. Beschreibung der einzelnen
		// Befehle
		Stage stage = viewManager.getPrimaryStage();
		stage.close();
		Stage stageNew = new Stage();
		viewManager.setPrimaryStage(stageNew);
		LoginView loginPage = new LoginView();
		Scene scene = new Scene(loginPage.getView());
		stageNew.setScene(scene);
		stageNew.setTitle("High Speed Sales - Login");
		stageNew.setResizable(false);
		stageNew.setMaximized(false);
		stageNew.setHeight(500);
		stageNew.setWidth(300);
		stageNew.centerOnScreen();
		stageNew.getIcons().add(new Image(getClass().getResourceAsStream("highSpeed_Icon.png")));
		stageNew.show();
	}

}
