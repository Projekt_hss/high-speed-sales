package de.hft_stuttgart.hss.view.common.validator;

public interface HssValidatorField {
	Object getValue(Object obj);

	HssValidatorType getType();

	boolean isRequired(Object obj);

	public default Object getSecondaryInfo() {
		return null;
	}
}
