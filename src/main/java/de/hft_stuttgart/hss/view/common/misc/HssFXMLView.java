package de.hft_stuttgart.hss.view.common.misc;

import com.airhacks.afterburner.views.FXMLView;

public class HssFXMLView extends FXMLView {
	@Override
	public Void exceptionReporter(Throwable t) {
		t.printStackTrace();
		return null;
	}
}
