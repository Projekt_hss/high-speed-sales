package de.hft_stuttgart.hss.view.common.misc;

import java.text.DecimalFormat;
import java.text.ParsePosition;

import javafx.scene.control.TextFormatter;

public class DoubleTextFormtter extends TextFormatter<Double> {

	public DoubleTextFormtter() {
		super(c -> {
			if (c.getControlNewText().isEmpty()) {
				return c;
			}

			ParsePosition parsePosition = new ParsePosition(0);
			Object object = new DecimalFormat("#.0").parse(c.getControlNewText(), parsePosition);

			if (object == null || parsePosition.getIndex() < c.getControlNewText().length()) {
				return null;
			} else {
				return c;
			}
		});
	}
}
