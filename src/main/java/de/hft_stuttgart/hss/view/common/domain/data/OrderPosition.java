package de.hft_stuttgart.hss.view.common.domain.data;

import de.hft_stuttgart.hss.entities.OrderPositionEntity;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class OrderPosition {

	private Integer orderPositionid;
	private IntegerProperty position = new SimpleIntegerProperty();
	private IntegerProperty articleId = new SimpleIntegerProperty();
	private StringProperty articleName = new SimpleStringProperty();
	private DoubleProperty amount = new SimpleDoubleProperty();
	private StringProperty unity = new SimpleStringProperty();
	private DoubleProperty singlePrice = new SimpleDoubleProperty();

	public OrderPosition() {
	}

	public OrderPosition(OrderPositionEntity orderPosition) {
		this(orderPosition.getNumber(), orderPosition.getArticle().getArticleId(), orderPosition.getArticle().getArticleName(), orderPosition.getAmount(), orderPosition.getArticle().getUnity(),
				orderPosition.getArticle().getSalesPrice());
		orderPositionid = orderPosition.getOrderPositionId();
	}

	public OrderPosition(ArticleSearch article) {
		this.articleId.set(article.getArticleId());
		this.articleName.set(article.getArticleName());
		this.amount.set(1.0);
		this.unity.set(article.getUnity());
		this.singlePrice.set(article.getSalesPrice());
	}

	public OrderPosition(Integer position, Integer articleId, String articleName, Double amount, String unity, Double singlePrice) {
		this.position.set(position);
		this.articleId.set(articleId);
		this.articleName.set(articleName);
		this.amount.set(amount);
		this.unity.set(unity);
		this.singlePrice.set(singlePrice);
	}

	public Integer getOrderPositionid() {
		return orderPositionid;
	}

	public IntegerProperty positionProperty() {
		return position;
	}

	public IntegerProperty articleIdProperty() {
		return articleId;
	}

	public StringProperty articleNameProperty() {
		return articleName;
	}

	public DoubleProperty amountProperty() {
		return amount;
	}

	public double getAmount() {
		return amount.get();
	}

	public void setAmount(Double amount) {
		this.amount.set(amount);
	}

	public StringProperty unityProperty() {
		return unity;
	}

	public DoubleProperty singlePriceProperty() {
		return singlePrice;
	}

	public double getSinglePrice() {
		return singlePrice.get();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((articleId == null) ? 0 : articleId.getValue().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderPosition other = (OrderPosition) obj;
		if (articleId == null) {
			if (other.articleId != null)
				return false;
		} else if (!articleId.isEqualTo(other.articleId).get())
			return false;
		return true;
	}


}
