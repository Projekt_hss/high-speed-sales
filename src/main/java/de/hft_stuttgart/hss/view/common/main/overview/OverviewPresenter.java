package de.hft_stuttgart.hss.view.common.main.overview;

import de.hft_stuttgart.hss.view.article.overview.ArticleOverviewView;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.customer.overview.CustomerOverviewView;
import de.hft_stuttgart.hss.view.invoice.overview.InvoiceOverviewView;
import de.hft_stuttgart.hss.view.order.overview.OrderOverviewView;
import de.hft_stuttgart.hss.view.shipping.overview.ShippingOverviewView;

public class OverviewPresenter extends AbstractPresenter {

	/**
	 * Ein neuer Akt (KundenÜbersichtsSeite) wird erstellt - Bühnengerüst bleibt
	 * - Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void customerButtonClicked() {
		navigateTo(CustomerOverviewView.class);
	}

	/**
	 * Ein neuer Akt (AuftragsÜbersichtsSeite)wird erstellt - Bühnengerüst
	 * bleibt - Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void orderButtonClicked() {
		navigateTo(OrderOverviewView.class);
	}

	/**
	 * Ein neuer Akt (LieferscheinÜbersichtsSeite)wird erstellt - Bühnengerüst
	 * bleibt - Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void shippingButtonClicked() {
		navigateTo(ShippingOverviewView.class);
	}

	/**
	 * Ein neuer Akt (ArtikelÜbersichtsSeite) wird erstellt - Bühnengerüst
	 * bleibt - Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void articleButtonClicked() {
		navigateTo(ArticleOverviewView.class);
	}

	/**
	 * Ein neuer Akt (RechnungÜbersichtsSeite) wird erstellt - Bühnengerüst
	 * bleibt - Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void billButtonClicked() {
		navigateTo(InvoiceOverviewView.class);
	}

}
