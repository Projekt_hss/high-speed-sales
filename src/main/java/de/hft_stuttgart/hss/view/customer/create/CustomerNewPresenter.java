package de.hft_stuttgart.hss.view.customer.create;

import static de.hft_stuttgart.hss.view.customer.create.CustomerNewField.CITY;
import static de.hft_stuttgart.hss.view.customer.create.CustomerNewField.COMPANY;
import static de.hft_stuttgart.hss.view.customer.create.CustomerNewField.DISCOUNT;
import static de.hft_stuttgart.hss.view.customer.create.CustomerNewField.FAX;
import static de.hft_stuttgart.hss.view.customer.create.CustomerNewField.LASTNAME;
import static de.hft_stuttgart.hss.view.customer.create.CustomerNewField.MAIL;
import static de.hft_stuttgart.hss.view.customer.create.CustomerNewField.MOBILE;
import static de.hft_stuttgart.hss.view.customer.create.CustomerNewField.NUMBER;
import static de.hft_stuttgart.hss.view.customer.create.CustomerNewField.PHONE;
import static de.hft_stuttgart.hss.view.customer.create.CustomerNewField.PRENAME;
import static de.hft_stuttgart.hss.view.customer.create.CustomerNewField.STREET;
import static de.hft_stuttgart.hss.view.customer.create.CustomerNewField.TITLE;
import static de.hft_stuttgart.hss.view.customer.create.CustomerNewField.ZIP;

import javax.inject.Inject;

import de.hft_stuttgart.hss.entities.TitleType;
import de.hft_stuttgart.hss.services.CustomerService;
import de.hft_stuttgart.hss.view.common.domain.context.CustomerContext;
import de.hft_stuttgart.hss.view.common.domain.data.CustomerDetails;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.common.main.overview.OverviewView;
import de.hft_stuttgart.hss.view.common.misc.EnumStringConverter;
import de.hft_stuttgart.hss.view.common.misc.IntegerTextFormtter;
import de.hft_stuttgart.hss.view.common.misc.PercentageTextFormtter;
import de.hft_stuttgart.hss.view.customer.overview.CustomerOverviewView;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.util.converter.PercentageStringConverter;

public class CustomerNewPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	TextField company;
	@FXML
	TextField lastname;
	@FXML
	TextField prename;
	@FXML
	TextField street;
	@FXML
	TextField zip;
	@FXML
	TextField city;
	@FXML
	TextField phone;
	@FXML
	TextField fax;
	@FXML
	TextField number;
	@FXML
	TextField mobil;
	@FXML
	TextField mail;
	@FXML
	TextField discount;
	@FXML
	ComboBox<TitleType> title;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	CustomerService customerService;
	@Inject
	CustomerContext context;

	@Override
	protected void init() {
		if (context.getCustomer() == null) {
			context.setCustomer(new CustomerDetails());
		}

		setCustomer(context.getCustomer());
		
		title.getItems().addAll(TitleType.values());
		title.setConverter(new EnumStringConverter<>(resources, "customer.title."));

		discount.setTextFormatter(new PercentageTextFormtter());
		discount.focusedProperty().addListener((ov, oldV, newV) -> {
			String text = discount.getText();
			if (text == null || text.isEmpty()) {
				discount.setText("0");
			}
		});
		zip.setTextFormatter(new IntegerTextFormtter());

		addValidatorField(CITY, city);
		addValidatorField(COMPANY, company);
		addValidatorField(DISCOUNT, discount);
		addValidatorField(FAX, fax);
		addValidatorField(LASTNAME, lastname);
		addValidatorField(MAIL, mail);
		addValidatorField(MOBILE, mobil);
		addValidatorField(NUMBER, number);
		addValidatorField(PHONE, phone);
		addValidatorField(PRENAME, prename);
		addValidatorField(STREET, street);
		addValidatorField(TITLE, title);
		addValidatorField(ZIP, zip);
	}

	/**
	 * Ein neuer Akt (ÜbersichtsSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void abortButtonClicked() {
		navigateTo(OverviewView.class);
	}

	/**
	 * Ein neuer Akt (KundeÜbersichtsSeite) wird erstellt - Bühnengerüst bleibt
	 * - Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void backButtonClicked() {
		navigateTo(CustomerOverviewView.class);
	}

	/**
	 * Kunde wird bei Methodenaufruf abgespeichert
	 */
	public void saveCustomer() {
		CustomerDetails customer = context.getCustomer();
		validate(customer, CustomerNewField.values(), t -> {
			customerService.saveCustomer(customer);

			// Zeige Info Dialog an
			showInfoDialog("dialog.customer.create.title", "dialog.customer.create.text", customer.getPrename(), customer.getLastname());

			navigateToOverview();
		});
	}

	/**
	 * Zuordnung der Variablen
	 */
	public void setCustomer(CustomerDetails customer) {
		company.textProperty().bindBidirectional(customer.companyProperty());
		lastname.textProperty().bindBidirectional(customer.lastnameProperty());
		prename.textProperty().bindBidirectional(customer.prenameProperty());
		street.textProperty().bindBidirectional(customer.streetProperty());
		number.textProperty().bindBidirectional(customer.numberProperty());
		zip.textProperty().bindBidirectional(customer.zipProperty());
		city.textProperty().bindBidirectional(customer.cityProperty());
		phone.textProperty().bindBidirectional(customer.phoneProperty());
		fax.textProperty().bindBidirectional(customer.faxProperty());
		mail.textProperty().bindBidirectional(customer.mailProperty());
		mobil.textProperty().bindBidirectional(customer.mobileProperty());
		title.valueProperty().bindBidirectional(customer.titleProperty());
		discount.textProperty().bindBidirectional(customer.discountProperty(), new PercentageStringConverter());
	}

}

