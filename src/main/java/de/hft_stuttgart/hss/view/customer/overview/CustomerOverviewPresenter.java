package de.hft_stuttgart.hss.view.customer.overview;

import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.customer.create.CustomerNewView;
import de.hft_stuttgart.hss.view.customer.search.CustomerSearchView;

public class CustomerOverviewPresenter extends AbstractPresenter {

	/**
	 * Ein neuer Akt (ÜbersichtsSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void backButtonClicked() {
		navigateToOverview();
	}

	/**
	 * Ein neuer Akt (NeuerKundeSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void customerNewButtonClicked() {
		navigateTo(CustomerNewView.class);
	}

	/**
	 * Ein neuer Akt (KundeSuchSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void customerSearchButtonClicked() {
		navigateTo(CustomerSearchView.class);
	}

}
