package de.hft_stuttgart.hss.view.customer.search;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import de.hft_stuttgart.hss.services.CustomerService;
import de.hft_stuttgart.hss.view.common.domain.context.CustomerContext;
import de.hft_stuttgart.hss.view.common.domain.data.CustomerDetails;
import de.hft_stuttgart.hss.view.common.domain.data.CustomerSearch;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.customer.edit.CustomerEditView;
import de.hft_stuttgart.hss.view.customer.overview.CustomerOverviewView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class CustomerSearchPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	TableView<CustomerSearch> customerTable;
	@FXML
	TextField search;
	@FXML
	Button editButton;
	@FXML
	Button deleteButton;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	CustomerService customerService;
	@Inject
	CustomerContext context;

	@SuppressWarnings("unchecked")
	@Override
	protected void init() {
		List<CustomerSearch> allCustomers = customerService.getAll();
		customerTable.setItems(FXCollections.observableArrayList(allCustomers));

		ObservableList<TableColumn<CustomerSearch, ?>> columns = customerTable.getColumns();
		((TableColumn<CustomerSearch, Number>) columns.get(0)).setCellValueFactory((customer) -> customer.getValue().customerIdProperty());
		((TableColumn<CustomerSearch, String>) columns.get(1)).setCellValueFactory((customer) -> customer.getValue().companyProperty());
		((TableColumn<CustomerSearch, String>) columns.get(2)).setCellValueFactory((customer) -> customer.getValue().lastnameProperty());
		((TableColumn<CustomerSearch, String>) columns.get(3)).setCellValueFactory((customer) -> customer.getValue().prenameProperty());
		((TableColumn<CustomerSearch, String>) columns.get(4)).setCellValueFactory((customer) -> customer.getValue().streetProperty());
		((TableColumn<CustomerSearch, String>) columns.get(5)).setCellValueFactory((customer) -> customer.getValue().numberProperty());
		((TableColumn<CustomerSearch, String>) columns.get(6)).setCellValueFactory((customer) -> customer.getValue().zipProperty());
		((TableColumn<CustomerSearch, String>) columns.get(7)).setCellValueFactory((customer) -> customer.getValue().cityProperty());

		onItemSelected(customerTable, t -> {
			editButton.setDisable(false);
			deleteButton.setDisable(false);
		});
	}

	/**
	 * Ein neuer Akt (KundeÜbersichtsSeite) wird erstellt - Bühnengerüst bleibt
	 * - Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void backButtonClicked() {
		navigateTo(CustomerOverviewView.class);
	}

	/**
	 * Ein neuer Akt (ÜbersichtsSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void abortButtonClicked() {
		navigateToOverview();
	}

	/**
	 * Methode zum Suchen der Kunden
	 */
	public void search() {
		doSearch(search::getText, customerService::getAll, customerService::searchCustomer, customerTable::setItems);

		editButton.setDisable(true);
		deleteButton.setDisable(true);
	}

	/**
	 * Die Details des ausgewählten Kunden, werden in einem neuen Popup-Fenster
	 * angezeigt
	 */
	public void editButtonClicked() {
		CustomerSearch customerSearch = customerTable.getSelectionModel().getSelectedItem();
		CustomerDetails customerDetails = customerService.getCustomer(customerSearch.getCustomerId());
		context.setCustomer(customerDetails);
		navigateTo(CustomerEditView.class);
	}

	/**
	 * Der ausgewählte Kunde wird nach positiver Bestätigung des Dialogs
	 * gelöscht
	 */
	public void deleteButtonClicked() {
		CustomerSearch customer = customerTable.getSelectionModel().getSelectedItem();

		Optional<ButtonType> result = showConfirmationDialog("dialog.customer.search.delete.title", "dialog.customer.search.delete.text", customer.getPrename(), customer.getLastname());
		if (result.get() == ButtonType.YES) {
			try {
				customerService.deleteCustomer(customer.getCustomerId());
				customerTable.getItems().remove(customer);

			} catch (Exception e) {
				showErrorDialog("dialog.customer.search.cannot-delete.title", "dialog.customer.search.cannot-delete.text", customer.getPrename(), customer.getLastname());

			}

		}
	}

}
