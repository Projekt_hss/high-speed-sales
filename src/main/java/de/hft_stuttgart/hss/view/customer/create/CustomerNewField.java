package de.hft_stuttgart.hss.view.customer.create;

import static de.hft_stuttgart.hss.view.common.validator.HssValidatorType.DOUBLE;
import static de.hft_stuttgart.hss.view.common.validator.HssValidatorType.EMAIL;
import static de.hft_stuttgart.hss.view.common.validator.HssValidatorType.INT;
import static de.hft_stuttgart.hss.view.common.validator.HssValidatorType.OBJECT;
import static de.hft_stuttgart.hss.view.common.validator.HssValidatorType.STRING;
import static de.hft_stuttgart.hss.view.common.validator.HssValidatorType.TEL;

import de.hft_stuttgart.hss.view.common.domain.data.CustomerDetails;
import de.hft_stuttgart.hss.view.common.validator.HssValidatorField;
import de.hft_stuttgart.hss.view.common.validator.HssValidatorType;

public enum CustomerNewField implements HssValidatorField {
	COMPANY(false, STRING) {
		@Override
		public Object getValue(Object customer) {
			return ((CustomerDetails) customer).getCompany();
		}
	},
	LASTNAME(true, STRING) {
		@Override
		public Object getValue(Object customer) {
			return ((CustomerDetails) customer).getLastname();
		}
	},
	PRENAME(true, STRING) {
		@Override
		public Object getValue(Object customer) {
			return ((CustomerDetails) customer).getPrename();
		}
	},
	DISCOUNT(true, DOUBLE) {
		@Override
		public Object getValue(Object customer) {
			return ((CustomerDetails) customer).getDiscount();
		}
	},
	STREET(true, STRING) {
	@Override
		public Object getValue(Object customer) {
			return ((CustomerDetails) customer).getStreet();
		}
	
	},
	NUMBER(true, STRING) {
		@Override
		public Object getValue(Object customer) {
			return ((CustomerDetails) customer).getNumber();
		}

	},
	ZIP(true, INT) {
	@Override
		public Object getValue(Object customer) {
			return ((CustomerDetails) customer).getZip();
		}
	
	},
	CITY(true, STRING) {
		@Override
		public Object getValue(Object customer) {
			return ((CustomerDetails) customer).getCity();
		}

	},
	PHONE(false, TEL) {
		@Override
		public Object getValue(Object customer) {
			return ((CustomerDetails) customer).getPhone();
		}

	},
	MOBILE(false, TEL) {
		@Override
		public Object getValue(Object customer) {
			return ((CustomerDetails) customer).getMobile();
		}

	},
	FAX(false, TEL) {
		@Override
		public Object getValue(Object customer) {
			return ((CustomerDetails) customer).getFax();
		}

	},
	MAIL(false, EMAIL) {
		@Override
		public Object getValue(Object customer) {
			return ((CustomerDetails) customer).getMail();
		}

	},

	TITLE(true, OBJECT) {
		@Override
		public Object getValue(Object customer) {
			return ((CustomerDetails) customer).getTitle();
		}

	};


	private boolean required;
	private HssValidatorType type;

	private CustomerNewField(boolean required, HssValidatorType type) {
		this.required = required;
		this.type = type;
	}


	@Override
	public boolean isRequired(Object obj) {
		return required;
	}

	@Override
	public HssValidatorType getType() {
		return type;
	}
}
