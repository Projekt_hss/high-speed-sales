package de.hft_stuttgart.hss.view.order.create.position;

import java.util.Locale;
import java.util.Optional;

import javax.inject.Inject;

import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.domain.context.OrderDataContext;
import de.hft_stuttgart.hss.view.common.domain.data.OrderPosition;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.common.misc.DoubleTextFieldTableCell;
import de.hft_stuttgart.hss.view.order.common.position.search.OrderPositionArticleSearchView;
import de.hft_stuttgart.hss.view.order.create.shipping.OrderNewShippingView;
import de.hft_stuttgart.hss.view.order.create.sum.OrderNewSumView;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.CurrencyStringConverter;
import javafx.util.converter.NumberStringConverter;

public class OrderNewPositionPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	TableView<OrderPosition> orderPositionTable;
	@FXML
	Button removePositionButton;
	@FXML
	Button forwardButton;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	OrderDataContext dataContext;

	// Deklaration lokale Variable
	private OrderContext context;

	@SuppressWarnings("unchecked")
	@Override
	protected void init() {
		context = dataContext.getOrderContext();

		if (context.getPositions() == null) {
			context.setPositions(FXCollections.observableArrayList());
		}
		
		if (!context.getPositions().isEmpty()) {
			forwardButton.setDisable(false);
		}

		orderPositionTable.setItems(context.getPositions());

		ObservableList<TableColumn<OrderPosition, ?>> columns = orderPositionTable.getColumns();
		((TableColumn<OrderPosition, Number>) columns.get(0)).setCellValueFactory((position) -> position.getValue().positionProperty());
		((TableColumn<OrderPosition, Number>) columns.get(1)).setCellValueFactory((position) -> position.getValue().articleIdProperty());
		((TableColumn<OrderPosition, String>) columns.get(2)).setCellValueFactory((position) -> position.getValue().articleNameProperty());
		((TableColumn<OrderPosition, Number>) columns.get(3)).setCellValueFactory((position) -> position.getValue().amountProperty());
		((TableColumn<OrderPosition, Number>) columns.get(3)).setCellFactory(DoubleTextFieldTableCell.forTableColumn(new NumberStringConverter()));
		((TableColumn<OrderPosition, String>) columns.get(4)).setCellValueFactory((position) -> position.getValue().unityProperty());
		((TableColumn<OrderPosition, Number>) columns.get(5)).setCellValueFactory((position) -> position.getValue().singlePriceProperty());
		((TableColumn<OrderPosition, Number>) columns.get(5)).setCellFactory(TextFieldTableCell.forTableColumn(new CurrencyStringConverter(Locale.GERMANY)));
		((TableColumn<OrderPosition, Number>) columns.get(6)).setCellValueFactory((position) -> Bindings.multiply(position.getValue().singlePriceProperty(), position.getValue().amountProperty()));
		((TableColumn<OrderPosition, Number>) columns.get(6)).setCellFactory(TextFieldTableCell.forTableColumn(new CurrencyStringConverter(Locale.GERMANY)));

		onItemSelected(orderPositionTable, t -> removePositionButton.setDisable(false));
	}

	/**
	 * Popup-Fenster wird aufgerufen um Artikel zum Auftrag hinzufügen zu können
	 */
	public void addPositionButtonClicked() {
		showCustomDialog("dialog.order.create.position.search", OrderPositionArticleSearchView.class);

		if (!context.getPositions().isEmpty()) {
			forwardButton.setDisable(false);
			checkAddedPositinNumbers();
			orderPositionTable.sort();
		}
	}

	/**
	 * Die Positionsnummern der einzelnen hinzugefügten Artikel werden vergeben
	 */
	private void checkAddedPositinNumbers() {
		int actualSize = context.getPositions().size() - dataContext.getAddedPositions().size();

		for (int i = 0; i < dataContext.getAddedPositions().size(); i++) {
			OrderPosition position = dataContext.getAddedPositions().get(i);
			position.positionProperty().set(actualSize + i + 1);
		}
	}

	/**
	 * Artikelposition löschen und anschließend Überprüfung der Positionsnummer
	 */
	public void removePositionButtonClicked() {
		OrderPosition orderPosition = getSelectedItem(orderPositionTable);
		context.getPositions().remove(orderPosition);
		removePositionButton.setDisable(true);
		checkPositinNumbersAfterDelete(orderPosition.positionProperty().get());
		if (context.getPositions().isEmpty()) {
			forwardButton.setDisable(true);
		}
	}

	/**
	 * Positionsnummer werden nach Löschen eines Artikels erneut überprüft und
	 * ggf. verändert
	 */
	private void checkPositinNumbersAfterDelete(int removedNumber) {
		for (OrderPosition position : context.getPositions()) {
			IntegerProperty positionProperty = position.positionProperty();

			if (positionProperty.get() > removedNumber) {
				positionProperty.set(positionProperty.get() - 1);
			}
		}
	}

	/**
	 * Popup-Fenster über die Abfrage ob die Auftragserstellung wirklich
	 * abgebrochen werden soll
	 */
	public void abortButtonClicked() {
		Optional<ButtonType> result = showConfirmationDialog("dialog.order.create.abort.title", "dialog.order.create.abort.text");
		if (result.get() == ButtonType.YES) {
			navigateToOverview();
		}
	}

	/**
	 * Der Auftragswizard geht einen Punkt zurück --> von Position zu
	 * Lieferanschrift
	 */
	public void backButtonClicked() {
		navigateTo(OrderNewShippingView.class);
	}

	/**
	 * Der nächste Punkt im Auftragswizard wird angezeigt (Summe)
	 */
	public void forwardButtonClicked() {
		navigateTo(OrderNewSumView.class);
	}

}
