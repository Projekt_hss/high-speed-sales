package de.hft_stuttgart.hss.view.order.details;

import java.util.HashMap;
import java.util.Locale;

import javax.inject.Inject;

import de.hft_stuttgart.hss.services.ExportService;
import de.hft_stuttgart.hss.services.misc.ExportType;
import de.hft_stuttgart.hss.services.misc.HssUtils;
import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.domain.context.OrderDataContext;
import de.hft_stuttgart.hss.view.common.domain.data.OrderPosition;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.common.misc.EnumStringConverter;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.BooleanStringConverter;
import javafx.util.converter.CurrencyStringConverter;
import javafx.util.converter.NumberStringConverter;
import javafx.util.converter.PercentageStringConverter;

public class OrderDetailsPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	TableView<OrderPosition> orderPositionTable;
	@FXML
	TextField customerId;
	@FXML
	TextField company;
	@FXML
	TextField name;
	@FXML
	TextField street;
	@FXML
	TextField street2;
	@FXML
	TextField city;
	@FXML
	TextField city2;
	@FXML
	TextField orderId;
	@FXML
	TextField fax;
	@FXML
	TextField mobile;
	@FXML
	TextField phone;
	@FXML
	TextField mail;
	@FXML
	TextField shippingType;
	@FXML
	TextField paymentType;
	@FXML
	TextField installation;
	@FXML
	TextField sum;
	@FXML
	TextField discount;
	@FXML
	TextField preTax;
	@FXML
	TextField afterTax;
	@FXML
	TextField vat;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	OrderDataContext context;
	@Inject
	ExportService exportService;

	@SuppressWarnings("unchecked")
	@Override
	protected void init() {
		OrderContext orderContext = context.getOrderContext();

		customerId.textProperty().bindBidirectional(orderContext.getCustomer().customerIdProperty(), new NumberStringConverter());
		company.textProperty().bind(orderContext.getCustomer().companyProperty());
		name.textProperty().bind(HssUtils.concat(orderContext.getCustomer().prenameProperty(), orderContext.getCustomer().lastnameProperty()));
		street.textProperty().bind(HssUtils.concat(orderContext.getCustomer().streetProperty(), orderContext.getCustomer().numberProperty()));
		city.textProperty().bind(HssUtils.concat(orderContext.getCustomer().zipProperty(), orderContext.getCustomer().cityProperty()));
		fax.textProperty().bind(orderContext.getCustomer().faxProperty());
		mobile.textProperty().bind(orderContext.getCustomer().mobileProperty());
		phone.textProperty().bind(orderContext.getCustomer().phoneProperty());
		mail.textProperty().bind(orderContext.getCustomer().mailProperty());

		street2.textProperty().bind(HssUtils.concat(orderContext.getShipping().streetProperty(), orderContext.getShipping().numberProperty()));
		city2.textProperty().bind(HssUtils.concat(orderContext.getShipping().zipProperty(), orderContext.getShipping().cityProperty()));

		orderId.textProperty().bindBidirectional(orderContext.getOrder().orderIdProperty(), new NumberStringConverter());
		sum.textProperty().bindBidirectional(orderContext.getOrder().positionTotalProperty(), new CurrencyStringConverter(Locale.GERMANY));
		afterTax.textProperty().bindBidirectional(orderContext.getOrder().orderTotalProperty(), new CurrencyStringConverter(Locale.GERMANY));
		shippingType.textProperty().bindBidirectional(orderContext.getOrder().shippingTypeProperty(), new EnumStringConverter<>(resources, "order.shipping.type."));
		paymentType.textProperty().bindBidirectional(orderContext.getOrder().paymentTypeProperty(), new EnumStringConverter<>(resources, "order.payment.type."));
		installation.textProperty().bindBidirectional(orderContext.getOrder().installationProperty(), new BooleanStringConverter());
		vat.textProperty().bindBidirectional(orderContext.getOrder().vatProperty(), new PercentageStringConverter());
		discount.textProperty().bindBidirectional(orderContext.getOrder().discountProperty(), new PercentageStringConverter());
		preTax.textProperty().bindBidirectional(orderContext.getOrder().orderSumProperty(), new CurrencyStringConverter(Locale.GERMANY));

		orderPositionTable.setItems(orderContext.getPositions());

		ObservableList<TableColumn<OrderPosition, ?>> columns = orderPositionTable.getColumns();
		((TableColumn<OrderPosition, Number>) columns.get(0)).setCellValueFactory((position) -> position.getValue().positionProperty());
		((TableColumn<OrderPosition, Number>) columns.get(1)).setCellValueFactory((position) -> position.getValue().articleIdProperty());
		((TableColumn<OrderPosition, String>) columns.get(2)).setCellValueFactory((position) -> position.getValue().articleNameProperty());
		((TableColumn<OrderPosition, Number>) columns.get(3)).setCellValueFactory((position) -> position.getValue().amountProperty());
		((TableColumn<OrderPosition, Number>) columns.get(4)).setCellValueFactory((position) -> position.getValue().singlePriceProperty());
		((TableColumn<OrderPosition, Number>) columns.get(4)).setCellFactory(TextFieldTableCell.forTableColumn(new CurrencyStringConverter(Locale.GERMANY)));
		((TableColumn<OrderPosition, Number>) columns.get(5)).setCellValueFactory((position) -> Bindings.multiply(position.getValue().singlePriceProperty(), position.getValue().amountProperty()));
		((TableColumn<OrderPosition, Number>) columns.get(5)).setCellFactory(TextFieldTableCell.forTableColumn(new CurrencyStringConverter(Locale.GERMANY)));
	}

	/**
	 * Popup-Fenster wird geschlossen
	 */
	public void closeButtonClicked() {
		viewManager.getDialogStage().close();
	}

	/**
	 * Auftrag kann in PDF exportiert werden
	 */
	public void printOrder() {
		exportService.export(ExportType.ORDER, context.getOrderContext(), resources::getString, this::showProgressDialog, null, viewManager::getDialogStage, new HashMap<>());
	}

}
