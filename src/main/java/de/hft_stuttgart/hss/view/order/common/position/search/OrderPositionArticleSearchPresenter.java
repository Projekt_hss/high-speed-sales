package de.hft_stuttgart.hss.view.order.common.position.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.inject.Inject;

import de.hft_stuttgart.hss.services.ArticleService;
import de.hft_stuttgart.hss.view.common.domain.context.OrderDataContext;
import de.hft_stuttgart.hss.view.common.domain.data.ArticleSearch;
import de.hft_stuttgart.hss.view.common.domain.data.OrderPosition;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.util.converter.CurrencyStringConverter;
import javafx.util.converter.NumberStringConverter;

public class OrderPositionArticleSearchPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	TableView<ArticleSearch> articleTable;
	@FXML
	TextField search;
	@FXML
	Button selectButton;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	OrderDataContext context;
	@Inject
	ArticleService articleService;

	@SuppressWarnings("unchecked")
	@Override
	protected void init() {
		articleTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		List<Integer> articleIds = context.getOrderContext().getPositions().parallelStream().map((pos) -> pos.articleIdProperty().get()).collect(Collectors.toList());
		List<ArticleSearch> allArticles;
		if (articleIds.isEmpty()) {
			allArticles = articleService.getAll();
		} else {
			allArticles = articleService.getAllNotContained(articleIds);
		}

		if (allArticles.isEmpty()) {
			search.setDisable(true);
			selectButton.setDisable(true);
		} else {
			articleTable.setItems(FXCollections.observableArrayList(allArticles));
		}

		ObservableList<TableColumn<ArticleSearch, ?>> columns = articleTable.getColumns();
		((TableColumn<ArticleSearch, Number>) columns.get(0)).setCellValueFactory((article) -> article.getValue().articleIdProperty());
		((TableColumn<ArticleSearch, String>) columns.get(1)).setCellValueFactory((article) -> article.getValue().articleNameProperty());
		((TableColumn<ArticleSearch, String>) columns.get(2)).setCellValueFactory((article) -> article.getValue().descriptionProperty());
		((TableColumn<ArticleSearch, String>) columns.get(3)).setCellValueFactory((article) -> article.getValue().producerProperty());
		((TableColumn<ArticleSearch, Number>) columns.get(4)).setCellValueFactory((article) -> article.getValue().salesPriceProperty());
		((TableColumn<ArticleSearch, Number>) columns.get(4)).setCellFactory(TextFieldTableCell.forTableColumn(new CurrencyStringConverter(Locale.GERMANY)));
		((TableColumn<ArticleSearch, String>) columns.get(5)).setCellValueFactory((article) -> article.getValue().productGroupProperty());
		((TableColumn<ArticleSearch, String>) columns.get(6)).setCellValueFactory((article) -> article.getValue().unityProperty());
		((TableColumn<ArticleSearch, Number>) columns.get(7)).setCellValueFactory((article) -> article.getValue().stockProperty());
		((TableColumn<ArticleSearch, Number>) columns.get(7)).setCellFactory(TextFieldTableCell.forTableColumn(new NumberStringConverter()));
		((TableColumn<ArticleSearch, Number>) columns.get(8)).setCellValueFactory((article) -> article.getValue().recorderPointProperty());
		((TableColumn<ArticleSearch, Number>) columns.get(8)).setCellFactory(TextFieldTableCell.forTableColumn(new NumberStringConverter()));
	}

	/**
	 * Popup-Fenster wird geschlossen
	 */
	public void abortButtonClicked() {
		viewManager.getDialogStage().close();
	}

	/**
	 * Der ausgewählte Datensatz wird übernommen und Popup-Fenster wird
	 * geschlossen
	 */
	public void selectButtonClicked() {
		ObservableList<ArticleSearch> selectedItems = articleTable.getSelectionModel().getSelectedItems();

		ObservableList<OrderPosition> orderPositions = context.getOrderContext().getPositions();
		List<OrderPosition> addedPositions = new ArrayList<>();
		context.setAddedPositions(addedPositions);

		selectedItems.forEach(article -> {
			if (!orderPositions.contains(article)) {
				OrderPosition newPos = new OrderPosition(article);
				orderPositions.add(newPos);
				addedPositions.add(newPos);
			}
		});

		abortButtonClicked();
	}

	/**
	 * Suchen nach Artikeln
	 */
	public void search() {
		doSearch(search::getText, articleService::getAll, articleService::searchArticles, articleTable::setItems);
	}

	/**
	 * Methode prüft auf Doppelklick und ruft danach eine weitere Methode auf
	 */
	public void mouseClicked(MouseEvent event) {
		if (event.getClickCount() == 2) {
			selectButtonClicked();
		}
	}
}
