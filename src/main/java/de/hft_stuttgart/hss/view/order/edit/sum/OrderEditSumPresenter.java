package de.hft_stuttgart.hss.view.order.edit.sum;

import static de.hft_stuttgart.hss.view.order.create.sum.OrderNewSumFields.DISCOUNT;
import static de.hft_stuttgart.hss.view.order.create.sum.OrderNewSumFields.PAYMENT;
import static de.hft_stuttgart.hss.view.order.create.sum.OrderNewSumFields.SHIPPING;
import static de.hft_stuttgart.hss.view.order.create.sum.OrderNewSumFields.VAT;

import java.util.HashMap;
import java.util.Locale;
import java.util.Optional;

import javax.inject.Inject;

import de.hft_stuttgart.hss.entities.PaymentType;
import de.hft_stuttgart.hss.entities.ShippingType;
import de.hft_stuttgart.hss.services.ExportService;
import de.hft_stuttgart.hss.services.OrderService;
import de.hft_stuttgart.hss.services.misc.ExportType;
import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.domain.context.OrderDataContext;
import de.hft_stuttgart.hss.view.common.domain.data.OrderDetails;
import de.hft_stuttgart.hss.view.common.domain.data.OrderPosition;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.common.misc.EnumStringConverter;
import de.hft_stuttgart.hss.view.common.misc.PercentageTextFormtter;
import de.hft_stuttgart.hss.view.order.edit.position.OrderEditPositionView;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.NumberBinding;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.util.converter.CurrencyStringConverter;
import javafx.util.converter.PercentageStringConverter;

public class OrderEditSumPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	TextField sum;
	@FXML
	TextField discount;
	@FXML
	TextField preTax;
	@FXML
	TextField vat;
	@FXML
	TextField afterTax;
	@FXML
	ComboBox<ShippingType> shipping;
	@FXML
	ComboBox<PaymentType> payment;
	@FXML
	ToggleGroup installation;
	@FXML
	RadioButton installationYes;
	@FXML
	RadioButton installationNo;
	@FXML
	Button enableVatButton;
	@FXML
	Button enableDiscountButton;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	OrderDataContext dataContext;
	@Inject
	OrderService orderService;
	@Inject
	ExportService exportService;

	// Deklaration lokale Variable
	private OrderContext context;

	@Override
	protected void init() {
		context = dataContext.getOrderContext();

		installationNo.setUserData(false);
		installationYes.setUserData(true);

		OrderDetails orderDetails = context.getOrder();

		orderDetails.positionTotalProperty().bind(buildSumBinding());
		orderDetails.discountTotalProperty().bind(buildDiscountBinding());
		orderDetails.orderSumProperty().bind(buildPreTaxBinding());
		orderDetails.vatTotalProperty().bind(buildVatBinding());
		orderDetails.orderTotalProperty().bind(buildAfterTaxBinding());

		context.getPositions().addListener((ListChangeListener<? super OrderPosition>) change -> {
			orderDetails.positionTotalProperty().bind(buildSumBinding());
			orderDetails.discountTotalProperty().bind(buildDiscountBinding());
			orderDetails.orderSumProperty().bind(buildPreTaxBinding());
			orderDetails.vatTotalProperty().bind(buildVatBinding());
			orderDetails.orderTotalProperty().bind(buildAfterTaxBinding());
		});

		if (orderDetails.getInstallation()) {
			installationYes.setSelected(true);
		} else {
			installationNo.setSelected(true);
		}

		sum.textProperty().bindBidirectional(orderDetails.positionTotalProperty(), new CurrencyStringConverter(Locale.GERMANY));
		discount.textProperty().bindBidirectional(orderDetails.discountProperty(), new PercentageStringConverter());
		preTax.textProperty().bindBidirectional(orderDetails.orderSumProperty(), new CurrencyStringConverter(Locale.GERMANY));
		vat.textProperty().bindBidirectional(orderDetails.vatProperty(), new PercentageStringConverter());
		afterTax.textProperty().bindBidirectional(orderDetails.orderTotalProperty(), new CurrencyStringConverter(Locale.GERMANY));
		shipping.valueProperty().bindBidirectional(orderDetails.shippingTypeProperty());
		payment.valueProperty().bindBidirectional(orderDetails.paymentTypeProperty());

		shipping.getItems().addAll(ShippingType.values());
		shipping.setConverter(new EnumStringConverter<>(resources, "order.shipping.type."));

		payment.getItems().addAll(PaymentType.values());
		payment.setConverter(new EnumStringConverter<>(resources, "order.payment.type."));

		installation.selectedToggleProperty().addListener((observalve, oldValue, newValue) -> {
			context.getOrder().setInstallation((Boolean) newValue.getUserData());
		});

		addValidatorField(SHIPPING, shipping);
		addValidatorField(PAYMENT, payment);
		addValidatorField(DISCOUNT, discount);
		addValidatorField(VAT, vat);
	}

	private ObservableValue<? extends Number> buildVatBinding() {
		OrderDetails orderDetails = context.getOrder();

		return Bindings.multiply(orderDetails.orderSumProperty(), orderDetails.vatProperty());
	}

	private NumberBinding buildDiscountBinding() {
		OrderDetails orderDetails = context.getOrder();

		return Bindings.multiply(orderDetails.positionTotalProperty(), orderDetails.discountProperty());
	}

	private NumberBinding buildAfterTaxBinding() {
		OrderDetails orderDetails = context.getOrder();
		NumberBinding vatMultiplier = Bindings.add(1.0, orderDetails.vatProperty());
		return Bindings.multiply(orderDetails.orderSumProperty(), vatMultiplier);
	}

	private NumberBinding buildPreTaxBinding() {
		OrderDetails orderDetails = context.getOrder();
		NumberBinding discountMultiplier = Bindings.subtract(1.0, orderDetails.discountProperty());
		return Bindings.multiply(orderDetails.positionTotalProperty(), discountMultiplier);
	}

	private NumberBinding buildSumBinding() {
		NumberBinding sum = Bindings.add(new SimpleIntegerProperty(0), 0);

		for (OrderPosition pos : context.getPositions()) {
			NumberBinding posPrice = Bindings.multiply(pos.amountProperty(), pos.singlePriceProperty());
			sum = Bindings.add(posPrice, sum);
		}
		return sum;
	}

	/**
	 * Ein neuer Akt (ÜbersichtsSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt Es wird noch
	 * Abgefragt - ob wirklich abgebrochen werden soll
	 */
	public void abortButtonClicked() {
		Optional<ButtonType> result = showConfirmationDialog("dialog.order.create.abort.title", "dialog.order.create.abort.text");
		if (result.get() == ButtonType.YES) {
			navigateToOverview();
		}
	}

	/**
	 * Ein neuer Akt (AuftragBearbeitungPositionSeite) wird erstellt -
	 * Bühnengerüst bleibt - Szene wurde auf dem BorderPane in den Center
	 * gesetzt Der Auftragswizard geht einen Punkt zurück --> von Summe zu
	 * Positionen
	 */
	public void backButtonClicked() {
		navigateTo(OrderEditPositionView.class);
	}

	/**
	 * Mehrwertsteuer Button aktiviert Textfeld, um Mwst ändern zu können
	 */
	public void enableVatButtonClicked() {
		vat.setDisable(false);
		vat.setTextFormatter(new PercentageTextFormtter());
	}

	/**
	 * Rabatt Button aktiviert Textfeld, um Rabattsatz ändern zu können.
	 */
	public void enableDiscountButtonClicked() {
		discount.setDisable(false);
		discount.setTextFormatter(new PercentageTextFormtter());
	}

	/**
	 * Auftrag wird in Datenbank geschrieben - Möglichkeit den Auftrag in ein
	 * PDF zu exportieren. Abschließend wird die Übersichtsseite aufgerufen
	 */
	public void finishButtonClicked() {
		validate(context.getOrder(), OrderEditSumFields.values(), t -> {
			orderService.updateOrder(context);

			Optional<ButtonType> result = showConfirmationDialog("print.dialog.title", "print.dialog.text");
			if (result.get() == ButtonType.YES) {
				exportService.export(ExportType.ORDER, context, resources::getString, this::showProgressDialog, e -> navigateToOverview(), viewManager::getPrimaryStage, new HashMap<>());
			} else {
				navigateToOverview();
			}
		});
	}

}

