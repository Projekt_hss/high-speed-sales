package de.hft_stuttgart.hss.view.order.create.shipping;

import static de.hft_stuttgart.hss.view.common.validator.HssValidatorType.STRING;

import de.hft_stuttgart.hss.view.common.domain.data.ShippingDetails;
import de.hft_stuttgart.hss.view.common.validator.HssValidatorField;
import de.hft_stuttgart.hss.view.common.validator.HssValidatorType;

public enum OrderNewShippingFields implements HssValidatorField {
	STREET(STRING) {

		@Override
		public Object getValue(Object obj) {
			return ((ShippingDetails) obj).getStreet();
		}

	},
	NUMBER(STRING) {

		@Override
		public Object getValue(Object obj) {
			return ((ShippingDetails) obj).getNumber();
		}

	},
	ZIP(STRING) {

		@Override
		public Object getValue(Object obj) {
			return ((ShippingDetails) obj).getZip();
		}

	},
	CITY(STRING) {

		@Override
		public Object getValue(Object obj) {
			return ((ShippingDetails) obj).getCity();
		}

	};
	
	private HssValidatorType type;

	private OrderNewShippingFields(HssValidatorType type) {
		this.type = type;
	}

	@Override
	public boolean isRequired(Object obj) {
		return ((ShippingDetails) obj).isShippingAddressAvailable();
	}

	@Override
	public HssValidatorType getType() {
		return type;
	}
}
