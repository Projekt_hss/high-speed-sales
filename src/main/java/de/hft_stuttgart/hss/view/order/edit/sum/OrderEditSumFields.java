package de.hft_stuttgart.hss.view.order.edit.sum;

import static de.hft_stuttgart.hss.view.common.validator.HssValidatorType.DOUBLE;
import static de.hft_stuttgart.hss.view.common.validator.HssValidatorType.OBJECT;

import de.hft_stuttgart.hss.view.common.domain.data.OrderDetails;
import de.hft_stuttgart.hss.view.common.validator.HssValidatorField;
import de.hft_stuttgart.hss.view.common.validator.HssValidatorType;

public enum OrderEditSumFields implements HssValidatorField {

	SHIPPING(true, OBJECT) {
		@Override
		public Object getValue(Object obj) {
			return ((OrderDetails) obj).shippingTypeProperty().get();
		}
	},
	PAYMENT(true, OBJECT) {
		@Override
		public Object getValue(Object obj) {
			return ((OrderDetails) obj).paymentTypeProperty().get();
		}
	},
	DISCOUNT(true, DOUBLE) {
		@Override
		public Object getValue(Object obj) {
			return ((OrderDetails) obj).getDiscount();
		}
	},
	VAT(true, DOUBLE) {
		@Override
		public Object getValue(Object obj) {
			return ((OrderDetails) obj).getVat();
		}
	};

	private boolean required;
	private HssValidatorType type;

	private OrderEditSumFields(boolean required, HssValidatorType type) {
		this.required = required;
		this.type = type;
	}

	@Override
	public boolean isRequired(Object obj) {
		return required;
	}

	@Override
	public HssValidatorType getType() {
		return type;
	}
}
