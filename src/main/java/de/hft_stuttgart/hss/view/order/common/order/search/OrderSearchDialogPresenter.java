package de.hft_stuttgart.hss.view.order.common.order.search;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.inject.Inject;

import de.hft_stuttgart.hss.entities.OrderState;
import de.hft_stuttgart.hss.services.OrderService;
import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.domain.context.OrderDataContext;
import de.hft_stuttgart.hss.view.common.domain.data.OrderSearch;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.common.misc.EnumStringConverter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.util.converter.LocalDateTimeStringConverter;

public class OrderSearchDialogPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	TextField search;
	@FXML
	TableView<OrderSearch> orderTable;
	@FXML
	Button selectButton;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	OrderService orderService;
	@Inject
	OrderDataContext context;

	@SuppressWarnings("unchecked")
	@Override
	protected void init() {
		orderService.setStates(context.getStates());
		List<OrderSearch> allOrders = orderService.getAll();
		orderTable.setItems(FXCollections.observableArrayList(allOrders));

		if (allOrders.isEmpty()) {
			search.setDisable(true);
		}

		ObservableList<TableColumn<OrderSearch, ?>> columns = orderTable.getColumns();

		((TableColumn<OrderSearch, Number>) columns.get(0)).setCellValueFactory((order) -> order.getValue().orderIdProperty());
		((TableColumn<OrderSearch, LocalDateTime>) columns.get(1)).setCellValueFactory((order) -> order.getValue().orderDateProperty());
		((TableColumn<OrderSearch, LocalDateTime>) columns.get(1)).setCellFactory(TextFieldTableCell.forTableColumn(new LocalDateTimeStringConverter(DateTimeFormatter.ISO_LOCAL_DATE_TIME, DateTimeFormatter.ISO_LOCAL_DATE_TIME)));
		((TableColumn<OrderSearch, Number>) columns.get(2)).setCellValueFactory((order) -> order.getValue().customerIdProperty());
		((TableColumn<OrderSearch, String>) columns.get(3)).setCellValueFactory((order) -> order.getValue().companyProperty());
		((TableColumn<OrderSearch, String>) columns.get(4)).setCellValueFactory((order) -> order.getValue().lastnameProperty());
		((TableColumn<OrderSearch, String>) columns.get(5)).setCellValueFactory((order) -> order.getValue().prenameProperty());
		((TableColumn<OrderSearch, OrderState>) columns.get(6)).setCellValueFactory((order) -> order.getValue().stateProperty());
		((TableColumn<OrderSearch, OrderState>) columns.get(6)).setCellFactory(TextFieldTableCell.forTableColumn(new EnumStringConverter<OrderState>(resources, "order.state.type.")));
		((TableColumn<OrderSearch, String>) columns.get(7)).setCellValueFactory((order) -> order.getValue().employeeProperty());

		orderTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			selectButton.setDisable(false);
		});
	}

	/**
	 * Suchen nach einem Auftrag
	 */
	public void search() {
		doSearch(search::getText, orderService::getAll, orderService::searchOrder, orderTable::setItems);
	}

	/**
	 * Popup-Fenster wird geschlossen
	 */
	public void abortButtonClicked() {
		viewManager.getDialogStage().close();
	}

	/**
	 * Der ausgewählte Datensatz wird übernommen und das Popup Fenster
	 * geschlossen
	 */
	public void selectButtonClicked() {
		OrderSearch orderSearch = orderTable.getSelectionModel().getSelectedItem();
		if (orderSearch != null) {
			OrderContext orderContext = orderService.getOrder(orderSearch.getOrderId());
			context.setOrderContext(orderContext);
			abortButtonClicked();
		}
	}

	/**
	 * Methode prüft auf Doppelklick und ruft danach eine weitere Methode auf
	 */
	public void mouseClicked(MouseEvent event) {
		if (event.getClickCount() == 2) {
			selectButtonClicked();
		}
	}
}
