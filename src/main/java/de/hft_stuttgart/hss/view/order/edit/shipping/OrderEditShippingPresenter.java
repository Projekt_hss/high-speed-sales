package de.hft_stuttgart.hss.view.order.edit.shipping;

import static de.hft_stuttgart.hss.view.order.create.shipping.OrderNewShippingFields.CITY;
import static de.hft_stuttgart.hss.view.order.create.shipping.OrderNewShippingFields.NUMBER;
import static de.hft_stuttgart.hss.view.order.create.shipping.OrderNewShippingFields.STREET;
import static de.hft_stuttgart.hss.view.order.create.shipping.OrderNewShippingFields.ZIP;

import java.util.Optional;

import javax.inject.Inject;

import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.domain.context.OrderDataContext;
import de.hft_stuttgart.hss.view.common.domain.data.ShippingDetails;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.common.misc.IntegerTextFormtter;
import de.hft_stuttgart.hss.view.order.edit.customer.OrderEditCustomerView;
import de.hft_stuttgart.hss.view.order.edit.position.OrderEditPositionView;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;

public class OrderEditShippingPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	GridPane shippingAddressWrapper;
	@FXML
	TextField street;
	@FXML
	TextField number;
	@FXML
	TextField zip;
	@FXML
	TextField city;
	@FXML
	ToggleGroup request;
	@FXML
	RadioButton radioYes;
	@FXML
	RadioButton radioNo;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	OrderDataContext dataContext;

	// Deklaration lokale Variable
	private OrderContext context;

	@Override
	protected void init() {
		context = dataContext.getOrderContext();

		ShippingDetails shippingDetails = context.getShipping();

		radioYes.selectedProperty().bindBidirectional(shippingAddressWrapper.visibleProperty());
		radioYes.selectedProperty().bindBidirectional(shippingDetails.shippingAddressAvailableProperty());

		if (shippingDetails.isShippingAddressAvailable()) {
			radioYes.setSelected(true);
		} else {
			radioNo.setSelected(true);
		}

		street.textProperty().bindBidirectional(shippingDetails.streetProperty());
		number.textProperty().bindBidirectional(shippingDetails.numberProperty());
		zip.textProperty().bindBidirectional(shippingDetails.zipProperty());
		zip.setTextFormatter(new IntegerTextFormtter());
		city.textProperty().bindBidirectional(shippingDetails.cityProperty());

		addValidatorField(STREET, street);
		addValidatorField(NUMBER, number);
		addValidatorField(ZIP, zip);
		addValidatorField(CITY, city);
	}

	/**
	 * Ein neuer Akt (ÜbersichtsSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt Es wird noch
	 * Abgefragt - ob wirklich abgebrochen werden soll
	 */
	public void abortButtonClicked() {
		Optional<ButtonType> result = showConfirmationDialog("dialog.order.create.abort.title", "dialog.order.create.abort.text");
		if (result.get() == ButtonType.YES) {
			navigateToOverview();
		}
	}

	/**
	 * Ein neuer Akt (AuftragBearbeitungKundeSeite) wird erstellt - Bühnengerüst
	 * bleibt - Szene wurde auf dem BorderPane in den Center gesetzt. Der
	 * Auftragswizard geht einen Punkt zurück --> von Lieferanschrift zu
	 * Kundendaten
	 */

	public void backButtonClicked() {
		navigateTo(OrderEditCustomerView.class);
	}

	/**
	 * Der nächste Punkt im Auftragswizard wird angezeigt (Positionen)
	 */
	public void forwardButtonClicked() {
		validate(context.getShipping(), OrderEditShippingFields.values(), t -> navigateTo(OrderEditPositionView.class));
	}
}
