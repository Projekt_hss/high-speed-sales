package de.hft_stuttgart.hss.view.order.overview;

import com.airhacks.afterburner.injection.Injector;

import de.hft_stuttgart.hss.view.common.domain.context.OrderDataContext;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.order.create.customer.OrderNewCustomerView;
import de.hft_stuttgart.hss.view.order.search.OrderSearchView;

public class OrderOverviewPresenter extends AbstractPresenter {

	/**
	 * Ein neuer Akt (ÜbersichtsSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void backButtonClicked() {
		navigateToOverview();
	}

	/**
	 * Ein neuer Akt (NeueAuftragsSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void orderNewButtonClicked() {
		Injector.setModelOrService(OrderDataContext.class, new OrderDataContext());
		navigateTo(OrderNewCustomerView.class);
	}

	/**
	 * Ein neuer Akt (AuftragSuchSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void orderSearchButtonClicked() {
		Injector.setModelOrService(OrderDataContext.class, new OrderDataContext());
		navigateTo(OrderSearchView.class);
	}
}
