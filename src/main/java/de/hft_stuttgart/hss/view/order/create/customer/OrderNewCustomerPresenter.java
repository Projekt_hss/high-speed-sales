package de.hft_stuttgart.hss.view.order.create.customer;

import java.util.Optional;
import java.util.function.Consumer;

import javax.inject.Inject;

import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.domain.context.OrderDataContext;
import de.hft_stuttgart.hss.view.common.domain.data.CustomerDetails;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.common.misc.EnumStringConverter;
import de.hft_stuttgart.hss.view.order.common.customer.search.OrderCustomerSearchView;
import de.hft_stuttgart.hss.view.order.create.shipping.OrderNewShippingView;
import de.hft_stuttgart.hss.view.order.overview.OrderOverviewView;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

public class OrderNewCustomerPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	TextField customerId;
	@FXML
	TextField company;
	@FXML
	TextField lastname;
	@FXML
	TextField prename;
	@FXML
	TextField street;
	@FXML
	TextField zip;
	@FXML
	TextField city;
	@FXML
	TextField phone;
	@FXML
	TextField fax;
	@FXML
	TextField number;
	@FXML
	TextField mobile;
	@FXML
	TextField mail;
	@FXML
	TextField title;
	@FXML
	Button forwardButton;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	OrderDataContext dataContext;

	// Deklaration lokale Variable
	private OrderContext context;

	@Override
	protected void init() {
		if (dataContext.getOrderContext() == null) {
			dataContext.setOrderContext(new OrderContext());
		}
		context = dataContext.getOrderContext();

		if (context.getCustomer() != null) {
			setCustomer(context.getCustomer());
			forwardButton.setDisable(false);
		}
	}

	/**
	 * Zuordnung der Variablen
	 */
	private void setCustomer(CustomerDetails customer) {
		customerId.textProperty().bindBidirectional(customer.customerIdProperty(), new NumberStringConverter());
		company.textProperty().bind(customer.companyProperty());
		lastname.textProperty().bind(customer.lastnameProperty());
		prename.textProperty().bind(customer.prenameProperty());
		street.textProperty().bind(customer.streetProperty());
		zip.textProperty().bind(customer.zipProperty());
		city.textProperty().bind(customer.cityProperty());
		phone.textProperty().bind(customer.phoneProperty());
		fax.textProperty().bind(customer.faxProperty());
		number.textProperty().bind(customer.numberProperty());
		mobile.textProperty().bind(customer.mobileProperty());
		mail.textProperty().bind(customer.mailProperty());
		title.textProperty().bindBidirectional(customer.titleProperty(), new EnumStringConverter<>(resources, "customer.title."));
	}

	/**
	 * Ein neues Popup-Fenster wird erzeugt, um einen Kunden auswählen zu können
	 */
	public void selectCustomerButtonClicked() {
		showCustomDialog("dialog.order.create.customer.search", OrderCustomerSearchView.class);

		if (context.getCustomer() != null) {
			setCustomer(context.getCustomer());
			forwardButton.setDisable(false);
		}
	}

	/**
	 * Auftragserstellung wird abgebrochen und die Übersichtsseite wird wieder
	 * angezeigt
	 */
	public void abortButtonClicked() {
		checkAbortProcess(t -> navigateToOverview());
	}

	/**
	 * Popup-Fenster über die Abfrage ob die Auftragserstellung wirklich
	 * abgebrochen werden soll
	 */
	private void checkAbortProcess(Consumer<?> f) {
		if (!context.isEmpty()) {
			Optional<ButtonType> result = showConfirmationDialog("dialog.order.create.abort.title", "dialog.order.create.abort.text");
			if (result.get() == ButtonType.YES) {
				f.accept(null);
			}
		} else {
			f.accept(null);
		}
	}

	/**
	 * Popup-Fenster über die Abfrage ob die Auftragserstellung wirklich
	 * abgebrochen werden soll
	 */
	public void backButtonClicked() {
		checkAbortProcess(t -> navigateTo(OrderOverviewView.class));
	}

	/**
	 * Der nächste Punkt im Auftragswizard wird angezeigt (Lieferanschrift)
	 */
	public void forwardButtonClicked() {
		navigateTo(OrderNewShippingView.class);
	}

}
