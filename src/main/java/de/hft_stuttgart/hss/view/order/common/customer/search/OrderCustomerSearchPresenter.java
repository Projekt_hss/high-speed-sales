package de.hft_stuttgart.hss.view.order.common.customer.search;

import java.util.List;

import javax.inject.Inject;

import de.hft_stuttgart.hss.services.CustomerService;
import de.hft_stuttgart.hss.view.common.domain.context.OrderDataContext;
import de.hft_stuttgart.hss.view.common.domain.data.CustomerDetails;
import de.hft_stuttgart.hss.view.common.domain.data.CustomerSearch;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class OrderCustomerSearchPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	TableView<CustomerSearch> customerTable;
	@FXML
	TextField search;
	@FXML
	Button selectButton;
	@Inject
	OrderDataContext context;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	CustomerService customerService;

	@SuppressWarnings("unchecked")
	@Override
	protected void init() {
		List<CustomerSearch> allCustomers = customerService.getAll();
		customerTable.setItems(FXCollections.observableArrayList(allCustomers));

		ObservableList<TableColumn<CustomerSearch, ?>> columns = customerTable.getColumns();
		((TableColumn<CustomerSearch, Number>) columns.get(0)).setCellValueFactory((customer) -> customer.getValue().customerIdProperty());
		((TableColumn<CustomerSearch, String>) columns.get(1)).setCellValueFactory((customer) -> customer.getValue().companyProperty());
		((TableColumn<CustomerSearch, String>) columns.get(2)).setCellValueFactory((customer) -> customer.getValue().lastnameProperty());
		((TableColumn<CustomerSearch, String>) columns.get(3)).setCellValueFactory((customer) -> customer.getValue().prenameProperty());
		((TableColumn<CustomerSearch, String>) columns.get(4)).setCellValueFactory((customer) -> customer.getValue().streetProperty());
		((TableColumn<CustomerSearch, String>) columns.get(5)).setCellValueFactory((customer) -> customer.getValue().numberProperty());
		((TableColumn<CustomerSearch, String>) columns.get(6)).setCellValueFactory((customer) -> customer.getValue().zipProperty());
		((TableColumn<CustomerSearch, String>) columns.get(7)).setCellValueFactory((customer) -> customer.getValue().cityProperty());

		customerTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			selectButton.setDisable(false);
		});
	}

	/**
	 * Popup-Fenster wird geschlossen
	 */
	public void abortButtonClicked() {
		viewManager.getDialogStage().close();
	}

	/**
	 * 
	 */
	public void searchButtonClicked() {
		doSearch(search::getText, customerService::getAll, customerService::searchCustomer, customerTable::setItems);

		selectButton.setDisable(true);
	}

	/**
	 * Der ausgewählte Kunde wird zum Auftrag hinzugefügt. Popup Fenster wird
	 * anschließende geschlossen
	 */
	public void selectButtonClicked() {
		CustomerSearch customerSearch = getSelectedItem(customerTable);
		if (customerSearch != null) {
			CustomerDetails customerDetails = customerService.getCustomer(customerSearch.getCustomerId());
			context.getOrderContext().setCustomer(customerDetails);
			abortButtonClicked();
		}
	}

	/**
	 * Methode prüft auf Doppelklick und ruft danach eine weitere Methode auf
	 */
	public void mouseClicked(MouseEvent event) {
		if (event.getClickCount() == 2) {
			selectButtonClicked();
		}
	}

}
