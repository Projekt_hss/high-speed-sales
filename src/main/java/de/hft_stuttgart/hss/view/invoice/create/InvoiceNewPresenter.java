package de.hft_stuttgart.hss.view.invoice.create;

import static de.hft_stuttgart.hss.view.invoice.create.InvoiceNewFields.INVOICE_DATE;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import de.hft_stuttgart.hss.entities.OrderState;
import de.hft_stuttgart.hss.entities.PaymentType;
import de.hft_stuttgart.hss.services.ExportService;
import de.hft_stuttgart.hss.services.InvoiceService;
import de.hft_stuttgart.hss.services.misc.ExportType;
import de.hft_stuttgart.hss.services.misc.HssUtils;
import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.domain.context.OrderDataContext;
import de.hft_stuttgart.hss.view.common.domain.data.InvoiceDetails;
import de.hft_stuttgart.hss.view.common.domain.data.OrderPosition;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.common.misc.DateBeforeCell;
import de.hft_stuttgart.hss.view.common.misc.EnumStringConverter;
import de.hft_stuttgart.hss.view.invoice.overview.InvoiceOverviewView;
import de.hft_stuttgart.hss.view.order.common.order.search.OrderSearchDialogView;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.CurrencyStringConverter;
import javafx.util.converter.NumberStringConverter;

public class InvoiceNewPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	TableView<OrderPosition> orderPositionTable;
	@FXML
	Button orderSelectButton;
	@FXML
	Button createButton;
	@FXML
	TextField customerId;
	@FXML
	TextField orderId;
	@FXML
	TextField company;
	@FXML
	TextField name;
	@FXML
	TextField street;
	@FXML
	TextField city;
	@FXML
	DatePicker billDate;
	@FXML
	TextField paymentType;
	@FXML
	TextField posSum;
	@FXML
	TextField totalSum;
	@FXML
	TextField vatSum;
	@FXML
	TextField discount;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	OrderDataContext context;
	@Inject
	ExportService exportService;
	@Inject
	InvoiceService invoiceService;

	@SuppressWarnings("unchecked")
	@Override
	protected void init() {
		context.setStates(new OrderState[] { OrderState.DISTRIBUTION });

		ObservableList<TableColumn<OrderPosition, ?>> columns = orderPositionTable.getColumns();

		((TableColumn<OrderPosition, Number>) columns.get(0)).setCellValueFactory((position) -> position.getValue().positionProperty());
		((TableColumn<OrderPosition, Number>) columns.get(1)).setCellValueFactory((position) -> position.getValue().articleIdProperty());
		((TableColumn<OrderPosition, String>) columns.get(2)).setCellValueFactory((position) -> position.getValue().articleNameProperty());
		((TableColumn<OrderPosition, Number>) columns.get(3)).setCellValueFactory((position) -> position.getValue().amountProperty());
		((TableColumn<OrderPosition, Number>) columns.get(3)).setCellFactory(TextFieldTableCell.forTableColumn(new NumberStringConverter()));
		((TableColumn<OrderPosition, Number>) columns.get(4)).setCellValueFactory((position) -> position.getValue().singlePriceProperty());
		((TableColumn<OrderPosition, Number>) columns.get(4)).setCellFactory(TextFieldTableCell.forTableColumn(new CurrencyStringConverter(Locale.GERMANY)));
		((TableColumn<OrderPosition, Number>) columns.get(5)).setCellValueFactory((position) -> Bindings.multiply(position.getValue().singlePriceProperty(), position.getValue().amountProperty()));
		((TableColumn<OrderPosition, Number>) columns.get(5)).setCellFactory(TextFieldTableCell.forTableColumn(new CurrencyStringConverter(Locale.GERMANY)));

		billDate.setDayCellFactory(cell -> new DateBeforeCell((long) INVOICE_DATE.getSecondaryInfo()));

		addValidatorField(INVOICE_DATE, billDate);
	}

	/**
	 * Ein neuer Akt (ÜbersichtsSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void abortButtonClicked() {
		navigateToOverview();
	}

	/**
	 * Ein neuer Akt (RechnungsÜbersichtsSeite) wird erstellt - Bühnengerüst
	 * bleibt - Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void backButtonClicked() {
		navigateTo(InvoiceOverviewView.class);
	}

	/**
	 * Auftrag wird in die Datenbank geschrieben und es kann eine Rechnung in
	 * PDF Format abgespeichert werden
	 */
	public void createButtonClicked() {
		OrderContext order = context.getOrderContext();
		validate(order, InvoiceNewFields.values(), t -> {
			invoiceService.save(order);

			Optional<ButtonType> result = showConfirmationDialog("print.dialog.title", "print.dialog.text");
			if (result.get() == ButtonType.YES) {
				Map<Object, String> translations = new HashMap<>();
				PaymentType orderPaymentType = order.getOrder().getPaymentType();
				translations.put(orderPaymentType, resources.getString("order.payment.type." + orderPaymentType.toString()));
				exportService.export(ExportType.INVOICE, order, resources::getString, this::showProgressDialog, e -> navigateToOverview(), viewManager::getPrimaryStage, translations);
			} else {
				navigateToOverview();
			}
		});
	}

	/**
	 * Ein Popup-Fenster wird erzeugt, dass alle Aufträge mit dem Status "offen"
	 * anzeigt.
	 */
	public void selectOrderButtonClicked() {
		showCustomDialog("view.invoice.create.search", OrderSearchDialogView.class);

		if (context.getOrderContext() != null) {
			context.getOrderContext().setInvoice(new InvoiceDetails());
			setOrder(context.getOrderContext());
			createButton.setDisable(false);
		}
	}

	/**
	 * Zuordnung der Variablen
	 */
	private void setOrder(OrderContext context) {
		orderId.textProperty().bindBidirectional(context.getOrder().orderIdProperty(), new NumberStringConverter());
		customerId.textProperty().bindBidirectional(context.getCustomer().customerIdProperty(), new NumberStringConverter());
		company.textProperty().bind(context.getCustomer().companyProperty());
		name.textProperty().bind(HssUtils.concat(context.getCustomer().prenameProperty(), context.getCustomer().lastnameProperty()));
		street.textProperty().bind(HssUtils.concat(context.getCustomer().streetProperty(), context.getCustomer().numberProperty()));
		city.textProperty().bind(HssUtils.concat(context.getCustomer().zipProperty(), context.getCustomer().cityProperty()));
		// Currency String Converter konvertiert den Double Wert in Euro
		posSum.textProperty().bindBidirectional(context.getOrder().positionTotalProperty(), new CurrencyStringConverter(Locale.GERMANY));
		discount.textProperty().bindBidirectional(context.getOrder().discountTotalProperty(), new CurrencyStringConverter(Locale.GERMANY));
		vatSum.textProperty().bindBidirectional(context.getOrder().vatTotalProperty(), new CurrencyStringConverter(Locale.GERMANY));
		totalSum.textProperty().bindBidirectional(context.getOrder().orderTotalProperty(), new CurrencyStringConverter(Locale.GERMANY));
		billDate.valueProperty().bindBidirectional(context.getInvoice().invoiceDateProperty());
		paymentType.textProperty().bindBidirectional(context.getOrder().paymentTypeProperty(), new EnumStringConverter<>(resources, "order.payment.type."));

		orderPositionTable.setItems(context.getPositions());
	}

}
