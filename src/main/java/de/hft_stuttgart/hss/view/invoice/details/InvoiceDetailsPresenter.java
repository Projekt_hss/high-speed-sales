package de.hft_stuttgart.hss.view.invoice.details;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import de.hft_stuttgart.hss.entities.PaymentType;
import de.hft_stuttgart.hss.services.ExportService;
import de.hft_stuttgart.hss.services.misc.ExportType;
import de.hft_stuttgart.hss.services.misc.HssUtils;
import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.domain.context.OrderDataContext;
import de.hft_stuttgart.hss.view.common.domain.data.OrderPosition;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.common.misc.EnumStringConverter;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.CurrencyStringConverter;
import javafx.util.converter.LocalDateStringConverter;
import javafx.util.converter.NumberStringConverter;

public class InvoiceDetailsPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	TableView<OrderPosition> orderPositionTable;
	@FXML
	TextField customerId;
	@FXML
	TextField orderId;
	@FXML
	TextField invoiceId;
	@FXML
	TextField company;
	@FXML
	TextField name;
	@FXML
	TextField street;
	@FXML
	TextField city;
	@FXML
	TextField billDate;
	@FXML
	TextField paymentType;
	@FXML
	TextField posSum;
	@FXML
	TextField totalSum;
	@FXML
	TextField vatSum;
	@FXML
	TextField discount;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	OrderDataContext context;
	@Inject
	ExportService exportService;

	@SuppressWarnings("unchecked")
	@Override
	protected void init() {
		ObservableList<TableColumn<OrderPosition, ?>> columns = orderPositionTable.getColumns();

		OrderContext orderContext = context.getOrderContext();
		orderPositionTable.setItems(orderContext.getPositions());

		((TableColumn<OrderPosition, Number>) columns.get(0)).setCellValueFactory((position) -> position.getValue().positionProperty());
		((TableColumn<OrderPosition, Number>) columns.get(1)).setCellValueFactory((position) -> position.getValue().articleIdProperty());
		((TableColumn<OrderPosition, String>) columns.get(2)).setCellValueFactory((position) -> position.getValue().articleNameProperty());
		((TableColumn<OrderPosition, Number>) columns.get(3)).setCellValueFactory((position) -> position.getValue().amountProperty());
		((TableColumn<OrderPosition, Number>) columns.get(3)).setCellFactory(TextFieldTableCell.forTableColumn(new NumberStringConverter()));
		((TableColumn<OrderPosition, String>) columns.get(4)).setCellValueFactory((position) -> position.getValue().unityProperty());
		((TableColumn<OrderPosition, Number>) columns.get(5)).setCellValueFactory((position) -> position.getValue().singlePriceProperty());
		((TableColumn<OrderPosition, Number>) columns.get(5)).setCellFactory(TextFieldTableCell.forTableColumn(new CurrencyStringConverter(Locale.GERMANY)));
		((TableColumn<OrderPosition, Number>) columns.get(6)).setCellValueFactory((position) -> Bindings.multiply(position.getValue().singlePriceProperty(), position.getValue().amountProperty()));
		((TableColumn<OrderPosition, Number>) columns.get(6)).setCellFactory(TextFieldTableCell.forTableColumn(new CurrencyStringConverter(Locale.GERMANY)));

		invoiceId.textProperty().bindBidirectional(orderContext.getInvoice().invoiceIdProperty(), new NumberStringConverter());
		orderId.textProperty().bindBidirectional(orderContext.getOrder().orderIdProperty(), new NumberStringConverter());
		customerId.textProperty().bindBidirectional(orderContext.getCustomer().customerIdProperty(), new NumberStringConverter());
		company.textProperty().bind(orderContext.getCustomer().companyProperty());
		name.textProperty().bind(HssUtils.concat(orderContext.getCustomer().prenameProperty(), orderContext.getCustomer().lastnameProperty()));
		street.textProperty().bind(HssUtils.concat(orderContext.getCustomer().streetProperty(), orderContext.getCustomer().numberProperty()));
		city.textProperty().bind(HssUtils.concat(orderContext.getCustomer().zipProperty(), orderContext.getCustomer().cityProperty()));
		posSum.textProperty().bindBidirectional(orderContext.getOrder().positionTotalProperty(), new CurrencyStringConverter(Locale.GERMANY));
		discount.textProperty().bindBidirectional(orderContext.getOrder().discountTotalProperty(), new CurrencyStringConverter(Locale.GERMANY));
		vatSum.textProperty().bindBidirectional(orderContext.getOrder().vatTotalProperty(), new CurrencyStringConverter(Locale.GERMANY));
		totalSum.textProperty().bindBidirectional(orderContext.getOrder().orderTotalProperty(), new CurrencyStringConverter(Locale.GERMANY));
		paymentType.textProperty().bindBidirectional(orderContext.getOrder().paymentTypeProperty(), new EnumStringConverter<PaymentType>(resources, "order.payment.type."));
		billDate.textProperty().bindBidirectional(orderContext.getInvoice().invoiceDateProperty(), new LocalDateStringConverter(DateTimeFormatter.ISO_LOCAL_DATE, DateTimeFormatter.ISO_LOCAL_DATE));
	}

	/**
	 * Bei Aufruf der Methode wird das Popup-Fenster geschlossen
	 */
	public void exitButtonClicked() {
		viewManager.getDialogStage().close();
	}

	/**
	 * Die Rechnung wird in ein PDF exportiert
	 */
	public void printInvoice() {
		Map<Object, String> translations = new HashMap<>();
		PaymentType orderPaymentType = context.getOrderContext().getOrder().getPaymentType();
		translations.put(orderPaymentType, resources.getString("order.payment.type." + orderPaymentType.toString()));
		exportService.export(ExportType.INVOICE, context.getOrderContext(), resources::getString, this::showProgressDialog, null, viewManager::getDialogStage, translations);
	}

}
