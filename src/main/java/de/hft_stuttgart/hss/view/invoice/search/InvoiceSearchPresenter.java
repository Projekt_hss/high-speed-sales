package de.hft_stuttgart.hss.view.invoice.search;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import de.hft_stuttgart.hss.entities.PaymentType;
import de.hft_stuttgart.hss.services.InvoiceService;
import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.domain.context.OrderDataContext;
import de.hft_stuttgart.hss.view.common.domain.data.InvoiceSearch;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.common.misc.EnumStringConverter;
import de.hft_stuttgart.hss.view.invoice.details.InvoiceDetailsView;
import de.hft_stuttgart.hss.view.invoice.overview.InvoiceOverviewView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.util.converter.CurrencyStringConverter;
import javafx.util.converter.LocalDateStringConverter;

public class InvoiceSearchPresenter extends AbstractPresenter {

	/*
	 * Verknüpfung der Variablen im FXML (SceneBuilder)
	 */
	@FXML
	TableView<InvoiceSearch> billTable;
	@FXML
	TextField search;
	@FXML
	Button billSelectButton;

	/*
	 * Variabeln für Services und Seitenübergreifende Datenhaltung
	 * Framework,analyisiert das komplette Projekt beim Starten der Anwendung
	 * und speichert die Typen (Klassen). Wenn Objekt vom Container angefordert
	 * wird, arbeitet dieser die Felder der Klassen rekursiv ab.
	 */
	@Inject
	InvoiceService invoiceService;
	@Inject
	OrderDataContext context;

	@SuppressWarnings("unchecked")
	@Override
	protected void init() {
		List<InvoiceSearch> allInvoices = invoiceService.getAll();
		billTable.setItems(FXCollections.observableArrayList(allInvoices));
		
		if (allInvoices.isEmpty()) {
			search.setDisable(true);
		}

		ObservableList<TableColumn<InvoiceSearch, ?>> columns = billTable.getColumns();
		((TableColumn<InvoiceSearch, Number>) columns.get(0)).setCellValueFactory((bill) -> bill.getValue().invoiceIdProperty());
		((TableColumn<InvoiceSearch, LocalDate>) columns.get(1)).setCellValueFactory((bill) -> bill.getValue().invoiceDateProperty());
		((TableColumn<InvoiceSearch, LocalDate>) columns.get(1)).setCellFactory(TextFieldTableCell.forTableColumn(new LocalDateStringConverter(DateTimeFormatter.ISO_LOCAL_DATE, DateTimeFormatter.ISO_LOCAL_DATE)));
		((TableColumn<InvoiceSearch, Number>) columns.get(2)).setCellValueFactory((bill) -> bill.getValue().orderTotalProperty());
		((TableColumn<InvoiceSearch, Number>) columns.get(2)).setCellFactory(TextFieldTableCell.forTableColumn(new CurrencyStringConverter(Locale.GERMANY)));
		((TableColumn<InvoiceSearch, PaymentType>) columns.get(3)).setCellValueFactory((bill) -> bill.getValue().paymentTypeProperty());
		((TableColumn<InvoiceSearch, PaymentType>) columns.get(3)).setCellFactory(TextFieldTableCell.forTableColumn(new EnumStringConverter<PaymentType>(resources, "order.payment.type.")));
		((TableColumn<InvoiceSearch, Number>) columns.get(4)).setCellValueFactory((bill) -> bill.getValue().orderIdProperty());
		((TableColumn<InvoiceSearch, Number>) columns.get(5)).setCellValueFactory((bill) -> bill.getValue().customerIdProperty());
		((TableColumn<InvoiceSearch, String>) columns.get(6)).setCellValueFactory((bill) -> bill.getValue().companyProperty());
		((TableColumn<InvoiceSearch, String>) columns.get(7)).setCellValueFactory((bill) -> bill.getValue().lastnameProperty());
		((TableColumn<InvoiceSearch, String>) columns.get(8)).setCellValueFactory((bill) -> bill.getValue().prenameProperty());
		
		onItemSelected(billTable, t -> billSelectButton.setDisable(false));

	}

	/**
	 * Ein neuer Akt (ÜbersichtsSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void abortButtonClicked() {
		navigateToOverview();
	}

	/**
	 * Ein neuer Akt (RechnungsÜbersichtsSeite) wird erstellt - Bühnengerüst
	 * bleibt - Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void backButtonClicked() {
		navigateTo(InvoiceOverviewView.class);
	}

	/**
	 * Die Details des ausgewählte Datensatzes wird in einem neuen Popup Fenster
	 * angezeigt
	 */
	public void detailButtonClicked() {
		InvoiceSearch invoiceSearch = getSelectedItem(billTable);
		if (invoiceSearch != null) {
			OrderContext order = invoiceService.getInvoice(invoiceSearch.getInvoiceId());
			context.setOrderContext(order);
			showCustomDialog("dialog.invoice.details", InvoiceDetailsView.class);
		}
	}

	/**
	 * Suche nach angelegten Rechnungen
	 */
	public void search() {
		doSearch(search::getText, invoiceService::getAll, invoiceService::searchInvoices, billTable::setItems);
	}

	/**
	 * Methode prüft auf Doppelklick und ruft danach eine weitere Methode auf
	 */
	public void mouseClicked(MouseEvent event) {
		if (event.getClickCount() == 2) {
			detailButtonClicked();
		}
	}
}
