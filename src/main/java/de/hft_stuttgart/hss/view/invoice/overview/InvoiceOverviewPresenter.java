package de.hft_stuttgart.hss.view.invoice.overview;

import com.airhacks.afterburner.injection.Injector;

import de.hft_stuttgart.hss.view.common.domain.context.OrderDataContext;
import de.hft_stuttgart.hss.view.common.main.AbstractPresenter;
import de.hft_stuttgart.hss.view.invoice.create.InvoiceNewView;
import de.hft_stuttgart.hss.view.invoice.search.InvoiceSearchView;

public class InvoiceOverviewPresenter extends AbstractPresenter {

	/**
	 * Ein neuer Akt (ÜbersichtsSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void backButtonClicked() {
		navigateToOverview();
	}

	/**
	 * Ein neuer Akt (NeueRechnungsSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void billNewButtonClicked() {
		Injector.setModelOrService(OrderDataContext.class, new OrderDataContext());
		navigateTo(InvoiceNewView.class);
	}

	/**
	 * Ein neuer Akt (RechnungSuchSeite) wird erstellt - Bühnengerüst bleibt -
	 * Szene wurde auf dem BorderPane in den Center gesetzt
	 */
	public void billSearchButtonClicked() {
		Injector.setModelOrService(OrderDataContext.class, new OrderDataContext());
		navigateTo(InvoiceSearchView.class);
	}

}
