package de.hft_stuttgart.hss.view.invoice.create;

import static de.hft_stuttgart.hss.view.common.validator.HssValidatorType.DATE;

import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.validator.HssValidatorField;
import de.hft_stuttgart.hss.view.common.validator.HssValidatorType;

public enum InvoiceNewFields implements HssValidatorField {
	INVOICE_DATE(true, DATE) {
		@Override
		public Object getValue(Object object) {
			return ((OrderContext) object).getInvoice().getInvoiceDate();
		}

		@Override
		public Object getSecondaryInfo() {
			return 365L;
		}
	};

	private boolean required;
	private HssValidatorType type;

	private InvoiceNewFields(boolean required, HssValidatorType type) {
		this.required = required;
		this.type = type;
	}

	@Override
	public boolean isRequired(Object obj) {
		return required;
	}

	@Override
	public HssValidatorType getType() {
		return type;
	}
}
