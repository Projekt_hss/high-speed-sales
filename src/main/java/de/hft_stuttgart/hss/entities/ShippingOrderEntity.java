package de.hft_stuttgart.hss.entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the shippingOrder database table.
 * 
 */
@Entity(name = "ShippingOrder")
@Table(name = "shippingorder")
@NamedQueries({ 
		@NamedQuery(name = ShippingOrderEntity.QUERY_FIND_BY_ID, query = "SELECT NEW de.hft_stuttgart.hss.view.common.domain.context.OrderContext(s) FROM ShippingOrder s WHERE s.shippingOrderId = :shippingOrderId"),
		@NamedQuery(name = ShippingOrderEntity.QUERY_FIND_ALL, query = "SELECT NEW de.hft_stuttgart.hss.view.common.domain.data.ShippingSearch(s) FROM ShippingOrder s"),
		@NamedQuery(name = ShippingOrderEntity.QUERY_SEARCH, query = "SELECT NEW de.hft_stuttgart.hss.view.common.domain.data.ShippingSearch(s) FROM ShippingOrder s join s.order o join o.customer c WHERE c.company like :value or s.shippingOrderDate like :value or s.shippingOrderId like :value or o.orderId like :value or c.customerId like :value or c.lastname like :value or c.prename like :value")
})
public class ShippingOrderEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String QUERY_FIND_BY_ID = "ShippingOrder.findById";
	public static final String QUERY_FIND_ALL = "ShippingOrder.findAll";
	public static final String QUERY_SEARCH = "ShippingOrder.search";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer shippingOrderId;

	@Column(nullable = false)
	private LocalDate shippingOrderDate;

	@ManyToOne
	@JoinColumn(name = "Order_orderId", nullable = false)
	private OrderEntity order;

	@ManyToOne
	@JoinColumn(name = "Creator_employeeId", nullable = false)
	private EmployeeEntity creator;

	public ShippingOrderEntity() {
	}

	public Integer getShippingOrderId() {
		return shippingOrderId;
	}

	public void setShippingOrderId(Integer shippingOrderId) {
		this.shippingOrderId = shippingOrderId;
	}

	public LocalDate getShippingOrderDate() {
		return shippingOrderDate;
	}

	public void setShippingOrderDate(LocalDate shippingOrderDate) {
		this.shippingOrderDate = shippingOrderDate;
	}

	public OrderEntity getOrder() {
		return order;
	}

	public void setOrder(OrderEntity order) {
		this.order = order;
	}

	public EmployeeEntity getCreator() {
		return creator;
	}

	public void setCreator(EmployeeEntity creator) {
		this.creator = creator;
	}

}

