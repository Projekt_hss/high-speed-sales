package de.hft_stuttgart.hss.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the customer database table.
 * 
 */
@Entity(name = "Customer")
@Table(name = "customer")
@NamedQueries({
		@NamedQuery(name = CustomerEntity.QUERY_FIND_BY_ID, query = "SELECT NEW de.hft_stuttgart.hss.view.common.domain.data.CustomerDetails(c) FROM Customer c left outer join c.address a left outer join a.location l WHERE c.customerId = :customerId"),
		@NamedQuery(name = CustomerEntity.QUERY_FIND_ALL, query = "SELECT NEW de.hft_stuttgart.hss.view.common.domain.data.CustomerSearch(c) FROM Customer c"),
		@NamedQuery(name = CustomerEntity.QUERY_SEARCH, query = "SELECT new de.hft_stuttgart.hss.view.common.domain.data.CustomerSearch(c) FROM Customer c join c.address a join a.location l WHERE c.company like :value or c.customerId like :value or c.lastname like :value or c.prename like :value or a.street like :value or a.number like :value or l.zip like :value or l.city like :value")
})
public class CustomerEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String QUERY_FIND_BY_ID = "Customer.findById";
	public static final String QUERY_FIND_ALL = "Customer.findAll";
	public static final String QUERY_SEARCH = "Customer.search";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer customerId;

	@Column(length = 25)
	private String company;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 15)
	private TitleType title;

	@Column(nullable = false, length = 15)
	private String prename;

	@Column(nullable = false, length = 25)
	private String lastname;

	@Column(length = 15)
	private String fax;

	@Column(length = 55)
	private String mail;

	@Column(length = 15)
	private String phone;

	@Column(length = 15)
	private String mobile;

	@Column(nullable = false)
	private Double discount;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "Address_addressId", nullable = false)
	private AddressEntity address;

	// bi-directional many-to-one association to Order
	@OneToMany(mappedBy = "customer")
	private List<OrderEntity> orders;

	public CustomerEntity() {
	}

	public CustomerEntity(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public TitleType getTitle() {
		return title;
	}

	public void setTitle(TitleType title) {
		this.title = title;
	}

	public String getPrename() {
		return prename;
	}

	public void setPrename(String prename) {
		this.prename = prename;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public AddressEntity getAddress() {
		return address;
	}

	public void setAddress(AddressEntity address) {
		this.address = address;
	}

	public List<OrderEntity> getOrders() {
		return this.orders;
	}

	public void setOrders(List<OrderEntity> orders) {
		this.orders = orders;
	}

}