package de.hft_stuttgart.hss.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the responsible database table.
 * 
 */
@Entity(name = "Employee")
@Table(name = "employee")
@NamedQueries({ 
		@NamedQuery(name = EmployeeEntity.IS_AUTHENTICATED, query = "select r.employeeId from Employee r where r.username = :username and r.password = :password") 
})
public class EmployeeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String IS_AUTHENTICATED = "Employee.IsAuthenticated";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer employeeId;

	@Column(nullable = false, length = 25)
	private String lastname;

	@Column(nullable = false, length = 15)
	private String password;

	@Column(nullable = false, length = 15)
	private String prename;

	@Column(nullable = false, length = 45)
	private String username;

	public EmployeeEntity() {
	}

	public EmployeeEntity(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPrename() {
		return this.prename;
	}

	public void setPrename(String prename) {
		this.prename = prename;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}