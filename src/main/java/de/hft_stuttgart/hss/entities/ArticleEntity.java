package de.hft_stuttgart.hss.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
/**
 * The persistent class for the article database table.
 * 
 */
@Entity(name = "Article")
@Table(name = "article")
@NamedQueries({ 
		@NamedQuery(name = ArticleEntity.QUERY_FIND_ALL, query = "SELECT NEW de.hft_stuttgart.hss.view.common.domain.data.ArticleSearch(a) FROM Article a"),
		@NamedQuery(name = ArticleEntity.QUERY_FIND_ALL_NOT_CONTAINED, query = "SELECT NEW de.hft_stuttgart.hss.view.common.domain.data.ArticleSearch(a) FROM Article a WHERE a.articleId not in :articleIds"),
		@NamedQuery(name = ArticleEntity.QUERY_SEARCH, query = "SELECT NEW de.hft_stuttgart.hss.view.common.domain.data.ArticleSearch(a) FROM Article a WHERE a.articleName like :value or a.articleId like :value or a.eanNummer like :value or a.producer like :value or a.productGroup like :value or a.salesPrice like :value"), 
})
public class ArticleEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String QUERY_FIND_ALL = "Article.findAll";
	public static final String QUERY_FIND_ALL_NOT_CONTAINED = "Article.findAllNotContained";
	public static final String QUERY_SEARCH = "Article.search";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer articleId;

	@Column(nullable = false, length = 55)
	private String articleName;

	@Lob
	@Column(nullable = false)
	private String description;

	@Column(nullable = false, length = 25)
	private String eanNummer;

	@Column(nullable = false, length = 25)
	private String producer;

	@Column(nullable = false, length = 45)
	private String productGroup;

	@Column(nullable = false)
	private Double recorderPoint;

	@Column(nullable = false)
	private Double stock;

	@Column(nullable = false)
	private Double purchasePrice;

	@Column(nullable = false)
	private Double salesPrice;

	@Column(nullable = false, length = 25)
	private String unity;

	public ArticleEntity() {
	}

	public ArticleEntity(Integer articleId) {
		this.articleId = articleId;
	}

	public Integer getArticleId() {
		return this.articleId;
	}

	public void setArticleId(Integer articleId) {
		this.articleId = articleId;
	}

	public String getArticleName() {
		return this.articleName;
	}

	public void setArticleName(String articleName) {
		this.articleName = articleName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEanNummer() {
		return this.eanNummer;
	}

	public void setEanNummer(String eanNummer) {
		this.eanNummer = eanNummer;
	}

	public String getProducer() {
		return this.producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public String getProductGroup() {
		return this.productGroup;
	}

	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}

	public Double getPurchasePrice() {
		return this.purchasePrice;
	}

	public void setPurchasePrice(Double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public Double getRecorderPoint() {
		return this.recorderPoint;
	}

	public void setRecorderPoint(Double recorderPoint) {
		this.recorderPoint = recorderPoint;
	}

	public Double getSalesPrice() {
		return this.salesPrice;
	}

	public void setSalesPrice(Double salesPrice) {
		this.salesPrice = salesPrice;
	}

	public Double getStock() {
		return this.stock;
	}

	public void setStock(Double stock) {
		this.stock = stock;
	}

	public String getUnity() {
		return this.unity;
	}

	public void setUnity(String unity) {
		this.unity = unity;
	}

}