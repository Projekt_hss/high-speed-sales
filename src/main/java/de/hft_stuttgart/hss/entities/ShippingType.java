package de.hft_stuttgart.hss.entities;

public enum ShippingType {
	TRANSPORT, PICKUP;
}
