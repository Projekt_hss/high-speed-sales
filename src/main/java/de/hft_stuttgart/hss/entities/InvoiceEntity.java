package de.hft_stuttgart.hss.entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the bill database table.
 * 
 */
@Entity(name = "Invoice")
@Table(name = "invoice")
@NamedQueries({
		@NamedQuery(name = InvoiceEntity.QUERY_FIND_BY_ID, query = "SELECT NEW de.hft_stuttgart.hss.view.common.domain.context.OrderContext(b) FROM Invoice b WHERE b.invoiceId = :invoiceId"),
		@NamedQuery(name = InvoiceEntity.QUERY_FIND_ALL, query = "SELECT NEW de.hft_stuttgart.hss.view.common.domain.data.InvoiceSearch(b) FROM Invoice b"),
		@NamedQuery(name = InvoiceEntity.QUERY_SEARCH, query = "SELECT NEW de.hft_stuttgart.hss.view.common.domain.data.InvoiceSearch(b) FROM Invoice b join b.order o join o.customer c WHERE c.lastname like :value or b.invoiceId like :value or b.invoiceDate like :value or o.orderId like :value or c.customerId like :value or c.prename like :value"),
})
public class InvoiceEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String QUERY_SEARCH = "Invoice.search";
	public static final String QUERY_FIND_ALL = "Invoice.findAll";
	public static final String QUERY_FIND_BY_ID = "Invoice.findById";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer invoiceId;

	@Column(nullable = false)
	private LocalDate invoiceDate;

	@OneToOne
	@JoinColumn(name = "Order_orderId", nullable = false)
	private OrderEntity order;

	@ManyToOne
	@JoinColumn(name = "Creator_employeeId", nullable = false)
	private EmployeeEntity creator;

	public InvoiceEntity() {
	}

	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public LocalDate getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(LocalDate invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public OrderEntity getOrder() {
		return order;
	}

	public void setOrder(OrderEntity order) {
		this.order = order;
	}

	public EmployeeEntity getCreator() {
		return creator;
	}

	public void setCreator(EmployeeEntity creator) {
		this.creator = creator;
	}

}

