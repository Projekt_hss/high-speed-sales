package de.hft_stuttgart.hss.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the location database table.
 * 
 */
@Entity(name = "Location")
@Table(name = "location")
@NamedQueries({
		@NamedQuery(name = LocationEntity.QUERY_BY_FULL_LOCATION, query = "SELECT l FROM Location l WHERE l.zip = :zip AND l.city = :city")
})
public class LocationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String QUERY_BY_FULL_LOCATION = "LocationEntity.ByFullLocation";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer locationId;

	@Column(nullable = false, length = 45)
	private String city;

	@Column(nullable = false, length = 15)
	private String zip;

	// bi-directional many-to-one association to Address
	@OneToMany(mappedBy = "location")
	private List<AddressEntity> addresses;

	public LocationEntity() {
	}

	public Integer getLocationId() {
		return this.locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public List<AddressEntity> getAddresses() {
		return this.addresses;
	}

	public void setAddresses(List<AddressEntity> addresses) {
		this.addresses = addresses;
	}

	public AddressEntity addAddress(AddressEntity address) {
		getAddresses().add(address);
		address.setLocation(this);

		return address;
	}

	public AddressEntity removeAddress(AddressEntity address) {
		getAddresses().remove(address);
		address.setLocation(null);

		return address;
	}

}