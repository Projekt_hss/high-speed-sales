package de.hft_stuttgart.hss.entities;

public enum OrderState {
	OPEN, DISTRIBUTION, DELIVERED, CANCELED;
}
