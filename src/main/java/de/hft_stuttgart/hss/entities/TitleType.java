package de.hft_stuttgart.hss.entities;

public enum TitleType {
	FEMALE, MALE;
}
