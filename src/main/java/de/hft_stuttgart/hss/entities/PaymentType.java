package de.hft_stuttgart.hss.entities;

public enum PaymentType {
	BAR, BILL, PAYPAL;
}
