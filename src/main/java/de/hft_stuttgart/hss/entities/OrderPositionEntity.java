package de.hft_stuttgart.hss.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * The persistent class for the orderposition database table.
 * 
 */
@Entity(name = "OrderPosition")
@Table(name="orderposition")
public class OrderPositionEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer orderPositionId;

	@Column(nullable = false)
	private Integer number;

	@Column(nullable = false)
	private Double amount;

	@Column(nullable = false)
	private Double positionPrice;

	//bi-directional many-to-one association to Article
	@ManyToOne
	@JoinColumn(name = "Article_articleId", nullable = false)
	private ArticleEntity article;

	public OrderPositionEntity() {
	}

	public Integer getOrderPositionId() {
		return this.orderPositionId;
	}

	public void setOrderPositionId(Integer orderPositionId) {
		this.orderPositionId = orderPositionId;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getPositionPrice() {
		return this.positionPrice;
	}

	public void setPositionPrice(Double positionPrice) {
		this.positionPrice = positionPrice;
	}

	public ArticleEntity getArticle() {
		return this.article;
	}

	public void setArticle(ArticleEntity article) {
		this.article = article;
	}

}