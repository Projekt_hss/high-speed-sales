package de.hft_stuttgart.hss.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the order database table.
 * 
 */
@Entity(name = "Order")
@Table(name = "hssorder")
@NamedQueries({
		@NamedQuery(name = OrderEntity.QUERY_FIND_BY_ID, query = "SELECT NEW de.hft_stuttgart.hss.view.common.domain.context.OrderContext(o) FROM Order o WHERE o.orderId = :orderId"),
		@NamedQuery(name = OrderEntity.QUERY_FIND_ALL, query = "SELECT NEW de.hft_stuttgart.hss.view.common.domain.data.OrderSearch(o) FROM Order o WHERE o.state in :state"),
		@NamedQuery(name = OrderEntity.QUERY_SEARCH, query = "SELECT new de.hft_stuttgart.hss.view.common.domain.data.OrderSearch(o) FROM Order o join o.customer c join o.creator e WHERE (c.company like :value or o.date like :value or o.orderId like :value or c.customerId like :value or c.lastname like :value or c.prename like :value or o.state like :value or e.username like :value) and o.state in :state")
})

public class OrderEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String QUERY_FIND_BY_ID = "Order.findById";
	public static final String QUERY_FIND_ALL = "Order.findAll";
	public static final String QUERY_SEARCH = "Order.search";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer orderId;

	@Column(nullable = false)
	private LocalDateTime date;

	@Column(nullable = false)
	private Double orderTotal;

	@Column(nullable = false)
	private Double vatTotal;

	@Column(nullable = false)
	private Double discountTotal;

	@Column(nullable = false)
	private Double positionTotal;

	@Column(nullable = false)
	private Double discount;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 15)
	private OrderState state;

	@Column(nullable = false)
	private Double vat;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 15)
	private ShippingType shippingType;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 15)
	private PaymentType paymentType;

	@Column(nullable = false)
	private Boolean installation;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "Shipping_addressId", nullable = false)
	private AddressEntity shippingAddress;

	// bi-directional many-to-one association to Customer
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Customer_customerId", nullable = false)
	private CustomerEntity customer;

	// bi-directional many-to-one association to Responsible
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Creator_employeeId", nullable = false)
	private EmployeeEntity creator;

	// bi-directional many-to-one association to Orderposition
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "Order_orderId")
	private List<OrderPositionEntity> orderpositions;

	public OrderEntity() {
	}

	public OrderEntity(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getOrderId() {
		return this.orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public LocalDateTime getDate() {
		return this.date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public Double getOrderTotal() {
		return orderTotal;
	}

	public void setOrderTotal(Double orderTotal) {
		this.orderTotal = orderTotal;
	}

	public Double getVatTotal() {
		return vatTotal;
	}

	public void setVatTotal(Double vatTotal) {
		this.vatTotal = vatTotal;
	}

	public Double getDiscountTotal() {
		return discountTotal;
	}

	public void setDiscountTotal(Double discountTotal) {
		this.discountTotal = discountTotal;
	}

	public Double getPositionTotal() {
		return positionTotal;
	}

	public void setPositionTotal(Double positionTotal) {
		this.positionTotal = positionTotal;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public OrderState getState() {
		return this.state;
	}

	public void setState(OrderState state) {
		this.state = state;
	}

	public Double getVat() {
		return this.vat;
	}

	public void setVat(Double vat) {
		this.vat = vat;
	}

	public ShippingType getShippingType() {
		return shippingType;
	}

	public void setShippingType(ShippingType shippingType) {
		this.shippingType = shippingType;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public Boolean getInstallation() {
		return installation;
	}

	public void setInstallation(Boolean installation) {
		this.installation = installation;
	}

	public AddressEntity getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(AddressEntity shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public CustomerEntity getCustomer() {
		return this.customer;
	}

	public void setCustomer(CustomerEntity customer) {
		this.customer = customer;
	}

	public EmployeeEntity getCreator() {
		return creator;
	}

	public void setCreator(EmployeeEntity creator) {
		this.creator = creator;
	}

	public List<OrderPositionEntity> getOrderpositions() {
		return this.orderpositions;
	}

	public void setOrderpositions(List<OrderPositionEntity> orderpositions) {
		this.orderpositions = orderpositions;
	}

}