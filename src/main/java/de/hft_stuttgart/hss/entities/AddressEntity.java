package de.hft_stuttgart.hss.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the address database table.
 * 
 */
@Entity(name = "Address")
@Table(name = "address")
@NamedQueries({ 
	@NamedQuery(name = AddressEntity.QUERY_BY_FULL_ADDRESS, query = "SELECT a FROM Address a join a.location l where a.street = :street AND a.number = :number AND l.zip = :zip AND l.city = :city")
})
public class AddressEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String QUERY_BY_FULL_ADDRESS = "AddressEntity.ByFullAddress";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer addressId;

	@Column(nullable = false, length = 6)
	private String number;

	@Column(nullable = false, length = 45)
	private String street;

	// bi-directional many-to-one association to Customer
	@OneToMany(mappedBy = "address")
	private List<CustomerEntity> customers;

	// bi-directional many-to-one association to Location
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "Location_locationId", nullable = false)
	private LocationEntity location;

	public AddressEntity() {
	}

	public Integer getAddressId() {
		return addressId;
	}

	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}

	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public List<CustomerEntity> getCustomers() {
		return customers;
	}

	public void setCustomers(List<CustomerEntity> customers) {
		this.customers = customers;
	}

	public LocationEntity getLocation() {
		return this.location;
	}

	public void setLocation(LocationEntity location) {
		this.location = location;
	}

}