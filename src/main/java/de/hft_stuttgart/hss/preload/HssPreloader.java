package de.hft_stuttgart.hss.preload;

import java.io.IOException;

import de.hft_stuttgart.hss.services.misc.HssProgressNotification;
import javafx.application.Preloader;
import javafx.application.Preloader.StateChangeNotification.Type;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class HssPreloader extends Preloader {
	/*
	 * Fortschritt Elemente
	 */
	ProgressBar bar;
	Label text;

	// Bühne für den Splash Screen
	Stage stage;

	/**
	 * Erstelle die Oberfläche für den Splash Screen. Falls es Probleme beim
	 * Laden der FXML gibt, wird die Oberfläche "manuell" erstellt.
	 * 
	 * @return die erstellte Slpash Screen Szene.
	 */
	private Scene createPreloaderScene() {
		try {
			// Laden der Oberfläche via FXML
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("preloader.fxml"));
			Parent object = fxmlLoader.load();
			bar = (ProgressBar) object.lookup("#progress");
			text = (Label) object.lookup("#text");
			return new Scene(object);
		} catch (IOException e) {
			e.printStackTrace();
			// Backup / Manuelles Erstellen der Oberfläche
			bar = new ProgressBar(0);
			ProgressIndicator indicator = new ProgressIndicator();
			indicator.setPrefSize(25, 25);
			text = new Label("Init...");

			BorderPane p = new BorderPane();
			p.setCenter(new ImageView(new Image(getClass().getResourceAsStream("highSpeed_Logo.png"))));
			p.setBottom(new HBox(bar, indicator, text));
			return new Scene(p, 500, 200);
		}
	}

	@Override
	public void start(Stage stage) throws Exception {
		this.stage = stage;
		// Entferne alle Fenster typischen Elemente (z.B. Minimieren,
		// Maximieren, Schließen Buttons)
		stage.initStyle(StageStyle.UNDECORATED);
		stage.setScene(createPreloaderScene());
		stage.getIcons().add(new Image(getClass().getResourceAsStream("highSpeed_Icon.png")));
		stage.show();
	}

	@Override
	public void handleApplicationNotification(PreloaderNotification pn) {
		if (pn instanceof HssProgressNotification) {
			// Update fortschritt Elemente
			bar.setProgress(((HssProgressNotification) pn).getProgress());
			text.setText(((HssProgressNotification) pn).getDetails());
		} else if (pn instanceof StateChangeNotification) {
			// Schließe den Splash Screen wenn eine Status Nachricht von der
			// Anwendung kommt
			if (((StateChangeNotification) pn).getType() == Type.BEFORE_START) {
				stage.hide();
			}
		} else if (pn instanceof ErrorNotification) {
			// Wenn während dem initialen Laden ein Fehler aufgetreten ist,
			// zeige einen Fehlerdialog an und dann schließe den Splash
			// Screen
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText(((ErrorNotification) pn).getDetails());
			alert.showAndWait();
			stage.hide();
		}
	}
}
