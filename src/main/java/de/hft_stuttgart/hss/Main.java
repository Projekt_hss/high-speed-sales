package de.hft_stuttgart.hss;

import com.sun.javafx.application.LauncherImpl;

import de.hft_stuttgart.hss.preload.HssPreloader;

public class Main {

	public static void main(String[] args) {
		LauncherImpl.launchApplication(App.class, HssPreloader.class, args);
	}

}
