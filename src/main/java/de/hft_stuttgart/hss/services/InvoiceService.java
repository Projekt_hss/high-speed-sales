package de.hft_stuttgart.hss.services;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import de.hft_stuttgart.hss.entities.EmployeeEntity;
import de.hft_stuttgart.hss.entities.InvoiceEntity;
import de.hft_stuttgart.hss.entities.OrderEntity;
import de.hft_stuttgart.hss.entities.OrderState;
import de.hft_stuttgart.hss.services.misc.HssUtils;
import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.domain.data.InvoiceSearch;
import de.hft_stuttgart.hss.view.common.domain.data.OrderDetails;

@ApplicationScoped
public class InvoiceService {

	@Inject
	DatabaseService databaseService;
	@Inject
	LoginService loginService;

	public List<InvoiceSearch> searchInvoices(String searchValue) {
		return databaseService.searchInvoices(searchValue);
	}

	public List<InvoiceSearch> getAll() {
		return databaseService.getAllInvoices();
	}

	public void save(OrderContext context) {
		OrderDetails orderDetails = context.getOrder();
		EmployeeEntity employee = databaseService.find(EmployeeEntity.class, loginService.getLoggedUserId());
		OrderEntity order = databaseService.find(OrderEntity.class, orderDetails.getOrderId());

		InvoiceEntity invoiceEntity = new InvoiceEntity();
		invoiceEntity.setInvoiceDate(context.getInvoice().getInvoiceDate());
		invoiceEntity.setOrder(order);
		invoiceEntity.setCreator(employee);

		databaseService.startTx();
		databaseService.save(invoiceEntity);

		order.setState(OrderState.DELIVERED);
		databaseService.update(order);
		databaseService.commitTx();

		context.getInvoice().setInvoiceId(invoiceEntity.getInvoiceId());
		context.getInvoice().setInvoiceCreator(HssUtils.getCreatorName(employee));
	}

	public OrderContext getInvoice(Integer invoiceId) {
		return databaseService.getInvoice(invoiceId);
	}
}
