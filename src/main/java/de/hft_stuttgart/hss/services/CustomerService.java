package de.hft_stuttgart.hss.services;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import de.hft_stuttgart.hss.entities.AddressEntity;
import de.hft_stuttgart.hss.entities.CustomerEntity;
import de.hft_stuttgart.hss.entities.LocationEntity;
import de.hft_stuttgart.hss.view.common.domain.data.CustomerDetails;
import de.hft_stuttgart.hss.view.common.domain.data.CustomerSearch;

@ApplicationScoped
public class CustomerService {

	@Inject
	private DatabaseService databaseService;

	public void saveCustomer(CustomerDetails customer) {
		CustomerEntity customerEntity = mapCustomerToEntity(customer);
		mapAddressToCustomer(customer, customerEntity);
		databaseService.save(customerEntity);
	}

	private void mapAddressToCustomer(CustomerDetails customer, CustomerEntity customerEntity) {
		AddressEntity address = databaseService.getAddress(customer);

		if (address == null) {
			address = new AddressEntity();
			address.setStreet(customer.getStreet());
			address.setNumber(customer.getNumber());

			LocationEntity location = databaseService.getLocation(customer);
			if (location == null) {
				location = new LocationEntity();
				location.setCity(customer.getCity());
				location.setZip(customer.getZip());
			}

			address.setLocation(location);
		}

		customerEntity.setAddress(address);
	}

	/**
	 * Map a customer object from ui to customer object for db
	 * 
	 * @param customer
	 * @return
	 */
	private CustomerEntity mapCustomerToEntity(CustomerDetails customer) {
		CustomerEntity customerEntity = new CustomerEntity();

		if (customer.getCustomerId() > 0) {
			customerEntity.setCustomerId(customer.getCustomerId());
		}
		customerEntity.setCompany(customer.getCompany());
		customerEntity.setPrename(customer.getPrename());
		customerEntity.setLastname(customer.getLastname());
		customerEntity.setMail(customer.getMail());
		customerEntity.setMobile(customer.getMobile());
		customerEntity.setPhone(customer.getPhone());
		customerEntity.setFax(customer.getFax());
		customerEntity.setTitle(customer.getTitle());
		customerEntity.setDiscount(customer.getDiscount());

		return customerEntity;
	}

	public List<CustomerSearch> searchCustomer(String searchValue) {
		return databaseService.searchCustomers(searchValue);
	}

	public List<CustomerSearch> getAll() {
		return databaseService.getAllCustomers();
	}

	public CustomerDetails getCustomer(Integer customerId) {
		return databaseService.getCustomer(customerId);
	}

	public void deleteCustomer(Integer customerId) {
		databaseService.delete(CustomerEntity.class, customerId);
	}

	public void editCustomer(CustomerDetails customer) {
		CustomerEntity customerEntity = mapCustomerToEntity(customer);
		mapAddressToCustomer(customer, customerEntity);
		databaseService.update(customerEntity);
	}
}
