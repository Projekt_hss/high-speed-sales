package de.hft_stuttgart.hss.services.misc;

import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.CUSTOMER_CITY;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.CUSTOMER_COMPANY;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.CUSTOMER_ID;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.CUSTOMER_NAME;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.CUSTOMER_STREET;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.DISCOUNT;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.INVOICE_CREATOR;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.INVOICE_DATE;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.INVOICE_ID;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.INVOICE_PAYMENT;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.ORDER_CREATOR;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.ORDER_DATE;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.ORDER_ID;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.ORDER_SUM;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.ORDER_TOTAL;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.POSITION_TOTAL;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.SHIPPING_CITY;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.SHIPPING_COMPANY;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.SHIPPING_CREATOR;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.SHIPPING_DATE;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.SHIPPING_ID;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.SHIPPING_NAME;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.SHIPPING_STREET;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.VAT;
import static de.hft_stuttgart.hss.services.misc.ExportParameterKey.VAT_TOTAL;

import java.util.HashMap;
import java.util.Map;

import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;

/**
 * Die verschiedenen Export Typen mit den entsprechenden Reportnamen.
 */
public enum ExportType {
	ORDER("/reports/Order.jasper") {
		@Override
		public Map<String, Object> getParameters(OrderContext data, Map<Object, String> translations) {
			Map<String, Object> parameters = new HashMap<>();

			parameters.put(DISCOUNT, data.getOrder().getDiscountTotal());
			parameters.put(ORDER_SUM, data.getOrder().getOrderSum());
			parameters.put(POSITION_TOTAL, data.getOrder().getPositionTotal());
			parameters.put(VAT, data.getOrder().getVat());
			parameters.put(VAT_TOTAL, data.getOrder().getVatTotal());
			parameters.put(ORDER_TOTAL, data.getOrder().getOrderTotal());
			parameters.put(CUSTOMER_ID, data.getCustomer().getCustomerId());
			parameters.put(CUSTOMER_COMPANY, data.getCustomer().getCompany());
			parameters.put(CUSTOMER_NAME, data.getCustomer().getName());
			parameters.put(CUSTOMER_STREET, data.getCustomer().getStreetAndNumber());
			parameters.put(CUSTOMER_CITY, data.getCustomer().getZipAndCity());
			parameters.put(ORDER_ID, data.getOrder().getOrderId());
			parameters.put(ORDER_DATE, HssUtils.fromLocalDateTime(data.getOrder().getOrderDate()));
			parameters.put(ORDER_CREATOR, data.getOrder().getCreator());

			return parameters;
		}
	},
	SHIPPING("/reports/Shipping.jasper") {
		@Override
		public Map<String, Object> getParameters(OrderContext data, Map<Object, String> translations) {
			Map<String, Object> parameters = new HashMap<>();

			parameters.put(CUSTOMER_ID, data.getCustomer().getCustomerId());
			parameters.put(ORDER_ID, data.getOrder().getOrderId());
			parameters.put(ORDER_DATE, HssUtils.fromLocalDateTime(data.getOrder().getOrderDate()));
			parameters.put(SHIPPING_COMPANY, data.getCustomer().getCompany());
			parameters.put(SHIPPING_NAME, data.getCustomer().getName());
			parameters.put(SHIPPING_STREET, data.getShipping().getStreetAndNumber());
			parameters.put(SHIPPING_CITY, data.getShipping().getZipAndCity());
			parameters.put(SHIPPING_ID, data.getShipping().getShippingOrderId());
			parameters.put(SHIPPING_DATE, HssUtils.fromLocalDate(data.getShipping().getShippingDate()));
			parameters.put(SHIPPING_CREATOR, data.getShipping().getShippingCreator());

			return parameters;
		}
	},
	INVOICE("/reports/Invoice.jasper") {
		@Override
		public Map<String, Object> getParameters(OrderContext data, Map<Object, String> translations) {
			Map<String, Object> parameters = new HashMap<>();

			parameters.put(DISCOUNT, data.getOrder().getDiscountTotal());
			parameters.put(ORDER_SUM, data.getOrder().getOrderSum());
			parameters.put(POSITION_TOTAL, data.getOrder().getPositionTotal());
			parameters.put(VAT, data.getOrder().getVat());
			parameters.put(VAT_TOTAL, data.getOrder().getVatTotal());
			parameters.put(ORDER_TOTAL, data.getOrder().getOrderTotal());
			parameters.put(CUSTOMER_ID, data.getCustomer().getCustomerId());
			parameters.put(CUSTOMER_COMPANY, data.getCustomer().getCompany());
			parameters.put(CUSTOMER_NAME, data.getCustomer().getName());
			parameters.put(CUSTOMER_STREET, data.getCustomer().getStreetAndNumber());
			parameters.put(CUSTOMER_CITY, data.getCustomer().getZipAndCity());
			parameters.put(ORDER_ID, data.getOrder().getOrderId());
			parameters.put(ORDER_DATE, HssUtils.fromLocalDateTime(data.getOrder().getOrderDate()));
			parameters.put(INVOICE_PAYMENT, translations.get(data.getOrder().getPaymentType()));
			parameters.put(INVOICE_DATE, HssUtils.fromLocalDate(data.getInvoice().getInvoiceDate()));
			parameters.put(INVOICE_ID, data.getInvoice().getInvoiceId());
			parameters.put(INVOICE_CREATOR, data.getInvoice().getInvoiceCreator());

			return parameters;
		}
	};

	/**
	 * Dateiname des Reports
	 */
	private String report;

	private ExportType(String report) {
		this.report = report;
	}

	/**
	 * @return gibt den Dateinamen des Reports zurück.
	 */
	public String getReport() {
		return report;
	}

	/**
	 * Erstellt die Parameterliste für den Report aus dem {@link OrderContext}
	 * 
	 * @param data
	 *            der OrderContext mit den Daten zur Bestellung.
	 * @param translations
	 *            etwaige Übersetzungen für den Report.
	 * @return eine Map aus Keys (siehe {@link ExportParameterKey}) und Werten
	 *         aus dem OrderContext bezogen auf den Report
	 */
	public abstract Map<String, Object> getParameters(OrderContext data, Map<Object, String> translations);
}
