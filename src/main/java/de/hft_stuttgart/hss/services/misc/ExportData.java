package de.hft_stuttgart.hss.services.misc;

/**
 * Positionsdaten für die Reports.
 */
public class ExportData {
	private Integer pos;
	private Integer articleId;
	private String articleName;
	private Double amount;
	private String unity;
	private Double singlePrice;
	private Double totalPrice;

	public ExportData() {
	}

	public ExportData(Integer pos, Integer articleId, String articleName, Double amount, String unity, Double singlePrice, Double totalPrice) {
		this.pos = pos;
		this.articleId = articleId;
		this.articleName = articleName;
		this.amount = amount;
		this.unity = unity;
		this.singlePrice = singlePrice;
		this.totalPrice = totalPrice;
	}

	public Integer getPos() {
		return pos;
	}

	public void setPos(Integer pos) {
		this.pos = pos;
	}

	public Integer getArticleId() {
		return articleId;
	}

	public void setArticleId(Integer articleId) {
		this.articleId = articleId;
	}

	public String getArticleName() {
		return articleName;
	}

	public void setArticleName(String articleName) {
		this.articleName = articleName;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getUnity() {
		return unity;
	}

	public void setUnity(String unity) {
		this.unity = unity;
	}

	public Double getSinglePrice() {
		return singlePrice;
	}

	public void setSinglePrice(Double singlePrice) {
		this.singlePrice = singlePrice;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

}
