package de.hft_stuttgart.hss.services.misc;

import java.util.Properties;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

@ApplicationScoped
public class EntityManagerProducer {

	/**
	 * Datenbank Credentials erzeugt von {@link DBCredentialsProducer}
	 */
	@Inject
	@DbCredentials
	Properties properties;

	/**
	 * Erzeugermethode für den in der Anwendung einmaligen JPA (Hibernate)
	 * {@link EntityManager}. Damit kann der EntityManager überall mit @Inject
	 * verwendet werden.
	 * 
	 * @return den JPA EntityManager
	 */
	@Produces
	@ApplicationScoped
	public EntityManager getEM() {
		return Persistence.createEntityManagerFactory("hss", properties).createEntityManager();
	}

	/**
	 * Wenn die Anwendung Heruntergefahren wird, wird die Datenbankverbindung
	 * geschlossen. Diese Methode wird automatisch aufgerufen und muss nicht
	 * manuell aufgerufen werden.
	 * 
	 * @param em
	 *            Der Aktuelle EntityManager.
	 */
	public void closeEm(@Disposes EntityManager em) {
		if (em.isOpen()) {
			em.close();
		}
	}
}
