package de.hft_stuttgart.hss.services.misc;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import de.hft_stuttgart.hss.entities.EmployeeEntity;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringExpression;
import javafx.beans.property.StringProperty;

/**
 * Beinhaltet verschiedene Hilfsmethoden für die Applikation.
 */
public class HssUtils {
	/**
	 * Diese Klasse soll von außen nicht instanziiert werden können, da sie nur
	 * statische Hilfsmethoden beinhaltet.
	 */
	private HssUtils() {
	}

	/**
	 * Erzeug ein {@link java.util.Date} (Benutzung Java < 1.8, Datum und Zeit)
	 * Object aus einem {@link java.time.LocalDate} (Benutzung Java >= 1.8, nur
	 * Datum) Object
	 * 
	 * @param date
	 *            Das zu konvertierenede LocalDate Object
	 * @return das konvertierte Date Object
	 */
	public static Date fromLocalDate(LocalDate date) {
		return Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	/**
	 * Erzeug ein {@link java.util.Date} (Benutzung Java < 1.8, Datum und Zeit)
	 * Object aus einem {@link java.time.LocalDateTime} (Benutzung Java >= 1.8,
	 * Datum und Zeit) Object
	 * 
	 * @param date
	 *            Das zu konvertierenede LocalDate Object
	 * @return das konvertierte Date Object
	 */
	public static Date fromLocalDateTime(LocalDateTime orderDate) {
		return Date.from(orderDate.atZone(ZoneId.systemDefault()).toInstant());
	}

	/**
	 * Verbindet zwei StringProperties für das Binding. Damit können z.B. Vor-
	 * und Nachname in einem Textfeld angezeigt werden. Das Ergebnis sieht wie
	 * folgt aus: "{first} {last}"
	 * 
	 * @param first
	 *            Das erste StringProperty
	 * @param last
	 *            Das zweite StringProperty
	 * @return ein an ein Textfeld bindbares Object das dem Format wie oben
	 *         beschrieben entspricht.
	 */
	public static StringExpression concat(StringProperty first, StringProperty last) {
		return Bindings.concat(first, " ", last);
	}

	/**
	 * Mappe eine Liste von beliebigen Objecten auf eine Liste von
	 * {@link ExportData} Objekten zur Verwendung mit Reports. Verwendet wird
	 * die Java Stream API (seit Java 1.8)
	 * 
	 * @param data
	 *            Die Liste and Objekte, die gemappt werden soll.
	 * @param mapper
	 *            Eine Mappingfunktion, die das Mapping eines einzelnen Objekts
	 *            übernimmt
	 * @return eine Liste an ExportData Objekten
	 */
	public static <T> List<ExportData> mapData(List<T> data, Function<? super T, ? extends ExportData> mapper) {
		return data.stream().parallel().map(mapper).collect(Collectors.toList());
	}

	/**
	 * Erzeuge den Bearbeiternamen aus dem Mitarbeiter Object. Das Ergebnis
	 * sieht wie folgt aus: "{Vorname} {Nachname}"
	 * 
	 * @param employee
	 *            Das Mitarbeiter Object.
	 * @return ein String, der den Vor- und Nachnamen des Mitarbeiters enthält.
	 */
	public static String getCreatorName(EmployeeEntity employee) {
		return employee.getPrename() + " " + employee.getLastname();
	}

}
