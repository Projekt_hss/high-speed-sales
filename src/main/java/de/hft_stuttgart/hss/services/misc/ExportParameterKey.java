package de.hft_stuttgart.hss.services.misc;

/**
 * Liste an Parameter Keys für Reports. Variablen in Interfaces sind per
 * Konvention "public static final", daher wird hier auf die mehrfache
 * Ausführung verzichtet.
 */
public interface ExportParameterKey {
	// Numbers
	String DISCOUNT = "discount";
	String ORDER_SUM = "sum";
	String POSITION_TOTAL = "posSum";
	String VAT = "vat";
	String VAT_TOTAL = "mwst";
	String ORDER_TOTAL = "total";
	// Customer
	String CUSTOMER_ID = "customerId";
	String CUSTOMER_COMPANY = "customerCompany";
	String CUSTOMER_NAME = "customerName";
	String CUSTOMER_STREET = "customerStreet";
	String CUSTOMER_CITY = "customerCity";
	// Order
	String ORDER_ID = "orderId";
	String ORDER_DATE = "orderDate";
	String ORDER_CREATOR = "orderCreator";
	// Shipping
	String SHIPPING_COMPANY = "shippingCompany";
	String SHIPPING_NAME = "shippingName";
	String SHIPPING_STREET = "shippingStreet";
	String SHIPPING_CITY = "shippingCity";
	String SHIPPING_ID = "shippingOrderId";
	String SHIPPING_DATE = "shippingDate";
	String SHIPPING_CREATOR = "shippingCreator";
	// Invoice
	String INVOICE_PAYMENT = "paymentType";
	String INVOICE_DATE = "invoiceDate";
	String INVOICE_ID = "invoiceId";
	String INVOICE_CREATOR = "invoiceCreator";
}
