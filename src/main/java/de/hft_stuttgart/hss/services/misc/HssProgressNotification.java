package de.hft_stuttgart.hss.services.misc;

import javafx.application.Preloader.PreloaderNotification;

/**
 * ################################
 * COPY FROM JavaFX Runtime Library
 * ################################
 */
public class HssProgressNotification implements PreloaderNotification {
	private final double progress;
	private final String details;

	/**
     * Constructs a progress notification.
     *
     * @param progress a value indicating the progress.
     * A negative value for progress indicates that the progress is
     * indeterminate. A value between 0 and 1 indicates the amount
     * of progress. Any value greater than 1 is interpreted as 1.
     */
	public HssProgressNotification(double progress) {
        this(progress, "");
    }

	// NOTE: We could consider exposing details in the future, but currently
	// have no plan to do so. This method is private for now.
	/**
     * Constructs a progress notification.
     *
     * @param progress a value indicating the progress.
     * A negative value for progress indicates that the progress is
     * indeterminate. A value between 0 and 1 indicates the amount
     * of progress. Any value greater than 1 is interpreted as 1.
     *
     * @param details the details of this notification
     */
	public HssProgressNotification(double progress, String details) {
        this.progress = progress;
        this.details = details;
    }

	/**
	 * Retrieves the progress for this notification. Progress is in the range of
	 * 0 to 1, or is negative for indeterminate progress.
	 *
	 * @return the progress
	 */
	public double getProgress() {
		return progress;
	}

	/**
	 * Retrieves the details of the progress notification
	 *
	 * @return the details of this notification
	 */
	public String getDetails() {
		return details;
	}
}
