package de.hft_stuttgart.hss.services.misc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import javafx.concurrent.Task;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * Export Implementierung. Wird in einem separaten Thread ausgeführt, damit die
 * OBerfläche nicht blockiert wird.
 */
public class HssExportTask extends Task<Void> {

	/**
	 * Die Datei, wohin exportiert werden soll
	 */
	private File file;
	/**
	 * Der Name des zu exportiernden Reports
	 */
	private String report;
	/**
	 * Die Erzeugerfunktion für die statischen Parametern des Report
	 */
	private Supplier<Map<String, Object>> parameterSupplier;
	/**
	 * Die Erzeugerfunktion für die sich wiederholenden Positionen des Reports
	 */
	private Supplier<List<ExportData>> dataSupplier;

	public HssExportTask(String report, File file, Supplier<Map<String, Object>> parameterSupplier, Supplier<List<ExportData>> dataSupplier) {
		this.report = report;
		this.file = file;
		this.parameterSupplier = parameterSupplier;
		this.dataSupplier = dataSupplier;
	}

	@Override
	protected Void call() throws Exception {
		updateProgress(3, 10);
		updateMessage("Prepare Parameters");
		Map<String, Object> parameters = parameterSupplier.get();

		updateProgress(4, 10);
		updateMessage("Prepare Fields");
		List<ExportData> data = dataSupplier.get();
		JRDataSource dataSource = new JRBeanCollectionDataSource(data);

		updateProgress(5, 10);
		updateMessage("Prepare path");
		// Wenn der Dateinamen nicht mit ".pdf" aufhört, füge die Endung an den
		// Dateinamen an
		String path;
		if (file.getAbsolutePath().endsWith(".pdf")) {
			path = file.getAbsolutePath();
		} else {
			path = file.getAbsolutePath() + ".pdf";
		}

		updateProgress(6, 10);
		updateMessage("Prepare file");
		// Öffne die Datei zum schreiben (FileOutputStream). Die Datei wird am
		// Ende des Try Blocks automatisch geschlossen und festgeschrieben.
		try (FileOutputStream outputStream = new FileOutputStream(path)) {
			// Lade die Report Datei (.jasper)
			updateProgress(7, 10);
			updateMessage("Load template");
			InputStream inputStream = getClass().getResourceAsStream(report);
			// Befülle den Report mit den oben erzeugten Daten
			updateProgress(8, 10);
			updateMessage("Fill template");
			JasperPrint report = JasperFillManager.fillReport(inputStream, parameters, dataSource);
			// Speichere den Report in die geöffnete Datei
			updateProgress(9, 10);
			updateMessage("Save file");
			JasperExportManager.exportReportToPdfStream(report, outputStream);
			updateProgress(10, 10);
			updateMessage("Finish");
		}

		return null;
	}

}
