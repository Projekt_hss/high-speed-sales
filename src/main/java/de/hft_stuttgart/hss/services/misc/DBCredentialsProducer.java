package de.hft_stuttgart.hss.services.misc;

import java.util.Properties;

import javax.enterprise.inject.Produces;

public class DBCredentialsProducer {

	/**
	 * Erzeugermethode für die Datenbank Credentials. Mögliche Parameter sind:
	 * <ul>
	 * <li>db.url
	 * <li>db.user
	 * <li>db.password
	 * </ul>
	 * Beispiel zum Übergeben des Users/Passworts der SQL DB <br>
	 * java -Ddb.user=root -Dbd.password=root -jar highspeedsales.jar
	 * 
	 * @return eine Map mit den entsprechenden Keys und den Werten aus den JVM
	 *         Parametern
	 */
	@Produces
	@DbCredentials
	public Properties getDbCredentials() {
		Properties properties = new Properties();
		String url = System.getProperty("db.url");
		if (url != null && !url.trim().isEmpty()) {
			properties.put("javax.persistence.jdbc.url", url);
		}
		String user = System.getProperty("db.user");
		if (user != null && !user.trim().isEmpty()) {
			properties.put("javax.persistence.jdbc.user", user);
		}
		String password = System.getProperty("db.password");
		if (password != null && !password.trim().isEmpty()) {
			properties.put("javax.persistence.jdbc.password", password);
		}
		return properties;
	}
}
