package de.hft_stuttgart.hss.services.misc;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

public class ServiceLocator {

	private WeldContainer weldContainer;
	private Weld weld;

	/**
	 * Erstellt Container
	 */
	public ServiceLocator() {
		weld = new Weld();
	}

	/**
	 * Initialisiert den Container
	 */
	public void initialize() {
		weldContainer = weld.initialize();
	}

	/**
	 * Anfordern einer Instanz zu einer Klasse vom Container
	 * 
	 * @param serviceClass
	 *            Die Klasse, für die eine Instanz angefragt wird.
	 * @return eine Instanz zu der Klasse
	 */
	public <T> T getInstance(Class<T> serviceClass) {
		return weldContainer.instance().select(serviceClass).get();
	}

	/**
	 * Herunterfahren des Containers (wird nicht automatisch aufgerufen)
	 */
	public void close() {
		weld.shutdown();
	}

}
