package de.hft_stuttgart.hss.services;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import de.hft_stuttgart.hss.entities.EmployeeEntity;
import de.hft_stuttgart.hss.entities.OrderEntity;
import de.hft_stuttgart.hss.entities.OrderState;
import de.hft_stuttgart.hss.entities.ShippingOrderEntity;
import de.hft_stuttgart.hss.services.misc.HssUtils;
import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.domain.data.ShippingSearch;

@ApplicationScoped
public class ShippingOrderService {

	@Inject
	DatabaseService databaseService;
	@Inject
	LoginService loginService;

	public List<ShippingSearch> searchShippings(String searchValue) {
		return databaseService.searchShippings(searchValue);
	}

	public List<ShippingSearch> getAll() {
		return databaseService.getAllShippings();
	}

	public void save(OrderContext context) {
		EmployeeEntity employee = databaseService.find(EmployeeEntity.class, loginService.getLoggedUserId());
		OrderEntity order = databaseService.find(OrderEntity.class, context.getOrder().getOrderId());
		ShippingOrderEntity shippingOrder = new ShippingOrderEntity();
		shippingOrder.setShippingOrderDate(context.getShipping().getShippingDate());
		shippingOrder.setOrder(order);
		shippingOrder.setCreator(employee);

		databaseService.startTx();
		databaseService.save(shippingOrder);

		order.setState(OrderState.DISTRIBUTION);
		databaseService.update(order);
		databaseService.commitTx();

		context.getShipping().setToExtended();
		context.getShipping().setShippingId(shippingOrder.getShippingOrderId());
		context.getShipping().setShippingCreator(HssUtils.getCreatorName(employee));
	}

	public OrderContext getShipping(Integer shippingId) {
		return databaseService.getShipping(shippingId);
	}
	
}
