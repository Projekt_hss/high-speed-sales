package de.hft_stuttgart.hss.services;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import de.hft_stuttgart.hss.view.common.domain.data.User;

@ApplicationScoped
public class LoginService {
	private Integer loggedUserId;

	@Inject
	private DatabaseService databaseService;

	public boolean doLogin(User user) {
		Integer userId = databaseService.isUserAuthenticated(user);

		if (userId != null && !userId.equals(Integer.valueOf(-1))) {
			loggedUserId = userId;
			return true;
		}

		return false;
	}


	public Integer getLoggedUserId() {
		return loggedUserId;
	}

}