package de.hft_stuttgart.hss.services;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.enterprise.context.ApplicationScoped;

import de.hft_stuttgart.hss.services.misc.ExportData;
import de.hft_stuttgart.hss.services.misc.ExportType;
import de.hft_stuttgart.hss.services.misc.HssExportTask;
import de.hft_stuttgart.hss.services.misc.HssUtils;
import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import javafx.beans.binding.Bindings;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

@ApplicationScoped
public class ExportService {
	
	/**
	 * Selektiert eine Datei mit einem "Speichern Dialog" (wie aus dem
	 * jeweiligen OS bekannt). Wenn eine Datei ausgewählt wurde, wird der Export
	 * in einem separaten Thread ausgeführt.
	 * 
	 * @param type
	 *            Der zu exportierende {@link ExportType}
	 * @param data
	 *            Die Daten zur Bestellung
	 * @param saveDialogTitleSupplier
	 *            Funktion zur Bereitstelung des "Speichern Dialog" Titels
	 * @param progressConsumer
	 *            Funktion zur Anteige eines Progress Dialogs in Verbindug mit
	 *            dem Export Thread
	 * @param successConsumer
	 *            Funktion, die Ausgeführt werden soll, wenn der Export
	 *            erfolgreich ausgeführt wurde
	 * @param parentSupplier
	 *            Funktion, welche die aufrufende Bühne bereitstellt
	 * @param translations
	 *            Eine List an Übersetzungen für den Report
	 */
	public void export(ExportType type, OrderContext data, Function<String, String> saveDialogTitleSupplier, BiConsumer<Task<?>, Consumer<WorkerStateEvent>> progressConsumer,
			Consumer<WorkerStateEvent> successConsumer, Supplier<Stage> parentSupplier, Map<Object, String> translations) {
		
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle(saveDialogTitleSupplier.apply("print.dialog.title"));
		fileChooser.getExtensionFilters().add(new ExtensionFilter("PDF Files", "*.pdf"));
		File file = fileChooser.showSaveDialog(parentSupplier.get());

		HssExportTask exportTask = null;
		if (file != null) {
			Supplier<Map<String, Object>> parameterSupplier = () -> type.getParameters(data, translations);
			Supplier<List<ExportData>> dataSupplier = () -> HssUtils.mapData(data.getPositions(), (pos) -> new ExportData(pos.positionProperty().get(), pos.articleIdProperty().get(), pos.articleNameProperty().get(), 
					pos.amountProperty().get(), pos.unityProperty().get(), pos.singlePriceProperty().get(), Bindings.multiply(pos.amountProperty(), 
					pos.singlePriceProperty()).getValue().doubleValue()));
			
			exportTask = new HssExportTask(type.getReport(), file, parameterSupplier, dataSupplier);
			new Thread(exportTask).start();
		}

		progressConsumer.accept(exportTask, successConsumer);
	}

}
