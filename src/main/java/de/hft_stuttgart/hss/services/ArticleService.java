package de.hft_stuttgart.hss.services;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import de.hft_stuttgart.hss.view.common.domain.data.ArticleSearch;

@ApplicationScoped
public class ArticleService {

	@Inject
	private DatabaseService databaseService;

	public List<ArticleSearch> searchArticles(String searchValue) {
		return databaseService.searchArticles(searchValue);
	}

	public List<ArticleSearch> getAll() {
		return databaseService.getAllArticles();
	}

	public List<ArticleSearch> getAllNotContained(List<Integer> articleIds) {
		return databaseService.getAllArticlesWhichAreNotContained(articleIds);
	}
	
}
