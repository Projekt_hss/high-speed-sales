package de.hft_stuttgart.hss.services;

public interface AddressAware {
	String getStreet();

	String getNumber();

	String getZip();

	String getCity();
}
