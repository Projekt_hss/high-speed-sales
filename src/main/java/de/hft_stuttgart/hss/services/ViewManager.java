package de.hft_stuttgart.hss.services;

import javax.enterprise.context.ApplicationScoped;

import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

@ApplicationScoped
public class ViewManager {

	private BorderPane rootGrid;
	private Stage primaryStage;
	private Stage dialogStage;
	private Stage progressStage;

	public BorderPane getRootGrid() {
		return rootGrid;
	}

	public void setRootGrid(BorderPane rootGrid) {
		this.rootGrid = rootGrid;
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	public Stage getDialogStage() {
		return dialogStage;
	}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public Stage getProgressStage() {
		return progressStage;
	}

	public void setProgressStage(Stage progressStage) {
		this.progressStage = progressStage;
	}

}

