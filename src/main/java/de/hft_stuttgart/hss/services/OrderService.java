package de.hft_stuttgart.hss.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import de.hft_stuttgart.hss.entities.AddressEntity;
import de.hft_stuttgart.hss.entities.ArticleEntity;
import de.hft_stuttgart.hss.entities.CustomerEntity;
import de.hft_stuttgart.hss.entities.EmployeeEntity;
import de.hft_stuttgart.hss.entities.LocationEntity;
import de.hft_stuttgart.hss.entities.OrderEntity;
import de.hft_stuttgart.hss.entities.OrderPositionEntity;
import de.hft_stuttgart.hss.entities.OrderState;
import de.hft_stuttgart.hss.services.misc.HssUtils;
import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.domain.data.CustomerDetails;
import de.hft_stuttgart.hss.view.common.domain.data.OrderDetails;
import de.hft_stuttgart.hss.view.common.domain.data.OrderPosition;
import de.hft_stuttgart.hss.view.common.domain.data.OrderSearch;
import de.hft_stuttgart.hss.view.common.domain.data.ShippingDetails;
import javafx.collections.ObservableList;

@ApplicationScoped
public class OrderService {

	@Inject
	private LoginService loginService;

	@Inject
	private DatabaseService databaseService;

	private OrderState[] states;

	public void saveOrder(OrderContext context) {
		EmployeeEntity employee = databaseService.find(EmployeeEntity.class, loginService.getLoggedUserId());
		
		OrderEntity order = new OrderEntity();
		order.setCreator(employee);
		order.setCustomer(new CustomerEntity(context.getCustomer().getCustomerId()));
		order.setDate(LocalDateTime.now());

		mapOrderToOrder(order, context.getOrder());
		mapPositionsToOrder(order, context.getPositions());
		mapShippingAddressToOrder(order, context.getShipping(), context.getCustomer());

		databaseService.save(order);

		OrderDetails orderDetails = context.getOrder();
		orderDetails.orderIdProperty().set(order.getOrderId());
		orderDetails.orderDateProperty().set(order.getDate());
		orderDetails.creatorProperty().set(HssUtils.getCreatorName(employee));
	}

	private void mapShippingAddressToOrder(OrderEntity order, ShippingDetails shippingDetails, CustomerDetails customerDetails) {
		AddressAware searchCriteria;
		if (shippingDetails.isShippingAddressAvailable()) {
			searchCriteria = shippingDetails;
		} else {
			searchCriteria = customerDetails;
		}

		AddressEntity address = databaseService.getAddress(searchCriteria);
		if (address == null) {
			address = new AddressEntity();
			address.setStreet(searchCriteria.getStreet());
			address.setNumber(searchCriteria.getNumber());

			LocationEntity location = databaseService.getLocation(searchCriteria);
			if (location == null) {
				location = new LocationEntity();
				location.setCity(searchCriteria.getCity());
				location.setZip(searchCriteria.getZip());
			}

			address.setLocation(location);
		}

		order.setShippingAddress(address);
	}

	private void mapPositionsToOrder(OrderEntity order, ObservableList<OrderPosition> orderPositions) {
		List<OrderPositionEntity> orderPositionEnities = new ArrayList<>();
		order.setOrderpositions(orderPositionEnities);
		for (OrderPosition orderPosition : orderPositions) {
			OrderPositionEntity pos = new OrderPositionEntity();
			pos.setOrderPositionId(orderPosition.getOrderPositionid());
			pos.setNumber(orderPosition.positionProperty().get());
			pos.setAmount(orderPosition.getAmount());
			pos.setArticle(new ArticleEntity(orderPosition.articleIdProperty().get()));
			pos.setPositionPrice(orderPosition.getSinglePrice() * orderPosition.getAmount());
			orderPositionEnities.add(pos);
		}
	}

	private void mapOrderToOrder(OrderEntity order, OrderDetails orderDelivery) {
		order.setDiscount(orderDelivery.getDiscount());
		order.setInstallation(orderDelivery.getInstallation());
		order.setPaymentType(orderDelivery.getPaymentType());
		order.setShippingType(orderDelivery.getShippingType());
		order.setPositionTotal(orderDelivery.getPositionTotal());
		order.setDiscountTotal(orderDelivery.getDiscountTotal());
		order.setVatTotal(orderDelivery.getVatTotal());
		order.setOrderTotal(orderDelivery.getOrderTotal());
		order.setState(OrderState.OPEN);
		order.setVat(orderDelivery.getVat());
	}


	public List<OrderSearch> searchOrder(String searchValue) {
		return databaseService.searchOrders(searchValue, getOrderStateList(states));
	}

	public List<OrderSearch> getAll() {
		return databaseService.getAllOrders(getOrderStateList(states));
	}

	private List<OrderState> getOrderStateList(OrderState[] states) {
		if (states == null || states.length == 0) {
			return Arrays.asList(OrderState.values());
		}

		return Arrays.asList(states);
	}

	public OrderContext getOrder(Integer orderId) {
		return databaseService.getOrder(orderId);
	}

	public void cancelOrder(Integer orderId) {
		OrderEntity order = databaseService.find(OrderEntity.class, orderId);
		order.setState(OrderState.CANCELED);
		databaseService.update(order);
	}

	public void updateOrder(OrderContext context) {
		EmployeeEntity employee = databaseService.find(EmployeeEntity.class, loginService.getLoggedUserId());

		OrderEntity order = new OrderEntity();
		order.setOrderId(context.getOrder().orderIdProperty().get());
		order.setCreator(employee);
		order.setCustomer(new CustomerEntity(context.getCustomer().getCustomerId()));
		order.setDate(context.getOrder().orderDateProperty().get());
		mapOrderToOrder(order, context.getOrder());
		mapPositionsToOrder(order, context.getPositions());
		mapShippingAddressToOrder(order, context.getShipping(), context.getCustomer());

		databaseService.update(order);
	}

	public void setStates(OrderState[] states) {
		this.states = states;
	}
}
