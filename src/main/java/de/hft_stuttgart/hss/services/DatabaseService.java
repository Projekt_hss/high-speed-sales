package de.hft_stuttgart.hss.services;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import de.hft_stuttgart.hss.entities.AddressEntity;
import de.hft_stuttgart.hss.entities.ArticleEntity;
import de.hft_stuttgart.hss.entities.CustomerEntity;
import de.hft_stuttgart.hss.entities.EmployeeEntity;
import de.hft_stuttgart.hss.entities.InvoiceEntity;
import de.hft_stuttgart.hss.entities.LocationEntity;
import de.hft_stuttgart.hss.entities.OrderEntity;
import de.hft_stuttgart.hss.entities.OrderState;
import de.hft_stuttgart.hss.entities.ShippingOrderEntity;
import de.hft_stuttgart.hss.view.common.domain.context.OrderContext;
import de.hft_stuttgart.hss.view.common.domain.data.ArticleSearch;
import de.hft_stuttgart.hss.view.common.domain.data.CustomerDetails;
import de.hft_stuttgart.hss.view.common.domain.data.CustomerSearch;
import de.hft_stuttgart.hss.view.common.domain.data.InvoiceSearch;
import de.hft_stuttgart.hss.view.common.domain.data.OrderSearch;
import de.hft_stuttgart.hss.view.common.domain.data.ShippingSearch;
import de.hft_stuttgart.hss.view.common.domain.data.User;

@ApplicationScoped
public class DatabaseService {

	@Inject
	private EntityManager entityManager;

	public Integer isUserAuthenticated(User user) {
		TypedQuery<Integer> query = entityManager.createNamedQuery(EmployeeEntity.IS_AUTHENTICATED, Integer.class);
		query.setParameter("username", user.getUsername());
		query.setParameter("password", user.getPassword());
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return Integer.valueOf(-1);
		}
	}

	public List<CustomerSearch> getAllCustomers() {
		return entityManager.createNamedQuery(CustomerEntity.QUERY_FIND_ALL, CustomerSearch.class).getResultList();
	}

	public void save(Object entity) {
		boolean txActive = entityManager.getTransaction().isActive();
		if (!txActive) {
			startTx();
		}
		entityManager.persist(entity);
		if (!txActive) {
			commitTx();
		}
		entityManager.refresh(entity);
	}

	public <T> T find(Class<T> type, Object id) {
		return entityManager.find(type, id);
	}

	public <T> T update(T entity) {
		boolean txActive = entityManager.getTransaction().isActive();
		if (!txActive) {
			startTx();
		}
		T merged = entityManager.merge(entity);
		if (!txActive) {
			commitTx();
		}
		return merged;
	}

	public <T> void delete(Class<T> type, Integer id) {
		boolean txActive = entityManager.getTransaction().isActive();
		if (!txActive) {
			startTx();
		}
		T t = entityManager.find(type, id);
		entityManager.remove(t);
		if (!txActive) {
			commitTx();
		}
	}

	public void startTx() {
		entityManager.getTransaction().begin();
	}

	public void commitTx() {
		try {
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
			throw e;
		}
	}

	public List<CustomerSearch> searchCustomers(String searchValue) {
		TypedQuery<CustomerSearch> query = entityManager.createNamedQuery(CustomerEntity.QUERY_SEARCH,
				CustomerSearch.class);
		query.setParameter("value", "%" + searchValue + "%");
		return query.getResultList();
	}

	public AddressEntity getAddress(AddressAware addressAwareObject) {
		TypedQuery<AddressEntity> query = entityManager.createNamedQuery(AddressEntity.QUERY_BY_FULL_ADDRESS,
				AddressEntity.class);
		query.setParameter("street", addressAwareObject.getStreet());
		query.setParameter("number", addressAwareObject.getNumber());
		query.setParameter("zip", addressAwareObject.getZip());
		query.setParameter("city", addressAwareObject.getCity());

		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public LocationEntity getLocation(AddressAware locationEntity) {
		TypedQuery<LocationEntity> query = entityManager.createNamedQuery(LocationEntity.QUERY_BY_FULL_LOCATION,
				LocationEntity.class);
		query.setParameter("zip", locationEntity.getZip());
		query.setParameter("city", locationEntity.getCity());

		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<ArticleSearch> getAllArticles() {
		return entityManager.createNamedQuery(ArticleEntity.QUERY_FIND_ALL, ArticleSearch.class).getResultList();
	}

	public List<ArticleSearch> searchArticles(String searchValue) {
		TypedQuery<ArticleSearch> query = entityManager.createNamedQuery(ArticleEntity.QUERY_SEARCH,
				ArticleSearch.class);
		query.setParameter("value", "%" + searchValue + "%");
		return query.getResultList();
	}

	public CustomerDetails getCustomer(Integer customerId) {
		TypedQuery<CustomerDetails> query = entityManager.createNamedQuery(CustomerEntity.QUERY_FIND_BY_ID, CustomerDetails.class);
		query.setParameter("customerId", customerId);
		return query.getSingleResult();
	}

	public List<OrderSearch> getAllOrders(List<OrderState> list) {
		TypedQuery<OrderSearch> query = entityManager.createNamedQuery(OrderEntity.QUERY_FIND_ALL, OrderSearch.class);
		query.setParameter("state", list);
		return query.getResultList();
	}

	public List<ArticleSearch> getAllArticlesWhichAreNotContained(List<Integer> articleIds) {
		TypedQuery<ArticleSearch> query = entityManager.createNamedQuery(ArticleEntity.QUERY_FIND_ALL_NOT_CONTAINED, ArticleSearch.class);
		query.setParameter("articleIds", articleIds);
		return query.getResultList();
	}

	public List<OrderSearch> searchOrders(String searchValue, List<OrderState> list) {
		TypedQuery<OrderSearch> query = entityManager.createNamedQuery(OrderEntity.QUERY_SEARCH, OrderSearch.class);
		query.setParameter("value", "%" + searchValue + "%");
		query.setParameter("state", list);
		return query.getResultList();
	}

	public OrderContext getOrder(Integer orderId) {
		TypedQuery<OrderContext> query = entityManager.createNamedQuery(OrderEntity.QUERY_FIND_BY_ID, OrderContext.class);
		query.setParameter("orderId", orderId);
		return query.getSingleResult();
	}

	public List<InvoiceSearch> searchInvoices(String searchValue) {
		TypedQuery<InvoiceSearch> query = entityManager.createNamedQuery(InvoiceEntity.QUERY_SEARCH, InvoiceSearch.class);
		query.setParameter("value", "%" + searchValue + "%");
		return query.getResultList();
	}

	public List<InvoiceSearch> getAllInvoices() {
		return entityManager.createNamedQuery(InvoiceEntity.QUERY_FIND_ALL, InvoiceSearch.class).getResultList();
	}

	public OrderContext getInvoice(Integer invoiceId) {
		TypedQuery<OrderContext> query = entityManager.createNamedQuery(InvoiceEntity.QUERY_FIND_BY_ID, OrderContext.class);
		query.setParameter("invoiceId", invoiceId);
		return query.getSingleResult();
	}

	public List<ShippingSearch> getAllShippings() {
		return entityManager.createNamedQuery(ShippingOrderEntity.QUERY_FIND_ALL, ShippingSearch.class).getResultList();
	}

	public List<ShippingSearch> searchShippings(String searchValue) {
		TypedQuery<ShippingSearch> query = entityManager.createNamedQuery(ShippingOrderEntity.QUERY_SEARCH, ShippingSearch.class);
		query.setParameter("value", "%" + searchValue + "%");
		return query.getResultList();
	}

	public OrderContext getShipping(Integer shippingId) {
		TypedQuery<OrderContext> query = entityManager.createNamedQuery(ShippingOrderEntity.QUERY_FIND_BY_ID, OrderContext.class);
		query.setParameter("shippingOrderId", shippingId);
		return query.getSingleResult();
	}

}
