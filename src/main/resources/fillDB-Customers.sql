-- Customers
INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Oberndorf', '71708');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES('65', 'Hohenbergstr.', '1');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('Wössner GmbH', '0.5', '12456879', 'Wössner', 'dwoessner@me.com', '654789', '45321456', 'David', 'MALE', '1');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Schwetzingen', '68723');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('10', 'Muehlstr.', '2');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('Bauer GmbH', '0.1', '389276498', 'Bauer', 'Bauer@gmail.de', '387886', '34297467', 'Kai', 'MALE', '2');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Reilingen', '68799');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('14', 'Bromberweg', '3');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('Computershop GmbH', '0.2', '321478', 'Leopold', 'leopold@outlook.de', '387478', '2347827', 'Robin', 'MALE', '3');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Neckargemuend', '69151');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('20', 'Stuttgarterstr.', '4');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('Pflanzen AG', '0.2', '478234', 'Schneider', 'schneider@outlook.de', '43248', '4234532', 'Guenther', 'MALE', '4');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Dielheim', '69234');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('2', 'Industriestr.', '5');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('Weinmann GmbH', '0.0', '43452', 'Weinmann', 'Weinmann@outlook.de', '3467', '324787', 'Andrea', 'FEMALE', '5');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Gaiberg', '69251');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('102', 'Bauernstr.', '6');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('Schmidt GmbH', '0.1', '3242349', 'Schmidt', 'Schmidt@me.com', '63423', '432452', 'Emma', 'FEMALE', '6');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Eppelheim', '69214');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('34', 'Ahornweg', '7');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('Elektro GmbH', '0.1', '432412', 'Fischer', 'Elektro@gmx.de', '23443', '234325', 'Hannah', 'FEMALE', '7');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Ditzingen', '71254');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('132', 'Gartenstr.', '8');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('Weber GmbH', '0.05', '423423', 'Weber', 'Weber@me.com', '43245', '234748', 'Leon', 'MALE', '8');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Aidlingen', '71134');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('23', ' Goethestr.', '9');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('Meyer GmbH', '0.25', '324783', 'Meyer', 'Meyer@outlook.com', '34298', '2349893', 'Maximilian', 'MALE', '9');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Magstadt', '71106');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('15', 'Bahnhofstr.', '10');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('Wagner AG', '0.02', '324882', 'Wagner', 'Wagner@me.com', '23498', '2349832', 'Noah', 'MALE', '10');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Gärtringen', '71116');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('90', 'Danzigerstr.', '11');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('', '0.0', '93284', 'Becker', 'Julian.Becker@gmail.com', '34243', '2342352', 'Julian', 'MALE', '11');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Heimsheim', '71292');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('11', ' Koblenzerstr.', '12');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('', '0.0', '4234459', 'Schulz', 'Schulz@me.com', '654789', '45321456', 'Johanna', 'FEMALE', '12');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Schwaikheim', '71409');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('25', 'Lindenstr.', '13');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('', '0.0', '13323', 'Hoffmann', 'Hoffmann@gmx.com', '23423', '5433322', 'Fabian', 'MALE', '13');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Asperg', '71679');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('89', 'Neustr.', '14');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('', '0.0', '43249', 'Schäfer', 'Schäfer@me.com', '39429', '975347', 'Theo', 'MALE', '14');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Murr', '71711');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('45', 'Waldstr.', '15');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('', '0.0', '92489', 'Koch', 'Koch@outlook.com', '823475', '2397482', 'Till', 'MALE', '15');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Markgröningen', '71706');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('48', 'Eichendorffstr.', '16');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('', '0.0', '432429', 'Schröder', 'Schröder@gmx.de', '28374', '4398432', 'Samuel', 'MALE', '16');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Alzenau', '63755');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('6', 'Dorfstr.', '17');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('', '0.0', '23452', 'Schwarz', 'Schwarz@web.de', '23847', '297478', 'Jana', 'FEMALE', '17');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Elsenfeld', '63820');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('67', 'Wilhelmstr.', '18');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('', '0.0', '453533', 'Krause', 'Krause@me.com', '34958', '985934', 'Mira', 'FEMALE', '18');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Hösbach', '63768');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('20', 'Bergstr.', '19');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('', '0.0', '85738', 'Fuchs', 'Fuchs@web.com', '39475', '3945899', 'Fabienne', 'FEMALE', '19');

INSERT INTO `hssdb`.`location` (`city`, `zip`) VALUES ('Mainaschaff', '63814');
INSERT INTO `hssdb`.`address` (`number`, `street`, `Location_locationId`) VALUES ('2', 'Berlinerstr.', '20');
INSERT INTO `hssdb`.`customer` (`company`, `discount`, `fax`, `lastname`, `mail`, `mobile`, `phone`, `prename`, `title`, `Address_addressId`) VALUES ('', '0.0', '543456', 'Schreiber', 'Schreiber@outlook.com', '65449', '387356', 'Lotte', 'FEMALE', '20');

