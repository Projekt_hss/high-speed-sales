-- Items
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Crucial_8GB_DDR3','649528754592','Crucial','8GB Crucial DDR3L-1600 CL11 SO-DIMM','Stück','41.93','49.9','Arbeitsspeicher','100','10');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Corsair_16GB_DDR4','843591070010','Corsair','16GB Corsair Vengeance DDR4 DRAM 3000 MHz schwarz','Stück','99.52','109.99','Arbeitsspeicher','50','10');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('SSD_850Pro_256GB','8806086264631','Samsung','Samsung 850 PRO Series SSD - 256GB','Stück','88.56','129.99','Festplatten','50','10');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('SSD_m2_850EVO_250GB','8806086585675','Samsung','Samsung 850 EVO Series mSATA SSD - 120GB','Stück','33.2','69.9','Festplatten','30','5');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Intel_i7_6700','5032037076685','Intel','Intel Core i7-6700k 4x 4.00GHz, Sockel 1151, 8MB Cache, Quad-Core, OC, boxed ohne Kühler','Stück','320.1','356.99','Prozessoren','25','5');

INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Sharkoon VS4-V','527589842459','Sharkoon','Kabelmanagement-System ,Front-I/O: 2x USB 3.0, 2x Audio, Schnellverschlüsse für zwei optische Laufwerke und drei 3,5" Festplatten, 6 Slots für Erweiterungskarten','Stück','26.88','31.99','Gehaeuse','50','15');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Thermaltake Versa H15','827590874138','Thermaltake','Frontanschlüsse: 1x USB 3.0, 1x USB 2.0, HD Audio, 1 vorinstallierter 120mm Lüfter, Seitenfenster ','Stück','38.56','45.99','Gehaeuse','30','25');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Silent Base 800 BG001 ATX ','857639278045','be quiet ','3x 5.25 extern, 7x 3.5 intern, Drei be quiet! Pure Wings 2 Lüfter, Front USB 3.0 und Audio Schnittstellen, Isolierungsmatten, silber/schwarzes Design ','Stück','95.71','113.90','Gehaeuse','20','19');


-- CPU-Kuehler
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('BK013 Shadow Rock','728958027652','Listan','2 CPU-Kühler LGA 775 /1150 /1155 /1156 /1366 /2011/AMD 754 /939 /940 /AM2+ /AM3+ /FM1 /FM2 (4-polig)','Stück','35.21','41.90','CPU-Kuehler','80','40');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('ARCTIC Freezer 13','287569251674','Artic','92 mm PWM Lüfter - CPU Kühler für AMD und Intel Sockel bis zu 200 Watt Kühlleistung','Stück','21.76','25.89','CPU-Kuehler','100','35');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('T4 CPU-Kühler ','287168946742','Cooler Master','120mm Lüfter','Stück','22.61','16.90','CPU-Kühler','60','15');

-- Prozessor
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Intel Core i7-6700K','190384736829','Intel','i7-6700K (8MB Cache, LGA1151, 4GHz)','Stück','294.12','350.00','Prozessor','120','80');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Intel Core i5-6600 ','826450287357','Intel','i5-6600 (bis zu 3.90 GHz, 65 W, 6 MB SmartCache) ','Stück','195.74','232.93','Prozessor','110','20');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Intel i7-4790K','289167457892','Intel','i7-4790K (4.00 GHz, Max. Turbo 4.4 GHz, Sockel 1150, 8M Cache, 88Watt)','Stück','314.29','374.00','Prozessor','80','70');

-- Arbeitsspeicher

INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Crucial_8GB_DDR3','649528754592','Crucial','8GB Crucial DDR3L-1600 CL11 SO-DIMM','Stück','41.93','49.9','Arbeitsspeicher','100','10');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('HyperX Beast 8GB DDR3','287469176389','HyperX','8GB HyperX Beast XMP KHX16C9T3K2/Arbeitsspeicher (1600MHz, CL9) DDR3-RAM','Stück','25.21','30.00','Arbeitsspeicher','150','100');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Corsair CMZ8GX3M2A1600C9 Vengeance 8GB','837561982520','Corsair','2x 4GB DDR3 1600 Mhz','Stück','52.93','62.99','Arbeitsspeicher','50','25');

-- Grafikkarte

INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('GEFORCE GTX 1070','829176489376','Nvidia','Pascal/8 GB GDDR5/1683 MHz','Stück','419.33','499.00','Grafikkarte','50','40');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('GeForce GTX 980','829174637892','Nvidia','4GB/1216 MHz/224 GB/s','Stück','504.20','600.00','Grafikkarte','100','20');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('GEFORCE GTX 1080','829176583982','Nvidia','Pascal/8 GB GDDR5X/1733 MHz','Stück','663.03','789.00','Grafikkarte','50','40');

-- Mainboard

INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Asus Z170 Pro Gaming Mainboard','839287462985','Asus','Sockel 1151 (ATX, Intel Z170, 4x DDR4-Speicher, USB 3.1, M.2 Schnittstelle)','Stück','132.88','158.13','Mainboard','100','60');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('MSI 7693-040R 970 Gaming','371936298519','MSI','AM3+ AMD Motherboard (ATX, 4x DDR3 1333, SATA 3, USB 3.0)','Stück','83.10','98.89','Mainboard','80','10');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('MSI Mainboard H110M GAMING ','291875683986','MSI','LGA1151 max 32GB 2x DDR4 2x PCIe 1x PCIe x16 1x VGA 1x DVI 4x SATA3 2x USB2.0 2x USB3.1 mATX','Stück','62.18','74.00','Arbeitsspeicher','100','10');

-- Festplatte

INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('SanDisk SSD 240GB','289187469847','SanDisk','240GB Sata III 2,5 Zoll Interne SSD, bis zu 530 MB/Sek','Stück','55.46','66.00','Festplatte','150','50');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Samsung MZ-75E500B/EU 500GB','289187495876','Samsung','Samsung MZ-75E500B/EU 850 EVO interne SSD 500GB (6,4 cm (2,5 Zoll), SATA III) schwarz','Stück','133.61','159.00','Festplatte','100','40');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Seagate Desktop HDD 1 TB','28847658028','Seagate','interne Festplatte 3.5 SATA 6GB/s - ST1000DM003','Stück','45.37','53.99','Festplatte','200','60');

-- Laufwerk

INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('LG GH24NSB0 DVD','829761783936','LG','DVD 24x Brenner (DVD±RW) bulk schwarz','Stück','14.66','17.44','Laufwerk','100','20');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Asus DRW-24D5MT DVD','1983781658092','Asus','Asus DRW-24D5MT interner 24x DVD Brenner (DVD+-RW, Retail E-Green Silent) schwarz','Stück','15.12','17.99','Laufwerk','50','35');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Asus BC-12D2HT Blu-Ray','9281984798276','Asus','Asus BC-12D2HT Silent internes Blu-Ray Combo Laufwerk (12x BD-R (Lesen), 16x DVD±R (Schreiben), Bulk, BDXL, Sata, Schwarz','Stück','50.42','60.00','Laufwerk','100','70');

-- Netzteil
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`)VALUES ('HKC V-POWER 550 Watt','2891874907386','HKC','HKC® V-POWER 550 Watt ATX PC-Netzteil, Schutzschaltkreise: OPP, OCP, OVP, SCP, 20+4pin Stromversorgung, PFC, Ultra-leise, 120mm FAN (V-550)','Stück','33.53','39.90','Netzteil','150','50');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Sharkoon WPM600 600 Watt','910987636784','Sharkoon','Sharkoon WPM600 Bronze PC-Netzteil (600 Watt, ATX, Kabelmanagement)','Stück','50.34','59.90','Netztteil','120','60');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Corsair CP-9020097-EU 550 Watt','289381764790','Corsair','Corsair CP-9020097-EU VS Serie VS550 80 Plus ATX Netzteil (230V, 550W)','Stück','37.73','44.90','Netzteil','100','40');


-- Soundkarte
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Creative Sound Blaster Z','378279387891','Creative','Creative Sound Blaster Z Interne Soundkarte (PCI-Express, leistungsstarker Kopfhörerverstärker, Mikrofon mit Beamforming)','Stück','60.50','71.99','Soundkarte','50','10');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Asus Xonar DGX','829187490817','Asus','Asus Xonar DGX 5.1 PCI-Express Sound Karte (105dB, 3,5mm RCA Jack)','Stück','30.25','36.00','Soundkarte','50','25');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Asus Strix Soar','178297468946','Asus','Asus Strix Soar interne Gaming Soundkarte (PCI-Express, Kopfhörerverstärker, 116db SNR)','Stück','75.54','89.89','Soundkarte','100','50');


-- Betriebssytem
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Windows 10 Home','289174840926','Microsoft','64 Bit/DVD','Stück','16.80','19.99','Betriebssysytem','200','40');
INSERT INTO `hssdb`.`article` (`articleName`, `eanNummer`, `producer`, `description`, `unity`, `purchasePrice`, `salesPrice`, `productGroup`, `recorderPoint`, `stock`) VALUES ('Windows 10 Pro','839284756719','Microsoft','64 Bit/DVD','Stück','50.34','59.90','Betriebssystem','200','30');
