-- Users
INSERT INTO `hssdb`.`employee` (`lastname`, `password`, `prename`, `username`) VALUES ('Wössner', 'd', 'David', 'dwoessner');
INSERT INTO `hssdb`.`employee` (`lastname`, `password`, `prename`, `username`) VALUES ('Eggers', 'l', 'Lukas', 'leggers');
INSERT INTO `hssdb`.`employee` (`lastname`, `password`, `prename`, `username`) VALUES ('Szvitok', 'd', 'Kevin', 'kszvitok');
INSERT INTO `hssdb`.`employee` (`lastname`, `password`, `prename`, `username`) VALUES ('Barner', 'm', 'Marc', 'mbarner');
INSERT INTO `hssdb`.`employee` (`lastname`, `password`, `prename`, `username`) VALUES ('Kuzmiak', 'j', 'Janina', 'jkuzmiak');
INSERT INTO `hssdb`.`employee` (`lastname`, `password`, `prename`, `username`) VALUES ('Jay', 'a', 'Andreas', 'ajay');