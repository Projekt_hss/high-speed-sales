
    create table address (
        addressId integer not null auto_increment,
        number varchar(6) not null,
        street varchar(45) not null,
        Location_locationId integer not null,
        primary key (addressId)
    );

    create table article (
        articleId integer not null auto_increment,
        articleName varchar(55) not null,
        description longtext not null,
        eanNummer varchar(25) not null,
        producer varchar(25) not null,
        productGroup varchar(45) not null,
        purchasePrice double precision not null,
        recorderPoint double precision not null,
        salesPrice double precision not null,
        stock double precision not null,
        unity varchar(25) not null,
        primary key (articleId)
    );

    create table customer (
        customerId integer not null auto_increment,
        company varchar(25),
        discount double precision not null,
        fax varchar(15),
        lastname varchar(25) not null,
        mail varchar(55),
        mobile varchar(15),
        phone varchar(15),
        prename varchar(15) not null,
        title varchar(15) not null,
        Address_addressId integer not null,
        primary key (customerId)
    );

    create table employee (
        employeeId integer not null auto_increment,
        lastname varchar(25) not null,
        password varchar(15) not null,
        prename varchar(15) not null,
        username varchar(45) not null,
        primary key (employeeId)
    );

    create table hssorder (
        orderId integer not null auto_increment,
        date datetime not null,
        discount double precision not null,
        discountTotal double precision not null,
        installation bit not null,
        orderTotal double precision not null,
        paymentType varchar(15) not null,
        positionTotal double precision not null,
        shippingType varchar(15) not null,
        state varchar(15) not null,
        vat double precision not null,
        vatTotal double precision not null,
        Creator_employeeId integer not null,
        Customer_customerId integer not null,
        Shipping_addressId integer not null,
        primary key (orderId)
    );

    create table invoice (
        invoiceId integer not null auto_increment,
        invoiceDate date not null,
        Creator_employeeId integer not null,
        Order_orderId integer not null,
        primary key (invoiceId)
    );

    create table location (
        locationId integer not null auto_increment,
        city varchar(45) not null,
        zip varchar(15) not null,
        primary key (locationId)
    );

    create table orderposition (
        orderPositionId integer not null auto_increment,
        amount double precision not null,
        number integer not null,
        positionPrice double precision not null,
        Article_articleId integer not null,
        Order_orderId integer,
        primary key (orderPositionId)
    );

    create table shippingorder (
        shippingOrderId integer not null auto_increment,
        shippingOrderDate date not null,
        Creator_employeeId integer not null,
        Order_orderId integer not null,
        primary key (shippingOrderId)
    );

    alter table address 
        add constraint FKkjk93h702u3md8l3rgbjf4t5s 
        foreign key (Location_locationId) 
        references location (locationId);

    alter table customer 
        add constraint FKke8hc6lqgir36n63f3uibcq6p 
        foreign key (Address_addressId) 
        references address (addressId);

    alter table hssorder 
        add constraint FK1hyn6ojpk6wfanuvinfhld937 
        foreign key (Creator_employeeId) 
        references employee (employeeId);

    alter table hssorder 
        add constraint FKro6josxfaldgx3x57fwvu7xn8 
        foreign key (Customer_customerId) 
        references customer (customerId);

    alter table hssorder 
        add constraint FKlnqwfdvxj60dshonq0exkvgx6 
        foreign key (Shipping_addressId) 
        references address (addressId);

    alter table invoice 
        add constraint FK677n3gm1ymwm3u4n3aln6cjdl 
        foreign key (Creator_employeeId) 
        references employee (employeeId);

    alter table invoice 
        add constraint FKjk42eipn53p45fdo41l9uessw 
        foreign key (Order_orderId) 
        references hssorder (orderId);

    alter table orderposition 
        add constraint FKbluw66ut24cdwlknclbh574d9 
        foreign key (Article_articleId) 
        references article (articleId);

    alter table orderposition 
        add constraint FKm7g2i9q4umbaqnyvd3ovspqhw 
        foreign key (Order_orderId) 
        references hssorder (orderId);

    alter table shippingorder 
        add constraint FKakv8d2jdr6wceg7q9shtid22u 
        foreign key (Creator_employeeId) 
        references employee (employeeId);

    alter table shippingorder 
        add constraint FK2x4miwym0clbelr1grql0gr8i 
        foreign key (Order_orderId) 
        references hssorder (orderId);
